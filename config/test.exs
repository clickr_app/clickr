import Config

# Only in tests, remove the complexity from the password hashing algorithm
config :bcrypt_elixir, :log_rounds, 1

# Configure your database
#
# The MIX_TEST_PARTITION environment variable can be used
# to provide built-in test partitioning in CI environment.
# Run `mix help test` for more information.
config :clickr, Clickr.Repo,
  username: "postgres",
  password: "postgres",
  database: "clickr_test#{System.get_env("MIX_TEST_PARTITION")}",
  hostname: "postgres",
  pool_size: 10

config :clickr, Clickr.Commanded.EventStore,
  username: "postgres",
  password: "postgres",
  database: "clickr_event_store_test",
  hostname: "postgres",
  pool_size: 10

# We don't run a server during test. If one is required,
# you can enable the server option below.
config :clickr, ClickrWeb.Endpoint,
  http: [ip: {127, 0, 0, 1}, port: 4002],
  secret_key_base: "PPWhXToYiITDr4IZk8XjsU8pqi5aHPN4EjVezYw+kIKeGyniwxfNXscqHStx/+al",
  server: false

# In test we don't send emails.
config :clickr, Clickr.Mailer, adapter: Swoosh.Adapters.Test

# Print only warnings and errors during test
config :logger, level: :warn

# Initialize plugs at runtime for faster test compilation
config :phoenix, :plug_init_mode, :runtime

config :clickr, BotFramework, authentication_enabled?: true

config :gettext, :default_locale, "en"
