# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :clickr,
  ecto_repos: [Clickr.Repo],
  generators: [binary_id: true]

config :clickr, Clickr.Commanded.EventStore, column_data_type: "jsonb"

# Configures the endpoint
config :clickr, ClickrWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: ClickrWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: Clickr.PubSub,
  live_view: [signing_salt: "L6+f5cbg"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :clickr, Clickr.Mailer,
  adapter: Swoosh.Adapters.Local,
  from_email: "noreply@klassenknopf.de",
  from_name: "KlassenKnopf"

# Swoosh API client is needed for adapters other than SMTP.
config :swoosh, :api_client, false

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.12.18",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :surface, :components, [
  {Surface.Components.Form.ErrorTag,
   default_translator: {ClickrWeb.ErrorHelpers, :translate_error}}
]

config :clickr, Clickr.CommandedApplication,
  event_store: [
    adapter: Commanded.EventStore.Adapters.EventStore,
    event_store: Clickr.Commanded.EventStore
  ],
  # TODO #scale using phoenix pubsub
  pubsub: :local,
  # TODO #scale using distributed registry
  registry: :local

config :clickr, event_stores: [Clickr.Commanded.EventStore]

config :clickr, Clickr.Commanded.EventStore, serializer: Clickr.JsonbSerializer

config :commanded, default_dispatch_return: :aggregate_state
config :commanded, default_consistency: :strong

config :commanded_ecto_projections, repo: Clickr.Repo

config :vex, sources: [Clickr.Vex.Validators, Vex.Validators]

config :clickr, Clickr.TeamsApp, app_id: "426ca1a8-7ba0-4271-8df5-290485fbd68a"

config :clickr, BotFramework,
  authentication_enabled?: true,
  client_id: "e1043f25-f0a0-4091-b914-68fc2db92bac"

config :gettext, :default_locale, "de"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
