const defaultTheme = require('tailwindcss/defaultTheme')

module.exports = {
  mode: 'jit',
  purge: [
    './js/**/*.js',
    '../config/**/*.*exs',
    '../lib/*_web/**/*.*ex',
    '../lib/*_web/**/*.sface'
  ],
  theme: {
    extend: {
        colors: {
          primary: defaultTheme.colors.yellow,
        }
    },
  },
  variants: {
    extend: {},
  },
  plugins: [
    require('@tailwindcss/forms'),
  ],
}
