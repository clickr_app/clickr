defmodule Clickr.MixProject do
  use Mix.Project

  def project do
    [
      app: :clickr,
      version: "0.1.0",
      elixir: "~> 1.13.0-rc.1",
      elixirc_paths: elixirc_paths(Mix.env()),
      compilers: [:gettext] ++ Mix.compilers() ++ [:surface],
      start_permanent: Mix.env() == :prod,
      aliases: aliases(),
      deps: deps()
    ]
  end

  # Configuration for the OTP application.
  #
  # Type `mix help compile.app` for more information.
  def application do
    [
      mod: {Clickr.Application, []},
      extra_applications: [:logger, :runtime_tools]
    ]
  end

  # Specifies which paths to compile per environment.
  defp elixirc_paths(:test), do: ["lib", "test/support"]
  defp elixirc_paths(_), do: ["lib"]

  # Specifies your project dependencies.
  #
  # Type `mix help deps` for examples and options.
  defp deps do
    [
      # basics
      # password hashing
      {:bcrypt_elixir, "~> 2.0"},
      {:telemetry_metrics, "~> 0.6"},
      {:telemetry_poller, "~> 1.0"},
      # HTTP server
      {:plug_cowboy, "~> 2.5"},
      # JSON encode/decode
      {:jason, "~> 1.2"},
      {:elixir_uuid, "~> 1.2.1"},

      # phoenix + surface
      {:phoenix, "~> 1.6.2"},
      {:phoenix_html, "~> 3.0"},
      {:phoenix_live_view, "~> 0.16.4"},
      {:surface, "~> 0.6.1"},
      {:surface_formatter, "~> 0.6.0"},
      {:phoenix_live_dashboard, "~> 0.5.3"},

      # database
      {:phoenix_ecto, "~> 4.4"},
      {:ecto_sql, "~> 3.8.1"},
      {:postgrex, ">= 0.0.0"},
      {:ecto_psql_extras, "~> 0.6"},

      # CQRS/ES
      {:commanded, "~> 1.3"},
      {:commanded_eventstore_adapter, "~> 1.2"},
      {:commanded_ecto_projections, "~> 1.2"},

      # other
      # mails
      {:swoosh, "~> 1.3"},
      # required by swoosh api client
      {:hackney, "~> 1.18.0"},
      # i18n
      {:gettext, "~> 0.18"},
      # UI icons
      {:heroicons, "~> 0.2.4"},
      # validation
      {:vex, "~> 0.9"},
      # Struct constructors (.new)
      {:exconstructor, "~> 1.2.4"},
      # better date/time
      {:timex, "~> 3.7.6"},
      # UI charts
      {:contex, "~> 0.4.0"},
      # HTTP client
      {:httpoison, "~> 1.8.0"},
      # JWT token auth
      {:joken, "~> 2.4.1"},

      # dev
      {:phoenix_live_reload, "~> 1.3.3", only: :dev},
      {:esbuild, "~> 0.2", runtime: Mix.env() == :dev},
      {:phx_gen_tailwind, "~> 0.1.3", only: :dev},
      {:mix_test_watch, "~> 1.1", only: :dev, runtime: false},

      # test
      # HTML parser: search by CSS selector
      {:floki, ">= 0.30.0", only: :test}
    ]
  end

  # Aliases are shortcuts or tasks specific to the current project.
  # For example, to install project dependencies and perform other setup tasks, run:
  #
  #     $ mix setup
  #
  # See the documentation for `Mix` for more info on aliases.
  defp aliases do
    [
      dev: "phx.server",
      setup: [
        "deps.get",
        "ecto.setup",
        "event_store.setup",
        "run priv/repo/seeds.exs",
        "cmd npm --prefix ./assets install"
      ],
      "ecto.setup": ["ecto.create", "ecto.migrate"],
      "ecto.reset": ["ecto.drop", "ecto.setup"],
      "event_store.setup": ["event_store.create", "event_store.init"],
      "event_store.reset": ["event_store.drop", "event_store.setup"],
      test: [
        "ecto.create --quiet",
        "ecto.migrate --quiet",
        "event_store.create --quiet",
        "event_store.init --quiet",
        "test"
      ],
      "assets.deploy": [
        "cmd npm --prefix ./assets run deploy",
        "esbuild default --minify",
        "phx.digest"
      ],
      "teams.zip": ["cmd --cd teams_app zip clickr.zip *"],
      "translate.de": "gettext.extract --merge --locale de"
    ]
  end
end
