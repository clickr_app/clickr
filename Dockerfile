ARG BUILDER_IMAGE=hexpm/elixir:1.13.0-rc.1-erlang-24.1.7-alpine-3.14.2
ARG RUNNER_IMAGE=alpine:3.14.2

# Builder
FROM $BUILDER_IMAGE AS builder

ENV HEX_HTTP_TIMEOUT=20
ENV MIX_ENV=prod
ENV SECRET_KEY_BASE=nokey

RUN apk add --no-cache npm make g++
RUN mix local.hex --force
RUN mix local.rebar --force

COPY mix.exs mix.lock ./
RUN mix deps.get --only prod
RUN mix deps.compile

COPY assets/package.json assets/package-lock.json ./assets/
RUN npm --prefix ./assets ci --progress=false --no-audit --loglevel=error

COPY rel rel
COPY priv priv
COPY assets assets
COPY config config
COPY lib lib

RUN mix compile
RUN mix assets.deploy
RUN mix release

# Runner
FROM $RUNNER_IMAGE

RUN apk add --no-cache openssl ncurses-libs libstdc++

WORKDIR /app
ENV HOME=/app
ENV SECRET_KEY_BASE=nokey
ENV PORT=4000

COPY --from=builder _build/prod/rel/clickr ./
RUN set -eux
RUN ln -nfs bin/clickr ./entry

CMD ["./entry", "start"]
