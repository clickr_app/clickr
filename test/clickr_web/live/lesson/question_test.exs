defmodule ClickrWeb.Lesson.QuestionTest do
  use ClickrWeb.ConnCase
  import Phoenix.LiveViewTest
  alias Clickr.{Buttons, Classes, Lessons, Rooms, SeatingPlans, Students}

  setup :register_and_log_in_user

  setup %{button_id: b_id, user: user} do
    {:ok, %{id: r_id}} = Rooms.create(user, name: "R42", width: 1, height: 1)
    {:ok, %{id: c_id}} = Classes.create(user, name: "6a")
    {:ok, %{id: s_id}} = Students.create(user, name: "Max", class_id: c_id)
    wait_for_event(Classes.Events.AddedStudent)
    {:ok, %{id: sp_id}} = SeatingPlans.create(user, class_id: c_id, room_id: r_id)

    {:ok, _} = Rooms.position_button(user, id: r_id, button_id: b_id, x: 1, y: 1)
    {:ok, _} = SeatingPlans.position_student(user, id: sp_id, student_id: s_id, x: 1, y: 1)

    {:ok, %{id: l_id}} = Lessons.start(user, room_id: r_id, class_id: c_id)
    {:ok, _} = Lessons.call_the_roll(user, id: l_id)
    {:ok, _} = Lessons.raise_hand(user, id: l_id, student_id: s_id)
    {:ok, _} = Lessons.note_attendance(user, id: l_id)

    %{lesson_id: l_id}
  end

  test "student gets a point if clicked during question",
       %{conn: conn, lesson_id: l_id, button_id: b_id, user: user} do
    # when render
    {:ok, view, _html} = live(conn, Routes.lesson_path(conn, :await_question, l_id))

    # then has 0 points
    assert element(view, ".test-points") |> render() =~ "0"

    # when teacher clicked ask_question and student clicked
    element(view, "button[phx-click=ask_question]")
    |> render_click()

    Buttons.clicked(user, id: b_id)
    wait_for_event(Lessons.Events.RaisedHand)

    # then has raised hand
    assert has_element?(view, "[data-raised-hand]")

    # when click note_raised_hands
    element(view, "button[phx-click=note_raised_hands]")
    |> render_click()

    # then changes route and has 1 points
    assert_patched(view, Routes.lesson_path(conn, :await_question, l_id))
    assert element(view, ".test-points") |> render() =~ "1"
  end
end
