defmodule Clickr.Storage do
  @doc """
  Clear the event store and read store databases
  """
  def reset! do
    Application.stop(:clickr)
    reset_eventstore!()
    reset_readstore!()
    {:ok, _} = Application.ensure_all_started(:clickr)
  end

  defp reset_eventstore! do
    config = Clickr.Commanded.EventStore.config()

    {:ok, conn} =
      config
      |> EventStore.Config.default_postgrex_opts()
      |> Postgrex.start_link()

    EventStore.Storage.Initializer.reset!(conn, config)
  end

  defp reset_readstore! do
    {:ok, conn} = Postgrex.start_link(Clickr.Repo.config())

    Postgrex.query!(conn, truncate_readstore_tables(), [])
  end

  defp truncate_readstore_tables do
    projection_tables = Clickr.Mixins.Projection.tables()

    """
    TRUNCATE TABLE
    users,
    users_tokens,
    #{Enum.join(projection_tables, ", ")},
    projection_versions
    RESTART IDENTITY
    CASCADE;
    """
  end
end
