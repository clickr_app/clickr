defmodule Clickr.ClickrIds do
  defmacro __using__(_params) do
    quote do
      alias Clickr.{GradingIntervals, SeatingPlans, Grades}

      setup_all do
        static = %{
          button_id: "5e86a334-4240-11ec-a9b2-4b575b82b683",
          class_id: "89dddf5c-4f6b-11eb-9b90-37def753d03d",
          device_id: "64876002-4240-11ec-b74a-3f8038a51b4c",
          gateway_id: "84a27392-5e81-11ec-831d-dfec9a9a0ec2",
          grade_id: "53e5237a-5cc5-11ec-8182-d3689ac71c8d",
          grading_interval_id: GradingIntervals.default_id(),
          lesson_id: "eb370500-3e24-11ec-8dd0-2f308938e6cb",
          room_id: "be53bf80-3d78-11ec-96b4-ef0a730b3ac5",
          student_id: "d1afbb7a-4f6b-11eb-91ac-97c9bf0e09e1",
          user_id: "fd07e57c-4e02-11ec-be45-5b5bc150935e"
        }

        Map.merge(static, %{
          seating_plan_id: SeatingPlans.id(static),
          grade_id: Grades.id(static)
        })
      end
    end
  end
end
