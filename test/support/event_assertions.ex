defmodule Clickr.EventAssertions do
  alias Commanded.Assertions.EventAssertions, as: Delegate
  alias Clickr.CommandedApplication, as: App

  def assert_correlated(event_type_a, predicate_a, event_type_b, predicate_b) do
    Delegate.assert_correlated(App, event_type_a, predicate_a, event_type_b, predicate_b)
  end

  def assert_receive_event(event_type, assertion_fn) do
    Delegate.assert_receive_event(App, event_type, assertion_fn)
  end

  def assert_receive_event(event_type, predicate_fn, assertion_fn) do
    Delegate.assert_receive_event(App, event_type, predicate_fn, assertion_fn)
  end

  def refute_receive_event(event_type, refute_fn, opts \\ []) do
    Delegate.refute_receive_event(App, event_type, refute_fn, opts)
  end

  def wait_for_event(event_type) do
    Delegate.wait_for_event(App, event_type)
  end

  def wait_for_event(event_type, predicate_fn) do
    Delegate.wait_for_event(App, event_type, predicate_fn)
  end
end
