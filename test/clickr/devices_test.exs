defmodule Clickr.DevicesTest do
  use Clickr.DataCase

  alias Clickr.{Devices}

  setup do
    %{user: Clickr.AccountsFixtures.user_fixture()}
  end

  describe "upsert" do
    test "updates read model on repeated dispatches", %{
      user: user,
      device_id: d_id,
      gateway_id: g_id
    } do
      # when
      {:ok, _} = Devices.upsert(user, id: d_id, gateway_id: g_id, name: "42A", battery: nil)
      {:ok, _} = Devices.upsert(user, id: d_id, gateway_id: g_id, name: "42B", battery: 99.0)
      read_model = Devices.by_id(d_id)

      # then
      assert read_model.name == "42B"
      assert read_model.battery == 99.0
    end
  end
end
