defmodule Clickr.ClassesTest do
  use Clickr.DataCase

  alias Clickr.{Classes, Rooms, SeatingPlans, Students}
  alias Clickr.Classes.{Events}

  setup do
    %{user: Clickr.AccountsFixtures.user_fixture()}
  end

  describe "create" do
    test "publishes Created event", %{user: user} do
      {:ok, _} = Classes.create(user, name: "6a")

      assert_receive_event(Events.Created, fn e ->
        assert e.name == "6a"
      end)
    end
  end

  describe "delete" do
    test "deletes projection", %{user: user} do
      # given
      {:ok, %{id: id}} = Classes.create(user, name: "6a")
      assert {_, 1} = Classes.list()

      # when
      {:ok, _} = Classes.delete(user, id: id)

      # then
      assert {_, 0} = Classes.list()
    end
  end

  describe "add_student (called by Students.create)" do
    test "syncs student to seating plan", %{student_id: st_id, user: user} do
      # given
      {:ok, %{id: id}} = Classes.create(user, name: "6a")
      {:ok, %{id: r_id}} = Rooms.create(user, name: "R42", width: 8, height: 6)
      {:ok, %{id: sp_id}} = SeatingPlans.create(user, %{class_id: id, room_id: r_id})
      {:ok, _} = Students.create(user, %{id: st_id, class_id: id, name: "Max"})

      assert_receive_event(SeatingPlans.Events.AddedStudent, fn e ->
        assert e.id == sp_id
        assert e.student_id == st_id
      end)
    end
  end

  describe "list" do
    test "searches by class name prefix", %{user: user} do
      # given
      {:ok, %{id: id}} = Classes.create(user, name: "6a")
      {:ok, _} = Classes.create(user, name: "7a")

      # when
      {[class], count} = Classes.list(query: "6")

      # then
      assert count == 1
      assert class.id == id
    end
  end
end
