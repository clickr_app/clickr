defmodule Clickr.SeatingPlansTest do
  use Clickr.DataCase

  alias Clickr.{Classes, Rooms, SeatingPlans}
  alias Clickr.SeatingPlans.{Events}

  setup do
    %{user: Clickr.AccountsFixtures.user_fixture()}
  end

  describe "create" do
    setup %{user: user} do
      {:ok, %{id: class_id}} = Classes.create(user, name: "6a")
      {:ok, %{id: room_id}} = Rooms.create(user, name: "R42", width: 8, height: 6)
      [class_id: class_id, room_id: room_id]
    end

    test "publishes Created event", %{class_id: class_id, room_id: room_id, user: user} do
      {:ok, _} = SeatingPlans.create(user, class_id: class_id, room_id: room_id)

      assert_receive_event(Events.Created, fn e ->
        assert e.class_id == class_id
        assert e.room_id == room_id
      end)
    end

    test "creates read model entry", %{class_id: class_id, room_id: room_id, user: user} do
      {:ok, %{id: id}} = SeatingPlans.create(user, class_id: class_id, room_id: room_id)

      read_model = SeatingPlans.by_id!(id)
      assert read_model.class_id == class_id
      assert read_model.room_id == room_id
    end
  end
end
