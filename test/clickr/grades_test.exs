defmodule Clickr.GradesTest do
  use Clickr.DataCase

  alias Clickr.{Classes, GradingIntervals, Grades, Lessons, Rooms, SeatingPlans, Students}
  alias Clickr.Grades.{Events}
  import Clickr.AccountsFixtures

  setup do
    %{user: user_fixture()}
  end

  describe "create" do
    test "publishes Created event", %{user: user} do
      {:ok, %{id: c_id}} = Classes.create(user, name: "6a")
      {:ok, %{id: s_id}} = Students.create(user, name: "Max Mustermann", class_id: c_id)
      gi_id = GradingIntervals.default_id()

      assert_receive_event(Events.Created, fn e ->
        assert e.student_id == s_id
        assert e.grading_interval_id == gi_id
        assert e.id == Grades.id(%{student_id: s_id, grading_interval_id: gi_id})
      end)
    end
  end

  describe "grades" do
    setup %{user: user} do
      {:ok, %{id: c_id}} = Classes.create(user, name: "6a")
      {:ok, %{id: s_id}} = Students.create(user, name: "Max Mustermann", class_id: c_id)
      {:ok, %{id: r_id}} = Rooms.create(user, name: "R42", width: 8, height: 6)
      {:ok, _} = SeatingPlans.create(user, class_id: c_id, room_id: r_id)
      {:ok, %{id: l1_id}} = Lessons.start(user, class_id: c_id, room_id: r_id)
      {:ok, %{id: l2_id}} = Lessons.start(user, class_id: c_id, room_id: r_id)
      gi_id = GradingIntervals.default_id()
      g_id = Grades.id(%{student_id: s_id, grading_interval_id: gi_id})

      %{class_id: c_id, student_id: s_id, grade_id: g_id, lesson_1_id: l1_id, lesson_2_id: l2_id}
    end

    test "stores two lesson grades in database", %{
      user: user,
      grade_id: g_id,
      lesson_1_id: l1_id,
      lesson_2_id: l2_id
    } do
      {:ok, _} =
        Grades.add_lesson_grade(user, %Grades.Commands.AddLessonGrade{
          id: g_id,
          lesson_id: l1_id,
          percent: 0.42
        })

      {:ok, _} =
        Grades.add_lesson_grade(user, %Grades.Commands.AddLessonGrade{
          id: g_id,
          lesson_id: l2_id,
          percent: 0.84
        })

      assert %{lessons: [%{id: ^l1_id, percent: 0.42}, %{id: ^l2_id, percent: 0.84}]} =
               Grades.by_id!(g_id)
    end

    test "removes lesson grade from database", %{
      user: user,
      grade_id: g_id,
      lesson_1_id: l1_id,
      lesson_2_id: l2_id
    } do
      {:ok, _} =
        Grades.add_lesson_grade(user, %Grades.Commands.AddLessonGrade{
          id: g_id,
          lesson_id: l1_id,
          percent: 0.42
        })

      {:ok, _} =
        Grades.remove_lesson_grade(user, %Grades.Commands.AddLessonGrade{
          id: g_id,
          lesson_id: l1_id
        })

      assert %{lessons: []} = Grades.by_id!(g_id)
    end

    test "adds bonus grade to database", %{user: user, grade_id: g_id, lesson_1_id: l1_id} do
      {:ok, _} =
        Grades.add_bonus_grade(user, %Grades.Commands.AddBonusGrade{
          id: g_id,
          percent: 0.42
        })

      assert %{bonus: [0.42]} = Grades.by_id!(g_id)
    end
  end
end
