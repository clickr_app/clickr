defmodule Clickr.RoomsTest do
  use Clickr.DataCase
  alias Clickr.Rooms
  alias Clickr.Rooms.Events

  setup do
    %{user: Clickr.AccountsFixtures.user_fixture()}
  end

  describe "create" do
    test "publishes Created event", %{user: user} do
      {:ok, _} = Rooms.create(user, name: "R42", width: 8, height: 6)

      assert_receive_event(Events.Created, fn e ->
        assert e.name == "R42"
      end)
    end
  end
end
