defmodule Clickr.SeatingPlans.Aggregates.SeatingPlanTest do
  alias Clickr.SeatingPlans.Aggregates.SeatingPlan
  alias Clickr.SeatingPlans.Commands

  use Clickr.AggregateCase, aggregate: SeatingPlan

  test "executes all commands", %{
    class_id: c_id,
    room_id: r_id,
    seating_plan_id: id,
    student_id: s_id
  } do
    assert_state(
      # when
      [
        %Commands.Create{id: id, class_id: c_id, room_id: r_id, students: []},
        %Commands.AddStudent{id: id, student_id: s_id},
        %Commands.PositionStudent{id: id, student_id: s_id, x: 1, y: 2},
        %Commands.UnpositionStudent{id: id, student_id: s_id},
        %Commands.RemoveStudent{id: id, student_id: s_id},
        %Commands.Delete{id: id}
      ],

      # then
      %SeatingPlan{id: id, students: [], unpositioned: [], positioned: %{}, deleted?: true}
    )
  end
end
