defmodule Clickr.Lessons.Aggregates.LessonTest do
  alias Clickr.Lessons.Aggregates.Lesson
  alias Clickr.Lessons.Commands

  use Clickr.AggregateCase, aggregate: Lesson

  test "executes all commands", %{
    class_id: c_id,
    room_id: r_id,
    lesson_id: id,
    student_id: s_id,
    user_id: u_id
  } do
    # given
    other_s_id = UUID.uuid4()

    assert_state(
      # when
      [
        %Commands.Start{
          id: id,
          user_id: u_id,
          button_mapping: %{},
          class_id: c_id,
          room_id: r_id,
          students: [s_id, other_s_id]
        },
        %Commands.CallTheRoll{id: id},
        %Commands.RaiseHand{id: id, student_id: s_id},
        %Commands.NoteAttendance{id: id},
        %Commands.AskQuestion{id: id},
        %Commands.RaiseHand{id: id, student_id: s_id},
        %Commands.NoteRaisedHands{id: id},
        %Commands.AdjustPoints{id: id, student_id: s_id, delta: 5},
        %Commands.AddPointForAll{id: id, delta: 3},
        %Commands.NoteLateArrival{id: id, student_id: other_s_id},
        %Commands.NoteNotAttending{id: id, student_id: other_s_id},
        %Commands.End{id: id},
        %Commands.Grade{id: id, method: "linear", opts: %{"min" => 1, "max" => 10}},
        %Commands.Delete{id: id}
      ],

      # then
      %Lesson{
        id: id,
        status: "graded",
        students: MapSet.new([s_id, other_s_id]),
        attending: MapSet.new([s_id]),
        raised_hands: MapSet.new(),
        points: %{s_id => 9},
        questions: 1,
        deleted?: true
      }
    )
  end
end
