defmodule Clickr.StudentsTest do
  use Clickr.DataCase

  alias Clickr.{Classes, Rooms, SeatingPlans, Students}
  alias Clickr.Students.{Events}

  setup do
    %{user: Clickr.AccountsFixtures.user_fixture()}
  end

  describe "create" do
    setup %{user: user} do
      {:ok, %{id: class_id}} = Classes.create(user, name: "6a")
      [class_id: class_id]
    end

    test "publishes Created event", %{class_id: class_id, user: user} do
      {:ok, %{id: id}} = Students.create(user, name: "Max Mustermann", class_id: class_id)

      assert_receive_event(Events.Created, fn e ->
        assert e.name == "Max Mustermann"
      end)

      assert_receive_event(Clickr.Grades.Events.Created, fn e ->
        assert e.student_id == id
      end)
    end

    test "adds student to class", %{class_id: class_id, user: user} do
      {:ok, %{id: student_id}} = Students.create(user, name: "Max Mustermann", class_id: class_id)

      assert_receive_event(Classes.Events.AddedStudent, fn e ->
        assert e.student_id == student_id
      end)
    end

    test "renames student in seating plan", %{class_id: class_id, user: user} do
      {:ok, %{id: student_id}} = Students.create(user, name: "Max Mustermann", class_id: class_id)
      {:ok, %{id: room_id}} = Rooms.create(user, name: "R42", width: 1, height: 1)
      {:ok, %{id: sp_id}} = SeatingPlans.create(user, class_id: class_id, room_id: room_id)
      {:ok, _} = Students.rename(user, id: student_id, name: "Maxime")

      seating_plan = SeatingPlans.by_id!(sp_id)
      assert %{name: "Maxime"} = Enum.find(seating_plan.students, fn s -> s.id == student_id end)
    end

    test "writes grade to read model", %{class_id: c_id, lesson_id: l_id, user: user} do
      {:ok, %{id: id}} = Students.create(user, name: "Max Mustermann", class_id: c_id)
      wait_for_event(Grades.Events.Created)
      g_id = Grades.id(%{student_id: id, grading_interval_id: GradingIntervals.default_id()})
      {:ok, _} = Grades.add_lesson_grade(user, id: g_id, lesson_id: l_id, percent: 42)

      assert Students.by_id!(id).current_grade == 42
    end
  end
end
