defmodule Clickr.Rooms.Aggregates.RoomTest do
  alias Clickr.Rooms.Aggregates.Room
  alias Clickr.Rooms.Commands

  use Clickr.AggregateCase, aggregate: Room

  test "executes all commands", %{
    button_id: b_id,
    room_id: id
  } do
    assert_state(
      # when
      [
        %Commands.Create{id: id, name: "R42", width: 8, height: 6},
        %Commands.PositionButton{id: id, button_id: b_id, x: 1, y: 1},
        %Commands.RemoveButton{id: id, button_id: b_id},
        %Commands.ChangeSize{id: id, width: 10, height: 4},
        %Commands.Rename{id: id, name: "R142"},
        %Commands.Delete{id: id}
      ],

      # then
      %Room{id: id, buttons: %{}, deleted?: true}
    )
  end
end
