defmodule Clickr.Students.Aggregates.StudentTest do
  alias Clickr.Students.Aggregates.Student
  alias Clickr.Students.Commands

  use Clickr.AggregateCase, aggregate: Student

  test "executes all commands", %{
    student_id: s_id,
    class_id: c_id
  } do
    assert_state(
      # when
      [
        %Commands.Create{id: s_id, name: "Max Mustermann", class_id: c_id},
        %Commands.Rename{id: s_id, name: "Marta Mustermann"},
        %Commands.RemoveFromClass{id: s_id},
        %Commands.Delete{id: s_id}
      ],

      # then
      %Student{id: s_id, deleted?: true}
    )
  end
end
