defmodule Clickr.Classes.Aggregates.ClassTest do
  alias Clickr.Classes.Aggregates.Class
  alias Clickr.Classes.Commands

  use Clickr.AggregateCase, aggregate: Class

  test "executes all commands", %{
    class_id: c_id,
    student_id: s_id
  } do
    assert_state(
      # when
      [
        %Commands.Create{id: c_id, name: "6a"},
        %Commands.AddStudent{id: c_id, student_id: s_id},
        %Commands.RemoveStudent{id: c_id, student_id: s_id},
        %Commands.Rename{id: c_id, name: "7a"},
        %Commands.Delete{id: c_id}
      ],

      # then
      %Class{id: c_id, student_ids: [], deleted?: true}
    )
  end
end
