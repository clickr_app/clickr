defmodule Clickr.LessonsTest do
  use Clickr.DataCase

  alias Clickr.{Buttons, Classes, Grades, Rooms, Lessons, SeatingPlans, Students}
  alias Clickr.Lessons.{Events}

  setup do
    %{user: Clickr.AccountsFixtures.user_fixture()}
  end

  describe "create" do
    setup %{button_id: b_id, user: user} do
      {:ok, %{id: c_id}} = Classes.create(user, name: "6a")
      {:ok, %{id: r_id}} = Rooms.create(user, name: "R42", width: 8, height: 6)
      {:ok, %{id: sp_id}} = SeatingPlans.create(user, class_id: c_id, room_id: r_id)
      {:ok, %{id: s_id}} = Students.create(user, class_id: c_id, name: "Max Mustermann")

      assert_receive_event(Grades.Events.Created, fn e ->
        assert e.student_id == s_id
      end)

      {:ok, _} = Rooms.position_button(user, id: r_id, button_id: b_id, x: 1, y: 1)
      {:ok, _} = SeatingPlans.position_student(user, id: sp_id, student_id: s_id, x: 1, y: 1)

      [class_id: c_id, room_id: r_id, seating_plan_id: sp_id, student_id: s_id]
    end

    test "publishes Started event", %{class_id: c_id, room_id: r_id, user: user} do
      {:ok, _} = Lessons.start(user, class_id: c_id, room_id: r_id)

      assert_receive_event(Events.Started, fn e ->
        assert e.class_id == c_id
        assert e.room_id == r_id
      end)
    end

    test "subscribes to button click events and publishes grades", %{
      button_id: b_id,
      class_id: c_id,
      device_id: d_id,
      room_id: r_id,
      student_id: s_id,
      user: user
    } do
      {:ok, %{id: id}} = Lessons.start(user, class_id: c_id, room_id: r_id)

      {:ok, _} = Lessons.call_the_roll(user, id: id)

      :ok =
        Buttons.clicked(user, %{id: b_id, name: "button", device_id: d_id, device_name: "test"})

      wait_for_event(Events.RaisedHand)
      {:ok, _} = Lessons.note_attendance(user, id: id)

      {:ok, _} = Lessons.ask_question(user, id: id)

      :ok =
        Buttons.clicked(user, %{id: b_id, name: "button", device_id: d_id, device_name: "test"})

      wait_for_event(Events.RaisedHand, fn e -> e.cause == "question" end)
      {:ok, _} = Lessons.note_raised_hands(user, id: id)

      {:ok, _} = Lessons.end(user, id: id)

      assert_receive_event(Events.Ended, fn e ->
        assert e.points == %{s_id => 1}
      end)

      {:ok, _} = Lessons.grade(user, id: id, method: "linear", opts: %{"min" => 0, "max" => 2})

      assert_receive_event(Grades.Events.AddedLessonGrade, fn e ->
        assert e.lesson_id == id
        assert e.percent == 50
      end)
    end

    test "subscribes to teams button click events (mapped by name)", %{
      class_id: c_id,
      room_id: r_id,
      student_id: s_id,
      user: user
    } do
      {:ok, %{id: id}} = Lessons.start(user, class_id: c_id, room_id: r_id)

      {:ok, _} = Lessons.call_the_roll(user, id: id)

      clicked_event = Buttons.Teams.ByName.clicked(%{"name" => "Max Mustermann"})

      :ok = Buttons.clicked(user, clicked_event)

      assert_receive_event(Events.RaisedHand, fn e ->
        assert e.student_id == s_id
      end)
    end
  end
end
