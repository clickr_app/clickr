defmodule Clickr.Devices.Aggregates.DeviceTest do
  alias Clickr.Devices.Aggregates.Device
  alias Clickr.Devices.Commands

  use Clickr.AggregateCase, aggregate: Device

  test "executes all commands", %{
    device_id: d_id,
    gateway_id: g_id
  } do
    assert_state(
      # when
      [
        %Commands.Upsert{id: d_id, gateway_id: g_id, name: "42A", battery: nil},
        %Commands.Upsert{id: d_id, gateway_id: g_id, name: "42B", battery: 99},
        %Commands.Delete{id: d_id}
      ],

      # then
      %Device{id: d_id, name: "42B", battery: 99, deleted?: true}
    )
  end
end
