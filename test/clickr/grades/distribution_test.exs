defmodule Clickr.Grades.DistributionTest do
  alias Clickr.Grades.Distribution
  use ExUnit.Case, async: true

  describe "buckets" do
    test "calculates buckets correctly" do
      # given
      input = [10, 40, 95]
      number = 4

      # when
      result = Distribution.buckets(input, number)

      # then
      assert result == %{
               {0.0, 25.0} => 1,
               {25.0, 50.0} => 1,
               {50.0, 75.0} => 0,
               {75.0, 100.0} => 1
             }
    end
  end
end
