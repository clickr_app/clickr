defmodule Clickr.Grades.Aggregates.GradeTest do
  alias Clickr.Grades.Aggregates.Grade
  alias Clickr.Grades.Commands

  use Clickr.AggregateCase, aggregate: Grade

  test "executes all commands", %{
    grade_id: g_id,
    grading_interval_id: gi_id,
    lesson_id: l_id,
    student_id: s_id
  } do
    assert_state(
      # when
      [
        %Commands.Create{
          id: g_id,
          student_id: s_id,
          grading_interval_id: gi_id,
          calculation_method: "lesson_average",
          calculation_opts: %{}
        },
        %Commands.AddLessonGrade{id: g_id, lesson_id: l_id, percent: 95},
        %Commands.AddBonusGrade{id: g_id, percent: 90},
        %Commands.RemoveLessonGrade{id: g_id, lesson_id: l_id},
        %Commands.Delete{id: g_id}
      ],

      # then
      %Grade{
        id: g_id,
        calculation_method: "lesson_average",
        calculation_opts: %{},
        lessons: %{},
        bonus: [90],
        calculated: 90,
        deleted?: true
      }
    )
  end
end
