defmodule Clickr.Repo.Migrations.CreateDevicesDevice do
  use Ecto.Migration

  def change do
    create table(:devices_device, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :gateway_id, :binary_id
      add :name, :string
      add :battery, :float
      timestamps()
    end

    create(index(:devices_device, [:gateway_id]))
  end
end
