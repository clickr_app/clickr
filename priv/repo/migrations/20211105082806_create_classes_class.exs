defmodule Clickr.Repo.Migrations.CreateClassesClass do
  use Ecto.Migration

  def change do
    create table(:classes_class, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :student_ids, {:array, :binary_id}

      timestamps()
    end

    create(index(:classes_class, [:updated_at]))
    create(index(:classes_class, [:name]))
  end
end
