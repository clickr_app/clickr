defmodule Clickr.Repo.Migrations.AddLessonsAndBonusToGrade do
  use Ecto.Migration

  def change do
    alter table("grades_grade") do
      add :lessons, :jsonb
      add :bonus, {:array, :float}
    end
  end
end
