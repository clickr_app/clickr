defmodule Clickr.Repo.Migrations.AddRoomAndClassIndexToLessons do
  use Ecto.Migration

  def change do
    create(index(:lessons_lesson, [:room_id]))
    create(index(:lessons_lesson, [:class_id]))
  end
end
