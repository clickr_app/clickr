defmodule Clickr.Repo.Migrations.CreateLessonsLesson do
  use Ecto.Migration

  def change do
    create table(:lessons_lesson, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :status, :string
      add :class_id, :binary_id
      add :room_id, :binary_id
      add :class_name, :string
      add :room_name, :string
      add :width, :integer
      add :height, :integer
      add :students, :jsonb
      add :attending, :jsonb
      add :grades, :jsonb
      add :button_mapping, :jsonb
      timestamps()
    end

    create(index(:lessons_lesson, [:updated_at]))
  end
end
