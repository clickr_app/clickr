defmodule Clickr.Repo.Migrations.CreateStudentsStudent do
  use Ecto.Migration

  def change do
    create table(:students_student, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :class_id, :binary_id
      add :current_grade_id, :binary_id
      add :current_grade, :float
      timestamps()
    end

    create(index(:students_student, [:updated_at]))
    create(index(:students_student, [:current_grade_id]))
    create(index(:students_student, [:name]))
  end
end
