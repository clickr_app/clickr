defmodule Clickr.Repo.Migrations.CreateSeatingPlansSeatingPlan do
  use Ecto.Migration

  def change do
    create table(:seating_plans_seating_plan, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :class_id, :binary_id
      add :room_id, :binary_id
      add :class_name, :string
      add :room_name, :string
      add :students, :jsonb
      add :positions, :jsonb

      timestamps()
    end

    create(index(:seating_plans_seating_plan, [:updated_at]))
    create(index(:seating_plans_seating_plan, [:class_id, :room_id]))
    create(index(:seating_plans_seating_plan, [:class_name]))
    create(index(:seating_plans_seating_plan, [:room_name]))
  end
end
