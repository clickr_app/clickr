defmodule Clickr.Repo.Migrations.CreateGradesGrade do
  use Ecto.Migration

  def change do
    create table(:grades_grade, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :student_id, :binary_id
      add :grading_interval_id, :binary_id
      add :calculated, :float
      timestamps()
    end

    create(index(:grades_grade, [:student_id]))
    create(index(:grades_grade, [:grading_interval_id]))
  end
end
