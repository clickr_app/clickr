defmodule Clickr.Repo.Migrations.CreateRoomsRoom do
  use Ecto.Migration

  def change do
    create table(:rooms_room, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :name, :string
      add :width, :integer
      add :height, :integer
      add :buttons, :jsonb

      timestamps()
    end

    create(index(:rooms_room, [:updated_at]))
    create(index(:rooms_room, [:name]))
  end
end
