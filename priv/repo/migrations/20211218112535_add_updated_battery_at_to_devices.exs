defmodule Clickr.Repo.Migrations.AddUpdatedBatteryAtToDevices do
  use Ecto.Migration

  def change do
    alter table("devices_device") do
      add :updated_battery_at, :naive_datetime
    end
  end
end
