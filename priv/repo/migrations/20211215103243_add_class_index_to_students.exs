defmodule Clickr.Repo.Migrations.AddClassIndexToStudents do
  use Ecto.Migration

  def change do
    create(index(:students_student, [:class_id]))
  end
end
