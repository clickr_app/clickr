defmodule Clickr.Repo.Migrations.CreateGradesGradeWithDetails do
  use Ecto.Migration

  def change do
    create table(:grades_grade_with_details, primary_key: false) do
      add :id, :binary_id, primary_key: true
      add :student_id, :binary_id
      add :grading_interval_id, :binary_id
      add :calculated, :float
      add :lessons, :jsonb
      add :bonus, {:array, :float}
      timestamps()
    end

    create(index(:grades_grade_with_details, [:student_id]))
    create(index(:grades_grade_with_details, [:grading_interval_id]))
  end
end
