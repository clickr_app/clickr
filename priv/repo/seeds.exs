# Script for populating the database. You can run it as:
#
#     mix run priv/repo/seeds.exs
#
# Inside the script, you can read and write to any of your
# repositories directly:
#
#     Clickr.Repo.insert!(%Clickr.SomeSchema{})
#
# We recommend using the bang functions (`insert!`, `update!`
# and so on) as they will fail if something goes wrong.

{:ok, user} = Clickr.Accounts.register_user(%{email: "f@ftes.de", password: "passwordpassword"})
{:ok, %{id: room_id}} = Clickr.Rooms.create(user, %{name: "R42", width: 8, height: 6})
{:ok, %{id: class_id}} = Clickr.Classes.create(user, %{name: "6a"})

{:ok, %{id: seating_plan_id}} =
  Clickr.SeatingPlans.create(user, %{class_id: class_id, room_id: room_id})

keys = [
  "2345890-",
  "qweruiop",
  "asdfjkl;",
  "zxcvnm,."
]

for {key_row, y} <- Enum.with_index(keys, 1),
    {key, x} <- Enum.with_index(String.graphemes(key_row), 1) do
  clicked = Clickr.Buttons.Keyboard.KeyUp.clicked(user, key)
  {:ok, %{id: button_id}} = Clickr.Buttons.register(user, clicked)
  {:ok, _} = Clickr.Rooms.position_button(user, %{id: room_id, button_id: button_id, x: x, y: y})

  {:ok, %{id: student_id}} =
    Clickr.Students.create(user, %{name: "#{key} #{String.upcase(key)}", class_id: class_id})

  {:ok, _} =
    Clickr.SeatingPlans.position_student(user, %{
      id: seating_plan_id,
      student_id: student_id,
      x: x,
      y: y
    })
end
