# TODO

## Bugs
- only answer to first question gives a point (only occurred once?)

## Deployment
- new zigbee stick
- expose port 5000 screen sharing
- eventstoreDB

## Features
- exclude lesson from grading
- question with countdown (30s, 1m, 2m, 5m)
- auto select room and subject for teacher
- pagination
- device overview
- gateway overview
- student grade details view (on hover?)
- add statistical measures to chart (median, mean, std. deviation)
- back up/restore deconz configuration (/opt/deCONZ/zll.db)

## Tests
- enforce architectural boundaries: https://github.com/sasa1977/boundary

## Refactoring
- bump surface/lv version
- replace alpine with live view JS
- combine devices/buttons contexts (move button into device?)
- split into StudentLesson aggregates
- remove currentGrade from student
- merge into monorepo
- replace QuestionToTeams.MappingAgent with ETS
- nanoId (replace UUID)

## Multi tenancy #tenants
- pubsub topics: add tenant_id
- management UI
- commanded: one application per tenant (later)
- read model: one db schema per tenant (later)

## Scaling #scale
- use distributed registry for workflow GenServer/Agents

## Teams
- blog about elixir/botframework/teams
- MPN ID -> add to app
- publish teams app
- get rid of "something went wrong" on teams button click
- make Teams clicks safe: save student.teams_user_id instead of mapping via name
- show in-meeting dialog (bubble)

## Deconz/Zigbee
- source routing: did it help?
