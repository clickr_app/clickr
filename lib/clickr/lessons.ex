defmodule Clickr.Lessons do
  alias Clickr.Lessons.{Aggregates, Commands, Events, Queries}
  use Clickr.Mixins.Context, aggregate: Aggregates.Lesson, commands: Commands, events: Events
  import Clickr.Gettext

  def list(opts \\ []) do
    Queries.List.run(opts)
  end

  def by_id!(id) do
    Queries.ById.run!(id)
  end

  # ["started", "roll_call", "await_question", "question", "ended", "graded"]
  def gettext_status(%{status: "started"}), do: gettext("started")
  def gettext_status(%{status: "roll_call"}), do: gettext("roll call")
  def gettext_status(%{status: "await_question"}), do: gettext("waiting for question")
  def gettext_status(%{status: "question"}), do: gettext("question")
  def gettext_status(%{status: "ended"}), do: gettext("ended")
  def gettext_status(%{status: "graded"}), do: gettext("graded")
end
