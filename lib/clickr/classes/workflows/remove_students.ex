defmodule Clickr.Classes.Workflows.RemoveStudents do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.{Accounts, Students}
  alias Clickr.Classes.Events

  @impl true
  def handle(%Events.Deleted{} = e, metadata) do
    {students, _} = Students.list(where: [class_id: e.id])
    opts = with_cause(metadata, consistency: :eventual)

    for student <- students do
      Task.start(fn -> remove(student, opts) end)
    end

    :ok
  end

  defp remove(student, opts) do
    user = Accounts.system_user()
    attrs = [id: student.id]
    {:ok, _} = Students.remove_from_class(user, attrs, opts)
  end
end
