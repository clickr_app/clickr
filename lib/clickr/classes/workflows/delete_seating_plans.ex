defmodule Clickr.Classes.Workflows.DeleteSeatingPlans do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.{Accounts, SeatingPlans}
  alias Clickr.Classes.Events

  @impl true
  def handle(%Events.Deleted{} = e, metadata) do
    {seating_plans, _} = SeatingPlans.list(where: [class_id: e.id])
    opts = with_cause(metadata, consistency: :eventual)

    for seating_plan <- seating_plans do
      Task.start(fn -> delete(seating_plan, opts) end)
    end

    :ok
  end

  defp delete(seating_plan, opts) do
    user = Accounts.system_user()
    attrs = [id: seating_plan.id]
    {:ok, _} = SeatingPlans.delete(user, attrs, opts)
  end
end
