defmodule Clickr.Classes.Workflows.SyncStudentsToSeatingPlans do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.{Accounts, SeatingPlans}
  alias Clickr.Classes.Events

  @impl true
  def handle(%Events.AddedStudent{} = e, metadata) do
    handle_internal(e, metadata, &SeatingPlans.add_student/3)
  end

  def handle(%Events.RemovedStudent{} = e, metadata) do
    handle_internal(e, metadata, &SeatingPlans.remove_student/3)
  end

  defp handle_internal(%{id: class_id, student_id: student_id}, metadata, command_fn) do
    {seating_plans, _} = SeatingPlans.list(where: [class_id: class_id])
    opts = with_cause(metadata, consistency: :eventual)

    for seating_plan <- seating_plans do
      Task.start(fn -> add_or_remove(seating_plan.id, student_id, opts, command_fn) end)
    end

    :ok
  end

  defp add_or_remove(seating_plan_id, student_id, opts, command_fn) do
    user = Accounts.system_user()
    attrs = [id: seating_plan_id, student_id: student_id]
    {:ok, _} = command_fn.(user, attrs, opts)
  end
end
