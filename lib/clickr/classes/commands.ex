defmodule Clickr.Classes.Commands do
  use Clickr.Mixins.Commands

  field(:name, presence: true, length: [min: 2])
  field(:student_id, presence: true, uuid: true)

  command(:AddStudent, [:student_id])
  command(:Create, [:name], generate_id: true)
  command(:Delete, [])
  command(:RemoveStudent, [:student_id])
  command(:Rename, [:name])
end
