defmodule Clickr.Classes.Supervisor do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        Clickr.Classes.Projectors.Class,
        Clickr.Classes.Workflows.DeleteSeatingPlans,
        Clickr.Classes.Workflows.RemoveStudents,
        Clickr.Classes.Workflows.SyncStudentsToSeatingPlans
      ],
      strategy: :one_for_one
    )
  end
end
