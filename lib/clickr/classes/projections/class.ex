defmodule Clickr.Classes.Projections.Class do
  use Clickr.Mixins.Projection, projection_table: "classes_class"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :name, :string
    field :student_ids, {:array, :binary_id}, default: []
    timestamps()
  end
end
