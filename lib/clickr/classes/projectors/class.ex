defmodule Clickr.Classes.Projectors.Class do
  use Clickr.Mixins.Projector
  alias Clickr.Classes.{Events, Projections}

  project(%Events.Created{} = e, fn multi ->
    Ecto.Multi.insert(multi, :insert, %Projections.Class{
      id: e.id,
      name: e.name,
      student_ids: []
    })
  end)

  project(%Events.AddedStudent{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), push: [student_ids: e.student_id])
  end)

  project(%Events.RemovedStudent{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), pull: [student_ids: e.student_id])
  end)

  project(%Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [name: e.name])
  end)

  project(%Events.Deleted{} = e, fn multi ->
    Ecto.Multi.delete_all(multi, :delete, by_id(e.id))
  end)

  defp by_id(id), do: from(c in Projections.Class, where: c.id == ^id)
end
