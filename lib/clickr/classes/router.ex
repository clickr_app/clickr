defmodule Clickr.Classes.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.Classes.Aggregates.Class,
    commands: Clickr.Classes.Commands
end
