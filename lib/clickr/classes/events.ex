defmodule Clickr.Classes.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "class"

  event(:AddedStudent, [:student_id])
  event(:Created, [:name])
  event(:Deleted, [])
  event(:RemovedStudent, [:student_id])
  event(:Renamed, [:name])
end
