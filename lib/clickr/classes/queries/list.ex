defmodule Clickr.Classes.Queries.List do
  use Clickr.Mixins.Queries.List, schema: Clickr.Classes.Projections.Class, query_field: :name
end
