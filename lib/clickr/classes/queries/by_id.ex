defmodule Clickr.Classes.Queries.ById do
  use Clickr.Mixins.Queries.ById, schema: Clickr.Classes.Projections.Class
end
