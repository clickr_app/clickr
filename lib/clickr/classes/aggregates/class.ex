defmodule Clickr.Classes.Aggregates.Class do
  defstruct [:id, student_ids: [], deleted?: false]

  alias Clickr.Classes.{Commands, Events}
  alias __MODULE__, as: A

  use Clickr.Mixins.Aggregate,
    prefix: "class",
    with_deleted: [command: Commands.Delete, event: Events.Deleted]

  # Public: Command handlers

  def execute(%A{id: nil}, %Commands.Create{} = c) do
    %Events.Created{id: c.id, name: c.name}
  end

  def execute(%A{id: nil}, _c) do
    {:error, :not_created}
  end

  def execute(%A{} = a, %Commands.AddStudent{} = c) do
    with :ok <- student_not_added?(a, c) do
      %Events.AddedStudent{id: c.id, student_id: c.student_id}
    end
  end

  def execute(%A{} = a, %Commands.RemoveStudent{} = c) do
    with :ok <- student_added?(a, c) do
      %Events.RemovedStudent{id: c.id, student_id: c.student_id}
    end
  end

  def execute(%A{}, %Commands.Rename{} = c) do
    %Events.Renamed{id: c.id, name: c.name}
  end

  # Internal: State mutators

  def apply(%A{} = a, %Events.Created{} = e) do
    %A{a | id: e.id}
  end

  def apply(%A{} = a, %Events.AddedStudent{} = e) do
    %A{a | student_ids: [e.student_id | a.student_ids]}
  end

  def apply(%A{} = a, %Events.RemovedStudent{} = e) do
    %A{a | student_ids: List.delete(a.student_ids, e.student_id)}
  end

  def apply(%A{} = a, %Events.Renamed{}) do
    a
  end

  # Private

  defp student_not_added?(%A{student_ids: students}, %{student_id: student}) do
    case student in students do
      true -> {:error, :student_in_class}
      false -> :ok
    end
  end

  defp student_added?(%A{student_ids: students}, %{student_id: student}) do
    case student in students do
      true -> :ok
      false -> {:error, :student_not_in_class}
    end
  end
end
