defmodule Clickr.Lessons.Aggregates.Lesson do
  defstruct [
    :id,
    :status,
    students: MapSet.new(),
    attending: MapSet.new(),
    raised_hands: MapSet.new(),
    points: %{},
    questions: 0,
    deleted?: false
  ]

  # @statuses ["started", "roll_call", "await_question", "question", "ended", "graded"]

  alias Clickr.Lessons.{Commands, Events, GradingMethods}
  alias __MODULE__, as: A

  use Clickr.Mixins.Aggregate,
    prefix: "lesson"

  defimpl Commanded.Serialization.JsonDecoder do
    def decode(%A{} = a) do
      a
      |> Map.update!(:students, &MapSet.new/1)
      |> Map.update!(:attending, &MapSet.new/1)
      |> Map.update!(:raised_hands, &MapSet.new/1)
    end
  end

  defmodule Lifespan do
    @behaviour Commanded.Aggregates.AggregateLifespan

    @impl true
    def after_event(%Events.Graded{}), do: :stop
    def after_event(%Events.Deleted{}), do: :stop
    def after_event(_), do: :infinity

    @impl true
    def after_command(_), do: :infinity

    @impl true
    def after_error(_), do: :infinity
  end

  # Public API

  def grade(%A{} = a, method, opts) do
    grade = fn {std, p} -> {std, GradingMethods.grade(method, opts, p)} end

    a.points
    |> Enum.map(grade)
    |> Map.new()
  end

  # Command handlers

  def execute(%A{deleted?: true}, _c), do: {:error, :deleted}

  def execute(%A{id: nil} = a, %Commands.Start{} = c) do
    with {:ok, status} <- transition?(a, c) do
      Events.Started.from_command(c, status: status)
    end
  end

  def execute(%A{id: nil}, _c) do
    {:error, :not_created}
  end

  def execute(%A{} = a, %Commands.Delete{} = c) do
    %Events.Deleted{id: c.id, status: a.status, attending: MapSet.to_list(a.attending)}
  end

  def execute(%A{} = a, %Commands.CallTheRoll{} = c) do
    with {:ok, status} <- transition?(a, c) do
      %Events.CalledTheRoll{id: c.id, status: status}
    end
  end

  def execute(%A{} = a, %Commands.NoteAttendance{} = c) do
    with {:ok, status} <- transition?(a, c) do
      %Events.NotedAttendance{
        id: c.id,
        status: status,
        raised_hands: MapSet.to_list(a.raised_hands)
      }
    end
  end

  def execute(%A{status: status} = a, %Commands.NoteLateArrival{} = c)
      when status in ["await_question", "question", "ended"] do
    with :ok <- student?(a, c), :ok <- not_attending?(a, c) do
      %Events.NotedLateArrival{id: c.id, status: a.status, student_id: c.student_id}
    end
  end

  def execute(%A{status: status} = a, %Commands.NoteNotAttending{} = c)
      when status in ["await_question", "question", "ended"] do
    with :ok <- student?(a, c), :ok <- attending?(a, c) do
      %Events.NotedNotAttending{id: c.id, status: a.status, student_id: c.student_id}
    end
  end

  def execute(%A{} = a, %Commands.AskQuestion{} = c) do
    with {:ok, status} <- transition?(a, c) do
      %Events.AskedQuestion{id: c.id, status: status}
    end
  end

  def execute(%A{} = a, %Commands.NoteRaisedHands{} = c) do
    with {:ok, status} <- transition?(a, c) do
      inc = fn student -> if MapSet.member?(a.raised_hands, student), do: 1, else: 0 end
      points = Map.new(a.points, fn {student, p} -> {student, p + inc.(student)} end)

      %Events.NotedRaisedHands{
        id: c.id,
        status: status,
        raised_hands: MapSet.to_list(a.raised_hands),
        points: points
      }
    end
  end

  def execute(%A{} = a, %Commands.End{} = c) do
    with {:ok, status} <- transition?(a, c) do
      %Events.Ended{id: c.id, status: status, points: a.points, questions: a.questions}
    end
  end

  def execute(%A{} = a, %Commands.Grade{} = c) do
    with {:ok, status} <- transition?(a, c) do
      grades = grade(a, c.method, c.opts)
      %Events.Graded{id: c.id, status: status, method: c.method, opts: c.opts, grades: grades}
    end
  end

  def execute(%A{status: "roll_call"} = a, %Commands.RaiseHand{} = c) do
    with :ok <- student?(a, c),
         :ok <- not_raised_hand?(a, c) do
      %Events.RaisedHand{id: c.id, status: a.status, student_id: c.student_id, cause: "roll_call"}
    end
  end

  def execute(%A{status: "question"} = a, %Commands.RaiseHand{} = c) do
    with :ok <- attending?(a, c),
         :ok <- not_raised_hand?(a, c) do
      %Events.RaisedHand{id: c.id, status: a.status, student_id: c.student_id, cause: "question"}
    end
  end

  def execute(%A{}, %Commands.RaiseHand{}) do
    # ignore
    []
  end

  def execute(%A{status: status} = a, %Commands.AdjustPoints{} = c)
      when status in ["await_question", "question", "ended"] do
    with :ok <- attending?(a, c) do
      %Events.AdjustedPoints{id: c.id, status: a.status, student_id: c.student_id, delta: c.delta}
    end
  end

  def execute(%A{status: status} = a, %Commands.AddPointForAll{} = c)
      when status in ["await_question", "question", "ended"] do
    %Events.AddedPointForAll{id: c.id, status: a.status, delta: c.delta}
  end

  # Event handlers

  def apply(%A{} = a, %Events.Started{} = e) do
    %A{
      a
      | id: e.id,
        status: e.status,
        students: MapSet.new(e.students)
    }
  end

  def apply(%A{} = a, %Events.CalledTheRoll{} = e) do
    %A{a | status: e.status}
  end

  def apply(%A{} = a, %Events.NotedAttendance{} = e) do
    points = Map.new(e.raised_hands, fn student -> {student, 0} end)

    %A{
      a
      | status: e.status,
        attending: MapSet.new(e.raised_hands),
        raised_hands: MapSet.new(),
        points: points
    }
  end

  def apply(%A{} = a, %Events.NotedLateArrival{} = e) do
    %A{
      a
      | status: e.status,
        attending: MapSet.put(a.attending, e.student_id),
        points: Map.put(a.points, e.student_id, 0)
    }
  end

  def apply(%A{} = a, %Events.NotedNotAttending{} = e) do
    %A{
      a
      | status: e.status,
        attending: MapSet.delete(a.attending, e.student_id),
        points: Map.delete(a.points, e.student_id)
    }
  end

  def apply(%A{} = a, %Events.AskedQuestion{} = e) do
    %A{a | status: e.status}
  end

  def apply(%A{} = a, %Events.NotedRaisedHands{} = e) do
    %A{
      a
      | status: e.status,
        raised_hands: MapSet.new(),
        points: e.points,
        questions: a.questions + 1
    }
  end

  def apply(%A{} = a, %Events.Ended{} = e) do
    %A{a | status: e.status}
  end

  def apply(%A{} = a, %Events.Graded{} = e) do
    %A{a | status: e.status}
  end

  def apply(%A{} = a, %Events.RaisedHand{} = e) do
    %A{a | status: e.status, raised_hands: MapSet.put(a.raised_hands, e.student_id)}
  end

  def apply(%A{} = a, %Events.AdjustedPoints{} = e) do
    %A{a | status: e.status, points: Map.update!(a.points, e.student_id, &(&1 + e.delta))}
  end

  def apply(%A{} = a, %Events.AddedPointForAll{} = e) do
    %A{a | status: e.status, points: Map.new(a.points, fn {s, p} -> {s, p + e.delta} end)}
  end

  def apply(%A{} = a, %Events.Deleted{}) do
    %A{a | deleted?: true}
  end

  # Private
  transitions = [
    {nil, Commands.Start, "started"},
    {"started", Commands.CallTheRoll, "roll_call"},
    {"roll_call", Commands.NoteAttendance, "await_question"},
    {"await_question", Commands.AskQuestion, "question"},
    {"question", Commands.NoteRaisedHands, "await_question"},
    {"await_question", Commands.End, "ended"},
    {"ended", Commands.Grade, "graded"}
  ]

  for {from, cmd, to} <- transitions do
    defp transition?(%A{status: unquote(from)}, %unquote(cmd){}), do: {:ok, unquote(to)}
  end

  defp transition?(%A{status: from}, cmd),
    do: {:error, [:status_transition_not_allowed, from: from, to: cmd.__struct__]}

  defp student?(%A{students: students}, %{student_id: student}) do
    case MapSet.member?(students, student) do
      true -> :ok
      false -> {:error, :student_not_in_class}
    end
  end

  defp attending?(%A{attending: attending}, %{student_id: student}) do
    case MapSet.member?(attending, student) do
      true -> :ok
      false -> {:error, :student_not_attending}
    end
  end

  defp not_attending?(%A{attending: attending}, %{student_id: student}) do
    case MapSet.member?(attending, student) do
      true -> {:error, :student_already_attending}
      false -> :ok
    end
  end

  defp not_raised_hand?(%A{raised_hands: raised_hands}, %{student_id: student}) do
    case MapSet.member?(raised_hands, student) do
      true -> {:error, :student_already_raised_hand}
      false -> :ok
    end
  end
end
