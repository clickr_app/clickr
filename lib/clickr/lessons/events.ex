defmodule Clickr.Lessons.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "lesson"

  event(:AddedPointForAll, [:status, :delta])
  event(:AdjustedPoints, [:status, :student_id, :delta])
  event(:AskedQuestion, [:status, :raised_hands])
  event(:CalledTheRoll, [:status])
  event(:Deleted, [:status, :attending])
  event(:Ended, [:status, :points, :questions])
  event(:Graded, [:status, :method, :opts, :grades])
  event(:NotedAttendance, [:status, :raised_hands])
  event(:NotedLateArrival, [:status, :student_id])
  event(:NotedNotAttending, [:status, :student_id])
  event(:NotedRaisedHands, [:status, :raised_hands, :points])
  event(:RaisedHand, [:status, :student_id, :cause])
  event(:Started, [:status, :class_id, :room_id, :seating_plan_id, :students, :button_mapping])
end
