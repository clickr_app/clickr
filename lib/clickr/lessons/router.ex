defmodule Clickr.Lessons.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.Lessons.Aggregates.Lesson,
    commands: Clickr.Lessons.Commands
end
