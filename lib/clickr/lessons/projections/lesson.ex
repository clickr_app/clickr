defmodule Clickr.Lessons.Projections.Lesson do
  use Clickr.Mixins.Projection, projection_table: "lessons_lesson"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :status, :string
    field :class_id, :binary_id
    field :room_id, :binary_id
    field :class_name, :string
    field :room_name, :string
    field :width, :integer
    field :height, :integer
    field :attending, {:array, :string}
    field :button_mapping, :map
    field :grades, :map

    embeds_many :students, Student, primary_key: {:id, :binary_id, autogenerate: false} do
      field :name, :string
      field :x, :integer
      field :y, :integer
    end

    timestamps()
  end
end
