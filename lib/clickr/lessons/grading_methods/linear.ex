defmodule Clickr.Lessons.GradingMethods.Linear do
  defstruct [:min, :max]

  def grade(%{"min" => min, "max" => max}, _points) when min == max, do: 0.0

  def grade(%{"min" => min, "max" => max}, points) when is_integer(points) do
    (100.0 * (points - min) / (max - min))
    |> min(100.0)
    |> max(0.0)
  end

  def initial_opts(%Clickr.Lessons.Aggregates.Lesson{questions: questions}) do
    %{"min" => 0, "max" => questions}
  end

  def parse_opts(%{} = params) do
    {min, _} = Float.parse(params["min"])
    {max, _} = Float.parse(params["max"])
    %{"min" => min, "max" => max}
  end
end
