defmodule Clickr.Lessons.Workflows.QuestionToTeams.MappingAgent do
  use Agent

  def start_link(_) do
    Agent.start_link(fn -> %{} end, name: __MODULE__)
  end

  def get(lesson_id) do
    Agent.get(__MODULE__, &Map.get(&1, lesson_id))
  end

  def put(lesson_id, teams_activity) do
    Agent.get_and_update(__MODULE__, fn mapping ->
      case Map.has_key?(mapping, lesson_id) do
        true -> {:already_exists, mapping}
        false -> {:ok, Map.put(mapping, lesson_id, teams_activity)}
      end
    end)
  end

  def delete(lesson_id) do
    Agent.update(__MODULE__, &Map.delete(&1, lesson_id))
  end
end
