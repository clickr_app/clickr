defmodule Clickr.Lessons.Workflows.RemoveGrades do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.{Accounts, Grades, GradingIntervals}
  alias Clickr.Lessons.{Events}

  @impl true
  def handle(%Events.Deleted{status: "graded"} = e, metadata) do
    opts = with_cause(metadata, consistency: :eventual)

    for std <- e.attending do
      Task.start(fn -> remove_grade(e.id, std, opts) end)
    end

    :ok
  end

  defp remove_grade(lesson, student, opts) do
    user = Accounts.system_user()
    attrs = [id: grade_id(student), lesson_id: lesson]

    {:ok, _} = Grades.remove_lesson_grade(user, attrs, opts)
  end

  defp grade_id(student) do
    Grades.id(%{grading_interval_id: GradingIntervals.default_id(), student_id: student})
  end
end
