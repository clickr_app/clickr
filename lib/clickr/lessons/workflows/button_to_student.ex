defmodule Clickr.Lessons.Workflows.ButtonToStudent do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.Lessons.Events
  alias Clickr.Lessons.Workflows.ButtonToStudent.ClickProxy

  @impl true
  def handle(%Events.Started{} = e, _metadata) do
    ClickProxy.start(e.id, e.button_mapping, e.user_id)
    :ok
  end

  def handle(%Events.Ended{} = e, _metadata) do
    ClickProxy.stop(e.id)
    :ok
  end
end
