defmodule Clickr.Lessons.Workflows.ButtonToStudent.Supervisor do
  use DynamicSupervisor

  # Public API

  def start_child(opts) do
    DynamicSupervisor.start_child(__MODULE__, opts)
  end

  def stop_child(name) do
    case GenServer.whereis(name) do
      nil -> {:error, :not_found}
      pid -> GenServer.stop(pid)
    end
  end

  # Internal

  def start_link(init_arg) do
    DynamicSupervisor.start_link(__MODULE__, init_arg, name: __MODULE__)
  end

  @impl true
  def init(_init_arg) do
    DynamicSupervisor.init(strategy: :one_for_one)
  end
end
