defmodule Clickr.Lessons.Workflows.ButtonToStudent.ClickProxy do
  defstruct [:lesson_id, :mapping, :user_id]

  use GenServer
  alias Commanded.Registration
  alias Clickr.Lessons.Workflows.ButtonToStudent.Supervisor
  alias Clickr.{Accounts, CommandedApplication, Buttons, Lessons}
  alias __MODULE__, as: S
  require Logger

  # Public API

  def start(lesson_id, mapping, user_id) do
    state = %S{lesson_id: lesson_id, mapping: mapping, user_id: user_id}
    Supervisor.start_child({__MODULE__, state})
  end

  def stop(lesson_id) do
    Supervisor.stop_child(via_tuple(lesson_id))
  end

  # Internal

  def start_link(%S{} = state) do
    GenServer.start_link(__MODULE__, state, name: via_tuple(state.lesson_id))
  end

  @impl true
  def init(%S{} = state) do
    for button_id <- Map.keys(state.mapping) do
      Buttons.subscribe(button_id)
      Buttons.subscribe_only_for_user(state.user_id, button_id)
    end

    {:ok, state}
  end

  @impl true
  def handle_info(%Buttons.Events.Clicked{id: button}, %S{} = state) do
    student = state.mapping[button]
    Task.start(fn -> raise_hand(student, state) end)
    {:noreply, state}
  end

  # Private

  defp raise_hand(nil, _state), do: Logger.error("Button is not mapped to student")

  defp raise_hand(student, %S{} = s) do
    user = Accounts.system_user()
    attrs = [id: s.lesson_id, student_id: student]
    opts = [consistency: :eventual]
    Lessons.raise_hand(user, attrs, opts)
  end

  defp via_tuple(lesson_id) do
    name = {CommandedApplication, __MODULE__, lesson_id}
    Registration.via_tuple(CommandedApplication, name)
  end
end
