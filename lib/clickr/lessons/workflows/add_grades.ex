defmodule Clickr.Lessons.Workflows.AddGrades do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.{Accounts, Grades, GradingIntervals}
  alias Clickr.Lessons.Events

  @impl true
  def handle(%Events.Graded{} = e, metadata) do
    opts = with_cause(metadata, consistency: :eventual)

    for {std, pct} <- e.grades do
      Task.start(fn -> add_grade(e.id, std, pct, opts) end)
    end

    :ok
  end

  defp add_grade(lesson, student, percent, opts) do
    user = Accounts.system_user()
    attrs = [id: grade_id(student), lesson_id: lesson, percent: percent]

    {:ok, _} = Grades.add_lesson_grade(user, attrs, opts)
  end

  defp grade_id(student) do
    Grades.id(%{grading_interval_id: GradingIntervals.default_id(), student_id: student})
  end
end
