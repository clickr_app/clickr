defmodule Clickr.Lessons.Workflows.QuestionToTeams do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.Lessons.Events
  alias Clickr.Lessons.Workflows.QuestionToTeams.MappingAgent
  import Clickr.Gettext

  def handle(%Events.CalledTheRoll{} = e, _metadata) do
    message =
      Clickr.Buttons.Teams.Messages.button_card(%{
        title: gettext("Called the Roll"),
        button_label: gettext("I'm here!"),
        action: "clicked"
      })

    Task.start(fn -> send_button_card(e, message) end)
  end

  def handle(%Events.AskedQuestion{} = e, _metadata) do
    message =
      Clickr.Buttons.Teams.Messages.button_card(%{
        title: gettext("Asked Question"),
        button_label: gettext("I know the answer!"),
        action: "clicked"
      })

    Task.start(fn -> send_button_card(e, message) end)
  end

  def handle(%Events.Ended{} = e, _metadata) do
    MappingAgent.delete(e.id)
    :ok
  end

  defp send_button_card(%{id: id}, message) do
    case MappingAgent.get(id) do
      nil ->
        :ok

      teams_activity ->
        BotFramework.Client.send_message(message, teams_activity)
        :ok
    end
  end
end
