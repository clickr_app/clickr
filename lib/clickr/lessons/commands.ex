defmodule Clickr.Lessons.Commands do
  use Clickr.Mixins.Commands

  field(:students, presence: true)
  field(:class_id, presence: true, uuid: true, seating_plan_exists: true)
  field(:room_id, presence: true, uuid: true, seating_plan_exists: true)
  field(:seating_plan_id, presence: true, uuid: true)
  field(:student_id, presence: true, uuid: true)
  field(:method, presence: true, inclusion: Clickr.Lessons.GradingMethods.known())
  field(:opts, presence: true)
  field(:button_mapping, presence: true)
  field(:delta, presence: true)

  command(:AddPointForAll, [:delta])
  command(:AdjustPoints, [:student_id, :delta])
  command(:AskQuestion)
  command(:CallTheRoll)
  command(:Delete, [])
  command(:End)
  command(:Grade, [:method, :opts])
  command(:NoteAttendance)
  command(:NoteLateArrival, [:student_id])
  command(:NoteNotAttending, [:student_id])
  command(:NoteRaisedHands)
  command(:RaiseHand, [:student_id])

  command(:Start, [:class_id, :room_id, :seating_plan_id, :students, :button_mapping])
end

defimpl Clickr.Protocols.CommandEnrichment, for: Clickr.Lessons.Commands.Start do
  alias Clickr.{Buttons, SeatingPlans}

  def enrich(c) do
    c
    |> Clickr.Mixins.Commands.generate_id()
    |> generate_existing_seating_plan_id()
    |> load_students_from_seating_plan()
    |> load_button_mapping_from_aggregates()
  end

  defp generate_existing_seating_plan_id(c) do
    Clickr.Map.put_if_nil_lazy(c, :seating_plan_id, fn ->
      id = SeatingPlans.id(c)

      case SeatingPlans.by_id(SeatingPlans.id(c)) do
        nil -> nil
        _seating_plan -> id
      end
    end)
  end

  defp load_students_from_seating_plan(%{seating_plan_id: nil} = c), do: c

  defp load_students_from_seating_plan(%{seating_plan_id: id} = c) do
    Clickr.Map.put_if_nil_lazy(c, :students, fn ->
      %{students: students} = SeatingPlans.by_id!(id)
      Enum.map(students, & &1.id)
    end)
  end

  defp load_button_mapping_from_aggregates(%{seating_plan_id: nil} = c), do: c

  defp load_button_mapping_from_aggregates(c) do
    Clickr.Map.put_if_nil_lazy(c, :button_mapping, fn ->
      SeatingPlans.Buttons.map(c)
      |> Map.merge(Buttons.Teams.ByName.map(c))
    end)
  end
end
