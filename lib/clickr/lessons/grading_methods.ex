defmodule Clickr.Lessons.GradingMethods do
  alias Clickr.Lessons.GradingMethods.{Linear}
  alias Clickr.Lessons.Aggregates.Lesson

  def known(), do: ["linear"]

  def grade("linear", %{} = opts, points) when is_integer(points), do: Linear.grade(opts, points)

  def initial_opts("linear", %Lesson{} = lesson), do: Linear.initial_opts(lesson)

  def parse_opts("linear", %{} = opts), do: Linear.parse_opts(opts)
end
