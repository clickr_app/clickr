defmodule Clickr.Lessons.Supervisor do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        Clickr.Lessons.Workflows.AddGrades,
        Clickr.Lessons.Projectors.Lesson,
        Clickr.Lessons.Workflows.ButtonToStudent,
        Clickr.Lessons.Workflows.ButtonToStudent.Supervisor,
        Clickr.Lessons.Workflows.QuestionToTeams,
        Clickr.Lessons.Workflows.QuestionToTeams.MappingAgent,
        Clickr.Lessons.Workflows.RemoveGrades
      ],
      strategy: :one_for_one
    )
  end
end
