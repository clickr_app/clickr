defmodule Clickr.Grades.Grade do
  alias Clickr.Lessons.GradingMethods

  def grade(:linear, opts, points), do: GradingMethods.Linear.grade(opts, points)
end
