defmodule Clickr.Lessons.Projectors.Lesson do
  use Clickr.Mixins.Projector
  alias Clickr.{Classes, Lessons, Rooms, SeatingPlans}
  alias Clickr.Lessons.{Events, Projections}

  project(%Events.Started{} = e, fn multi ->
    class = Classes.by_id!(e.class_id)
    room = Rooms.by_id!(e.room_id)
    seating_plan = SeatingPlans.by_id!(SeatingPlans.id(e))
    names = Map.new(seating_plan.students, &{&1.id, &1.name})

    students =
      Enum.map(seating_plan.positions, fn {id, %{"x" => x, "y" => y}} ->
        %Projections.Lesson.Student{id: id, name: names[id], x: x, y: y}
      end)

    Ecto.Multi.insert(multi, :insert, %Projections.Lesson{
      id: e.id,
      status: e.status,
      button_mapping: e.button_mapping,
      class_id: e.class_id,
      room_id: e.room_id,
      class_name: class.name,
      room_name: room.name,
      width: room.width,
      height: room.height,
      students: students
    })
  end)

  project(%Events.NotedAttendance{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [status: e.status, attending: e.raised_hands]
    )
  end)

  project(%Events.NotedLateArrival{} = e, fn multi ->
    lesson = Lessons.by_id!(e.id)
    attending = [e.student_id | lesson.attending]

    changeset =
      lesson
      |> Ecto.Changeset.change()
      |> Ecto.Changeset.put_change(:attending, attending)

    Ecto.Multi.update(multi, :update, changeset)
  end)

  project(%Events.NotedNotAttending{} = e, fn multi ->
    lesson = Lessons.by_id!(e.id)
    attending = Enum.reject(lesson.attending, fn id -> id == e.student_id end)

    changeset =
      lesson
      |> Ecto.Changeset.change()
      |> Ecto.Changeset.put_change(:attending, attending)

    Ecto.Multi.update(multi, :update, changeset)
  end)

  project(%Events.Graded{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [status: e.status, grades: e.grades])
  end)

  project(%Events.Deleted{} = e, fn multi ->
    Ecto.Multi.delete_all(multi, :delete, by_id(e.id))
  end)

  project(%Classes.Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_class_id(e.id), set: [class_name: e.name])
  end)

  project(%Rooms.Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_room_id(e.id), set: [room_name: e.name])
  end)

  @status_only_events [
    Events.AdjustedPoints,
    Events.AskedQuestion,
    Events.CalledTheRoll,
    Events.Ended,
    Events.NotedRaisedHands,
    Events.RaisedHand
  ]

  for event_type <- @status_only_events do
    project(%unquote(event_type){} = e, fn multi ->
      Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [status: e.status])
    end)
  end

  defp by_id(id), do: from(l in Projections.Lesson, where: l.id == ^id)
  defp by_class_id(id), do: from(l in Projections.Lesson, where: l.class_id == ^id)
  defp by_room_id(id), do: from(l in Projections.Lesson, where: l.room_id == ^id)
end
