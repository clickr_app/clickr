defmodule Clickr.Lessons.Queries.ById do
  use Clickr.Mixins.Queries.ById, schema: Clickr.Lessons.Projections.Lesson
end
