defmodule Clickr.Lessons.Queries.List do
  use Clickr.Mixins.Queries.List, schema: Clickr.Lessons.Projections.Lesson
end
