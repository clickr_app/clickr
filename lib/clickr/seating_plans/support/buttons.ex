defmodule Clickr.SeatingPlans.Buttons do
  def map(%{room_id: room_id, seating_plan_id: seating_plan_id})
      when is_binary(room_id) and is_binary(seating_plan_id) do
    %{positioned: students} = Clickr.SeatingPlans.aggregate_state(seating_plan_id)
    %{buttons: buttons} = Clickr.Rooms.aggregate_state(room_id)

    map(%{buttons: buttons, students: students})
  end

  def map(%{buttons: %{} = btn_to_pos, students: %{} = std_to_pos}) do
    pos_to_std = invert(std_to_pos)

    btn_to_pos
    |> Enum.map(fn {btn, pos} -> {btn, pos_to_std[pos]} end)
    |> Enum.filter(fn {_btn, std} -> not is_nil(std) end)
    |> Map.new()
  end

  defp invert(%{} = map), do: Map.new(map, fn {k, v} -> {v, k} end)
end
