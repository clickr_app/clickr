defmodule Clickr.SeatingPlans.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "seating_plan"

  event(:AddedStudent, [:student_id])
  event(:Created, [:class_id, :room_id, :students])
  event(:Deleted)
  event(:PositionedStudent, [:student_id, :x, :y])
  event(:RemovedStudent, [:student_id])
  event(:UnpositionedStudent, [:student_id])
end
