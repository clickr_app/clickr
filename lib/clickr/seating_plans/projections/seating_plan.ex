defmodule Clickr.SeatingPlans.Projections.SeatingPlan do
  use Clickr.Mixins.Projection, projection_table: "seating_plans_seating_plan"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :class_id, :binary_id
    field :room_id, :binary_id

    field :class_name, :string
    field :room_name, :string

    embeds_many :students, Student, primary_key: {:id, :binary_id, autogenerate: false} do
      field :name, :string
    end

    field :positions, :map, default: %{}

    timestamps()
  end
end
