defmodule Clickr.SeatingPlans.Projectors.SeatingPlan do
  use Clickr.Mixins.Projector
  alias Clickr.{Classes, Rooms, SeatingPlans, Students}
  alias Clickr.SeatingPlans.{Events, Projections}

  project(%Events.Created{} = e, fn multi ->
    class = Classes.by_id!(e.class_id)
    room = Rooms.by_id!(e.room_id)
    students = Enum.map(e.students, &get_student_embed/1)

    Ecto.Multi.insert(multi, :insert, %Projections.SeatingPlan{
      id: e.id,
      class_id: e.class_id,
      room_id: e.room_id,
      class_name: class.name,
      room_name: room.name,
      students: students,
      positions: %{}
    })
  end)

  project(%Events.AddedStudent{} = e, fn multi ->
    old = by_id(e.id) |> Clickr.Repo.one!()

    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [students: [get_student_embed(e.student_id) | old.students]]
    )
  end)

  project(%Events.RemovedStudent{} = e, fn multi ->
    old = by_id(e.id) |> Clickr.Repo.one!()

    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [students: Enum.reject(old.students, &(&1.id == e.student_id))]
    )
  end)

  project(%Events.PositionedStudent{} = e, fn multi ->
    old = by_id(e.id) |> Clickr.Repo.one!()

    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [positions: Map.put(old.positions, e.student_id, %{x: e.x, y: e.y})]
    )
  end)

  project(%Events.UnpositionedStudent{} = e, fn multi ->
    old = by_id(e.id) |> Clickr.Repo.one!()

    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [positions: Map.delete(old.positions, e.student_id)]
    )
  end)

  project(%Events.Deleted{} = e, fn multi ->
    Ecto.Multi.delete_all(multi, :delete, by_id(e.id))
  end)

  project(%Classes.Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_class_id(e.id), set: [class_name: e.name])
  end)

  project(%Rooms.Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_room_id(e.id), set: [room_name: e.name])
  end)

  project(%Students.Events.Renamed{} = e, fn multi ->
    student = Students.by_id!(e.id)
    {seating_plans, _} = SeatingPlans.list(where: [class_id: student.class_id])

    for seating_plan <- seating_plans, reduce: multi do
      multi ->
        students =
          seating_plan.students
          |> Enum.map(fn s -> if s.id == e.id, do: %{id: e.id, name: e.name}, else: s end)

        changeset = Ecto.Changeset.change(seating_plan, students: students)

        Ecto.Multi.update(multi, :update, changeset)
    end
  end)

  defp get_student_embed(id) do
    student = Students.by_id!(id)
    %Projections.SeatingPlan.Student{id: student.id, name: student.name}
  end

  defp by_id(id), do: from(s in Projections.SeatingPlan, where: s.id == ^id)
  defp by_class_id(id), do: from(s in Projections.SeatingPlan, where: s.class_id == ^id)
  defp by_room_id(id), do: from(s in Projections.SeatingPlan, where: s.room_id == ^id)
end
