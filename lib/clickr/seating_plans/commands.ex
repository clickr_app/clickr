defmodule Clickr.SeatingPlans.Commands do
  use Clickr.Mixins.Commands

  field(:class_id, presence: true, uuid: true, seating_plan_does_not_exist: true)
  field(:room_id, presence: true, uuid: true, seating_plan_does_not_exist: true)
  field(:student_id, presence: true, uuid: true)
  # presence requires non-emtpy list
  field(:students, [])
  field(:x, [presence: true] ++ Clickr.Vex.x())
  field(:y, [presence: true] ++ Clickr.Vex.y())

  command(:AddStudent, [:student_id])
  command(:Create, [:class_id, :room_id, :students])
  command(:Delete, [])
  command(:PositionStudent, [:student_id, :x, :y])
  command(:RemoveStudent, [:student_id])
  command(:UnpositionStudent, [:student_id])
end

defimpl Clickr.Protocols.CommandEnrichment, for: Clickr.SeatingPlans.Commands.Create do
  alias Clickr.{Classes, SeatingPlans}

  def enrich(c) do
    c
    |> generate_id_from_class_and_room()
    |> add_students_from_class_read_model()
  end

  defp generate_id_from_class_and_room(c),
    do: Clickr.Map.put_if_nil_lazy(c, :id, fn -> SeatingPlans.id(c) end)

  defp add_students_from_class_read_model(c),
    do: Clickr.Map.put_if_nil_lazy(c, :students, fn -> Classes.by_id!(c.class_id).student_ids end)
end
