defmodule Clickr.SeatingPlans.Aggregates.SeatingPlan do
  defstruct [:id, students: [], positioned: %{}, unpositioned: [], deleted?: false]
  @namespace "da1ac6de-3e84-11ec-bd9a-172f922dbf43"

  alias Clickr.SeatingPlans.{Commands, Events}
  alias __MODULE__, as: A

  use Clickr.Mixins.Aggregate,
    prefix: "seating-plan",
    with_deleted: [command: Commands.Delete, event: Events.Deleted]

  # Public API

  def id(%{class_id: class_id, room_id: room_id}) do
    UUID.uuid5(@namespace, "#{class_id}/#{room_id}")
  end

  # Command handlers

  def execute(%A{id: nil}, %Commands.Create{} = c) do
    %Events.Created{id: c.id, class_id: c.class_id, room_id: c.room_id, students: c.students}
  end

  def execute(%A{id: nil}, _c) do
    {:error, :not_created}
  end

  def execute(%A{} = a, %Commands.AddStudent{} = c) do
    with :ok <- student_not_added?(a, c) do
      %Events.AddedStudent{id: c.id, student_id: c.student_id}
    end
  end

  def execute(%A{} = a, %Commands.RemoveStudent{} = c) do
    with :ok <- student_added?(a, c) do
      %Events.RemovedStudent{id: c.id, student_id: c.student_id}
    end
  end

  def execute(%A{} = a, %Commands.PositionStudent{} = c) do
    with :ok <- student_added?(a, c),
         :ok <- position_empty?(a, c) do
      %Events.PositionedStudent{id: c.id, student_id: c.student_id, x: c.x, y: c.y}
    end
  end

  def execute(%A{} = a, %Commands.UnpositionStudent{} = c) do
    with :ok <- student_positioned?(a, c) do
      %Events.UnpositionedStudent{id: c.id, student_id: c.student_id}
    end
  end

  # Event handlers

  def apply(%A{} = a, %Events.Created{} = e) do
    %A{a | id: e.id, students: e.students}
  end

  def apply(%A{} = a, %Events.AddedStudent{} = e) do
    a
    |> Map.update!(:students, &[e.student_id | &1])
    |> Map.update!(:unpositioned, &[e.student_id | &1])
  end

  def apply(%A{} = a, %Events.RemovedStudent{} = e) do
    a
    |> Map.update!(:students, &List.delete(&1, e.student_id))
    |> Map.update!(:positioned, &Map.delete(&1, e.student_id))
    |> Map.update!(:unpositioned, &List.delete(&1, e.student_id))
  end

  def apply(%A{} = a, %Events.PositionedStudent{} = e) do
    a
    |> Map.update!(:unpositioned, &List.delete(&1, e.student_id))
    |> Map.update!(:positioned, &Map.put(&1, e.student_id, {e.x, e.y}))
  end

  def apply(%A{} = a, %Events.UnpositionedStudent{} = e) do
    a
    |> Map.update!(:positioned, &Map.delete(&1, e.student_id))
  end

  # Private

  defp student_added?(%A{} = a, %{student_id: student}) do
    case student in a.students do
      true -> :ok
      false -> {:error, :student_not_added}
    end
  end

  defp student_not_added?(%A{} = a, %{student_id: student}) do
    case student in a.students do
      true -> {:error, :student_already_added}
      false -> :ok
    end
  end

  defp position_empty?(%A{} = a, %{x: x, y: y}) do
    case Enum.any?(a.positioned, fn {_, p} -> p == {x, y} end) do
      true -> {:error, :position_occupied}
      false -> :ok
    end
  end

  defp student_positioned?(%A{} = a, %{student_id: student}) do
    case Map.has_key?(a.positioned, student) do
      true -> :ok
      false -> {:error, :student_not_positioned}
    end
  end
end
