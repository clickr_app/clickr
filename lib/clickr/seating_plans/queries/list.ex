defmodule Clickr.SeatingPlans.Queries.List do
  import Ecto.Query

  def run(opts \\ []) do
    query = query(opts)
    {entries(query, opts), total_count(query, opts)}
  end

  defp query(opts) do
    Clickr.SeatingPlans.Projections.SeatingPlan
    |> where(^Keyword.get(opts, :where, []))
    |> where(
      [s],
      ilike(s.class_name, ^"#{Keyword.get(opts, :query, "")}%") or
        ilike(s.room_name, ^"#{Keyword.get(opts, :query, "")}%")
    )
  end

  defp entries(query, opts) do
    query
    |> limit(^opts[:limit])
    |> offset(^opts[:offset])
    |> Clickr.Repo.all()
  end

  defp total_count(query, _opts) do
    query
    |> Clickr.Repo.aggregate(:count)
  end
end
