defmodule Clickr.SeatingPlans.Queries.Exists do
  use Clickr.Mixins.Queries.Exists, schema: Clickr.SeatingPlans.Projections.SeatingPlan
end
