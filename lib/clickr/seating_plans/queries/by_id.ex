defmodule Clickr.SeatingPlans.Queries.ById do
  use Clickr.Mixins.Queries.ById, schema: Clickr.SeatingPlans.Projections.SeatingPlan
end
