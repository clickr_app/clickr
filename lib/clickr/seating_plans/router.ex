defmodule Clickr.SeatingPlans.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.SeatingPlans.Aggregates.SeatingPlan,
    commands: Clickr.SeatingPlans.Commands
end
