defmodule Clickr.Grades.Queries.ById do
  use Clickr.Mixins.Queries.ById, schema: Clickr.Grades.Projections.Grade
end
