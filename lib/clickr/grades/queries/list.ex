defmodule Clickr.Grades.Queries.List do
  use Clickr.Mixins.Queries.List, schema: Clickr.Grades.Projections.Grade
end
