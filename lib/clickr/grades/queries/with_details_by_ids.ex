defmodule Clickr.Grades.Queries.WithDetailsByIds do
  import Ecto.Query
  alias Clickr.Grades.{Projections}

  def run(ids) do
    Projections.GradeWithDetails
    |> where([g], g.id in ^ids)
    |> Clickr.Repo.all()
  end
end
