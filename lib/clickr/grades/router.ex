defmodule Clickr.Grades.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.Grades.Aggregates.Grade,
    commands: Clickr.Grades.Commands
end
