defmodule Clickr.Grades.Systems.German do
  ranges = [
    {98, "1+"},
    {95, "1"},
    {92, "1-"},
    {87, "2+"},
    {82, "2"},
    {77, "2-"},
    {72, "3+"},
    {68, "3"},
    {62, "3-"},
    {57, "4+"},
    {52, "4"},
    {47, "4-"},
    {42, "5+"},
    {37, "5"},
    {25, "5-"},
    {0, "6"}
  ]

  for {min, value} <- ranges do
    def format(percent) when percent >= unquote(min), do: unquote(value)
  end

  def format(_percent), do: nil
end
