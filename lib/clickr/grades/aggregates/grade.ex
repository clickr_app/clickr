defmodule Clickr.Grades.Aggregates.Grade do
  defstruct [
    :id,
    :student_id,
    :calculation_method,
    :calculation_opts,
    :calculated,
    lessons: %{},
    bonus: [],
    deleted?: false
  ]

  @namespace "fc7b6ba2-3e84-11ec-bca2-937a578a792d"

  alias Clickr.Grades.{CalculationMethods, Commands, Events}
  alias __MODULE__, as: A

  use Clickr.Mixins.Aggregate,
    prefix: "grade",
    with_deleted: [command: Commands.Delete, event: Events.Deleted]

  # Public API

  def id(%{student_id: student_id, grading_interval_id: grading_interval_id}) do
    UUID.uuid5(@namespace, "#{student_id}/#{grading_interval_id}")
  end

  def calculate(%A{} = a, overrides \\ []) do
    a = Map.merge(a, Map.new(overrides))

    CalculationMethods.calculate(
      a.calculation_method,
      a.calculation_opts,
      Map.values(a.lessons),
      a.bonus
    )
  end

  # Command handlers

  def execute(%A{id: nil}, %Commands.Create{} = c) do
    %Events.Created{
      id: c.id,
      student_id: c.student_id,
      grading_interval_id: c.grading_interval_id,
      calculation_method: c.calculation_method,
      calculation_opts: c.calculation_opts
    }
  end

  def execute(%A{id: nil}, _c) do
    {:error, :not_created}
  end

  def execute(%A{} = a, %Commands.AddLessonGrade{} = c) do
    with :ok <- lesson_not_added?(a, c) do
      calculated = calculate(a, lessons: Map.put(a.lessons, c.lesson_id, c.percent))

      [
        %Events.AddedLessonGrade{id: c.id, lesson_id: c.lesson_id, percent: c.percent},
        %Events.Changed{id: c.id, calculated: calculated}
      ]
    end
  end

  def execute(%A{} = a, %Commands.AddBonusGrade{} = c) do
    calculated = calculate(a, bonus: [c.percent | a.bonus])

    [
      %Events.AddedBonusGrade{id: c.id, percent: c.percent},
      %Events.Changed{id: c.id, calculated: calculated}
    ]
  end

  def execute(%A{} = a, %Commands.RemoveLessonGrade{} = c) do
    with :ok <- lesson_added?(a, c) do
      calculated = calculate(a, lessons: Map.delete(a.lessons, c.lesson_id))

      [
        %Events.RemovedLessonGrade{id: c.id, lesson_id: c.lesson_id},
        %Events.Changed{id: c.id, calculated: calculated}
      ]
    end
  end

  # Event handlers

  def apply(%A{} = a, %Events.Created{} = e) do
    %A{
      a
      | id: e.id,
        calculation_method: e.calculation_method,
        calculation_opts: e.calculation_opts
    }
  end

  def apply(%A{} = a, %Events.AddedLessonGrade{} = e) do
    %A{a | lessons: Map.put(a.lessons, e.lesson_id, e.percent)}
  end

  def apply(%A{} = a, %Events.AddedBonusGrade{} = e) do
    %A{a | bonus: [e.percent | a.bonus]}
  end

  def apply(%A{} = a, %Events.RemovedLessonGrade{} = e) do
    %A{a | lessons: Map.delete(a.lessons, e.lesson_id)}
  end

  def apply(%A{} = a, %Events.Changed{} = e) do
    %A{a | calculated: e.calculated}
  end

  # Private

  defp lesson_not_added?(%A{lessons: lessons}, %{lesson_id: lesson}) do
    case Map.has_key?(lessons, lesson) do
      true -> {:error, :lesson_already_added}
      false -> :ok
    end
  end

  defp lesson_added?(%A{lessons: lessons}, %{lesson_id: lesson}) do
    case Map.has_key?(lessons, lesson) do
      true -> :ok
      false -> {:error, :lesson_not_added}
    end
  end
end
