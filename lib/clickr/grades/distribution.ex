defmodule Clickr.Grades.Distribution do
  @spec buckets([float], integer) :: %{{float, float} => integer}
  def buckets(grades, number)

  def buckets(_grades, 0), do: %{}

  def buckets(grades, number) when is_list(grades) and is_integer(number) and number > 0 do
    width = 100.0 / number
    index = fn grade -> Enum.min([trunc((grade || 0) / width), number - 1]) end

    counts =
      grades
      |> Enum.group_by(index)
      |> Map.new(fn {n, grades} -> {n, length(grades)} end)

    0..(number - 1)
    |> Map.new(fn i -> {{i * width, (i + 1) * width}, counts[i] || 0} end)
  end
end
