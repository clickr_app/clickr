defmodule Clickr.Grades.CalculationMethods.LessonBonusAverage do
  defstruct []

  def grade(opts, lessons, bonus) do
    grade(opts, lessons ++ bonus)
  end

  def grade(%{} = _opts, []) do
    0
  end

  def grade(%{} = _opts, percentages) do
    Enum.sum(percentages) / Enum.count(percentages)
  end
end
