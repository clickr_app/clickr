defmodule Clickr.Grades.Commands do
  use Clickr.Mixins.Commands

  field(:student_id, presence: true, uuid: true)
  field(:grading_interval_id, presence: true, uuid: true)

  field(:calculation_method,
    presence: true,
    inclusion: Clickr.Grades.CalculationMethods.known()
  )

  field(:calculation_opts)
  field(:lesson_id, presence: true, uuid: true)
  field(:percent, [presence: true] ++ Clickr.Vex.percent())

  command(:AddBonusGrade, [:percent])
  command(:AddLessonGrade, [:lesson_id, :percent])
  command(:Create, [:student_id, :grading_interval_id, :calculation_method, :calculation_opts])
  command(:Delete, [])
  command(:RemoveLessonGrade, [:lesson_id])
end

defimpl Clickr.Protocols.CommandEnrichment, for: Clickr.Grades.Commands.Create do
  alias Clickr.Grades

  def enrich(c) do
    c
    |> Clickr.Map.put_if_nil(:calculation_method, "lesson_average")
    |> Clickr.Map.put_if_nil(:calculation_opts, %{})
    |> generate_id_from_student_and_grading_interval()
  end

  defp generate_id_from_student_and_grading_interval(c),
    do: Clickr.Map.put_if_nil_lazy(c, :id, fn -> Grades.id(c) end)
end
