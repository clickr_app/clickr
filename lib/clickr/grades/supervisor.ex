defmodule Clickr.Grades.Supervisor do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        Clickr.Grades.Projectors.Grade,
        Clickr.Grades.Projectors.GradeWithDetails
      ],
      strategy: :one_for_one
    )
  end
end
