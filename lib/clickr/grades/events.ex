defmodule Clickr.Grades.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "grade"

  event(:AddedBonusGrade, [:percent])
  event(:AddedLessonGrade, [:lesson_id, :percent])
  event(:Changed, [:calculated])
  event(:Created, [:student_id, :grading_interval_id, :calculation_method, :calculation_opts])
  event(:Deleted, [])
  event(:RemovedLessonGrade, [:lesson_id])
end
