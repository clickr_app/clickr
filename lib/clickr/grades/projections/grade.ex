defmodule Clickr.Grades.Projections.Grade do
  use Clickr.Mixins.Projection, projection_table: "grades_grade"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :student_id, :binary_id
    field :grading_interval_id, :binary_id
    field :calculated, :float

    embeds_many :lessons, Lesson, primary_key: {:id, :binary_id, autogenerate: false} do
      field :percent, :float
    end

    field :bonus, {:array, :float}
    timestamps()
  end
end
