defmodule Clickr.Grades.Projections.GradeWithDetails do
  use Clickr.Mixins.Projection, projection_table: "grades_grade_with_details"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :student_id, :binary_id
    field :grading_interval_id, :binary_id
    field :calculated, :float

    embeds_many :lessons, Lesson, primary_key: {:id, :binary_id, autogenerate: false} do
      field :percent, :float
    end

    field :bonus, {:array, :float}
    timestamps()
  end
end
