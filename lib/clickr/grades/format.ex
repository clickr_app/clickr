defmodule Clickr.Grades.Format do
  alias Clickr.Grades.Systems.{German}

  def format(_, nil), do: nil
  def format(:german, percent), do: German.format(percent) || fallback(percent)
  def format(_, percent), do: fallback(percent)

  defp fallback(percent), do: "#{percent}%"
end
