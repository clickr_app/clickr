defmodule Clickr.Grades.Projectors.GradeWithDetails do
  use Clickr.Mixins.Projector
  alias Clickr.Grades.{Events, Projections}

  project(%Events.Created{} = e, fn multi ->
    Ecto.Multi.insert(multi, :insert, %Projections.GradeWithDetails{
      id: e.id,
      grading_interval_id: e.grading_interval_id,
      student_id: e.student_id,
      calculated: 0.0,
      lessons: [],
      bonus: []
    })
  end)

  project(%Events.Changed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [calculated: e.calculated])
  end)

  project(%Events.Deleted{} = e, fn multi ->
    Ecto.Multi.delete_all(multi, :delete, by_id(e.id))
  end)

  project(%Events.AddedLessonGrade{} = e, fn multi ->
    old = Clickr.Repo.get!(Projections.GradeWithDetails, e.id)

    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [
        lessons:
          old.lessons ++
            [%Projections.GradeWithDetails.Lesson{id: e.lesson_id, percent: e.percent}]
      ]
    )
  end)

  project(%Events.RemovedLessonGrade{} = e, fn multi ->
    old = Clickr.Repo.get!(Projections.GradeWithDetails, e.id)

    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [lessons: Enum.reject(old.lessons, fn l -> l.id == e.lesson_id end)]
    )
  end)

  project(%Events.AddedBonusGrade{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), push: [bonus: e.percent])
  end)

  defp by_id(id), do: from(c in Projections.GradeWithDetails, where: c.id == ^id)
end
