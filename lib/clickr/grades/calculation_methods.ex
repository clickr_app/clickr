defmodule Clickr.Grades.CalculationMethods do
  alias Clickr.Grades.CalculationMethods.{LessonBonusAverage}

  def known(), do: ["lesson_average"]

  def calculate("lesson_average", opts, lessons, bonus),
    do: LessonBonusAverage.grade(opts, lessons, bonus)
end
