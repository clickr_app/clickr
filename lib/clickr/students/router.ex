defmodule Clickr.Students.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.Students.Aggregates.Student,
    commands: Clickr.Students.Commands
end
