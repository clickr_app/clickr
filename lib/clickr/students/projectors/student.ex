defmodule Clickr.Students.Projectors.Student do
  use Clickr.Mixins.Projector
  alias Clickr.{Grades}
  alias Clickr.Students.{Events, Projections}

  project(%Events.Created{} = e, fn multi ->
    Ecto.Multi.insert(multi, :insert, %Projections.Student{
      id: e.id,
      name: e.name,
      class_id: e.class_id,
      current_grade_id: nil,
      current_grade: 0.0
    })
  end)

  project(%Grades.Events.Created{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.student_id), set: [current_grade_id: e.id])
  end)

  project(%Grades.Events.Changed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_current_grade_id(e.id),
      set: [current_grade: e.calculated]
    )
  end)

  project(%Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [name: e.name])
  end)

  project(%Events.RemovedFromClass{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [class_id: nil])
  end)

  project(%Events.Deleted{} = e, fn multi ->
    Ecto.Multi.delete_all(multi, :delete, by_id(e.id))
  end)

  defp by_id(id), do: from(c in Projections.Student, where: c.id == ^id)

  defp by_current_grade_id(id),
    do: from(c in Projections.Student, where: c.current_grade_id == ^id)
end
