defmodule Clickr.Students.Queries.ByIds do
  import Ecto.Query
  alias Clickr.Students.{Projections}

  def run(ids) do
    Projections.Student
    |> where([s], s.id in ^ids)
    |> Clickr.Repo.all()
  end
end
