defmodule Clickr.Students.Queries.List do
  use Clickr.Mixins.Queries.List, schema: Clickr.Students.Projections.Student, query_field: :name
end
