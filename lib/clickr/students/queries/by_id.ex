defmodule Clickr.Students.Queries.ById do
  use Clickr.Mixins.Queries.ById, schema: Clickr.Students.Projections.Student
end
