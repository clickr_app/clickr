defmodule Clickr.Students.Projections.Student do
  use Clickr.Mixins.Projection, projection_table: "students_student"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :name, :string
    field :class_id, :binary_id
    field :current_grade_id, :binary_id
    field :current_grade, :float
    timestamps()
  end
end
