defmodule Clickr.Students.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "student"

  event(:Created, [:name, :class_id])
  event(:Deleted, [])
  event(:RemovedFromClass)
  event(:Renamed, [:name])
end
