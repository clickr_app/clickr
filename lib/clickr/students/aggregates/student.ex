defmodule Clickr.Students.Aggregates.Student do
  defstruct [:id, deleted?: false]

  alias Clickr.Students.{Commands, Events}
  alias __MODULE__, as: A

  use Clickr.Mixins.Aggregate,
    prefix: "student",
    with_deleted: [command: Commands.Delete, event: Events.Deleted]

  # Command handlers

  def execute(%A{id: nil}, %Commands.Create{} = c) do
    %Events.Created{id: c.id, name: c.name, class_id: c.class_id}
  end

  def execute(%A{id: nil}, _c) do
    {:error, :not_created}
  end

  def execute(%A{}, %Commands.Rename{} = c) do
    %Events.Renamed{id: c.id, name: c.name}
  end

  def execute(%A{}, %Commands.RemoveFromClass{} = c) do
    %Events.RemovedFromClass{id: c.id}
  end

  # Event handlers

  def apply(%A{} = a, %Events.Created{} = e) do
    %A{a | id: e.id}
  end

  def apply(%A{} = a, %Events.Renamed{}) do
    a
  end

  def apply(%A{} = a, %Events.RemovedFromClass{}) do
    a
  end
end
