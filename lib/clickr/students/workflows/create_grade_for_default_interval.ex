defmodule Clickr.Students.Workflows.CreateGradeForDefaultInterval do
  # start_from: :current added due to renaming of event handler
  use Clickr.Mixins.Workflow, consistency: :strong, start_from: :current
  alias Clickr.{Accounts, Grades, GradingIntervals}
  alias Clickr.Students.Events

  @impl true
  def handle(%Events.Created{} = e, metadata) do
    user = Accounts.system_user()
    attrs = [grading_interval_id: GradingIntervals.default_id(), student_id: e.id]
    opts = with_cause(metadata, consistency: :eventual)

    {:ok, _} = Grades.create(user, attrs, opts)
    :ok
  end
end
