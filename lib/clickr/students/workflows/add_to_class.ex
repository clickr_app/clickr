defmodule Clickr.Students.Workflows.AddToClass do
  use Clickr.Mixins.Workflow, consistency: :strong
  alias Clickr.{Accounts, Classes}
  alias Clickr.Students.Events

  @impl true
  def handle(%Events.Created{} = e, metadata) do
    user = Accounts.system_user()
    attrs = [id: e.class_id, student_id: e.id]
    opts = with_cause(metadata, consistency: :strong)

    {:ok, _} = Classes.add_student(user, attrs, opts)
    :ok
  end
end
