defmodule Clickr.Students.Workflows.RemoveFromClass do
  use Clickr.Mixins.Workflow, consistency: :strong
  alias Clickr.{Accounts, Classes, Students}
  alias Clickr.Students.Events

  @impl true
  def handle(%Events.Deleted{} = e, metadata) do
    user = Accounts.system_user()
    %{class_id: class_id} = Students.by_id!(e.id)
    attrs = [id: class_id, student_id: e.id]
    opts = with_cause(metadata, consistency: :strong)

    {:ok, _} = Classes.remove_student(user, attrs, opts)
    :ok
  end
end
