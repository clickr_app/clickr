defmodule Clickr.Students.Workflows.DeleteGrades do
  use Clickr.Mixins.Workflow, consistency: :eventual
  alias Clickr.{Accounts, Grades}
  alias Clickr.Students.Events

  @impl true
  def handle(%Events.Deleted{} = e, metadata) do
    {grades, _count} = Grades.list(where: [student_id: e.id])
    opts = with_cause(metadata, consistency: :eventual)

    for grade <- grades do
      Task.start(fn -> delete(grade, opts) end)
    end

    :ok
  end

  defp delete(grade, opts) do
    user = Accounts.system_user()
    attrs = [id: grade.id]
    {:ok, _} = Grades.delete(user, attrs, opts)
  end
end
