defmodule Clickr.Students.Supervisor do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        Clickr.Students.Projectors.Student,
        Clickr.Students.Workflows.AddToClass,
        Clickr.Students.Workflows.CreateGradeForDefaultInterval,
        Clickr.Students.Workflows.DeleteGrades,
        Clickr.Students.Workflows.RemoveFromClass
      ],
      strategy: :one_for_one
    )
  end
end
