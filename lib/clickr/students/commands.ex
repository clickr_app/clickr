defmodule Clickr.Students.Commands do
  use Clickr.Mixins.Commands

  field(:name, presence: true, length: [min: 2])
  field(:class_id, presence: true, uuid: true)

  command(:Create, [:name, :class_id], generate_id: true)
  command(:Delete, [])
  command(:RemoveFromClass, [])
  command(:Rename, [:name])
end
