defmodule Clickr.Buttons.Deconz do
  alias Clickr.Buttons.Zigbee.IkeaTradfriRemote

  def clicked(%{"modelid" => "TRADFRI remote control"} = sensor, msg) do
    IkeaTradfriRemote.deconz_clicked(sensor, msg)
  end

  def clicked(_type, _msg), do: nil
end
