defmodule Clickr.Buttons.Zigbee.IkeaTradfriRemote do
  @device_type_id "4e8b6610-4b08-11ec-8b77-2b2bbedad611"

  def home_assistant_clicked(
        %{
          "args" => [257, 13, 0],
          "command" => "press"
        } = event
      ) do
    clicked(device_ieee: event["device_ieee"], name: "left")
  end

  def home_assistant_clicked(
        %{
          "args" => [256, 13, 0],
          "command" => "press"
        } = event
      ) do
    clicked(device_ieee: event["device_ieee"], name: "right")
  end

  def home_assistant_clicked(_event), do: nil

  # https://dresden-elektronik.github.io/deconz-rest-doc/endpoints/sensors/button_events/#ikea-tradfri-round-5-button-remote
  def deconz_clicked(sensor, %{"state" => %{"buttonevent" => 4002}} = event) do
    clicked(device_ieee: event["uniqueid"], device_name: sensor["name"], name: "left")
  end

  def deconz_clicked(sensor, %{"state" => %{"buttonevent" => 5002}} = event) do
    clicked(device_ieee: event["uniqueid"], device_name: sensor["name"], name: "right")
  end

  def deconz_clicked(sensor, %{"state" => %{"buttonevent" => 1002}} = event) do
    clicked(device_ieee: event["uniqueid"], device_name: sensor["name"], name: "middle")
  end

  def deconz_clicked(_sensor, _event), do: nil

  defp clicked(opts) do
    device_ieee = opts[:device_ieee]
    device_id = opts[:device_id] || UUID.uuid5(@device_type_id, device_ieee)
    opts = Keyword.drop(opts, [:device_ieee])

    Clickr.Buttons.Events.Clicked.new(
      Keyword.merge(
        [
          id: UUID.uuid5(device_id, opts[:name]),
          device_id: device_id,
          user_id: Clickr.Accounts.system_user().id,
          device_name: "Ikea Tradfri Remote #{device_ieee || device_id}"
        ],
        opts
      )
    )
  end
end
