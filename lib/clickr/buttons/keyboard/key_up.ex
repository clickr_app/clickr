defmodule Clickr.Buttons.Keyboard.KeyUp do
  @device_id "78aa6b7c-4193-11ec-960b-e73f8c351ac2"

  alias Clickr.Accounts.User

  def clicked(%User{} = user, key) do
    %Clickr.Buttons.Events.Clicked{
      id: UUID.uuid5(@device_id, key),
      user_id: user.id,
      name: key,
      device_id: @device_id,
      device_name: "Keyboard",
      only_for_user_id: user.id
    }
  end
end
