defmodule Clickr.Buttons.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.Buttons.Aggregates.Button,
    commands: Clickr.Buttons.Commands
end
