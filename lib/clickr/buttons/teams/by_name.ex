defmodule Clickr.Buttons.Teams.ByName do
  @device_id "902afca6-4e29-11ec-932f-2367bde9ad7f"

  alias Clickr.{Accounts, SeatingPlans}

  def clicked(%{"name" => name}) do
    %Clickr.Buttons.Events.Clicked{
      id: button_id(name),
      user_id: Accounts.system_user(),
      name: name,
      device_id: @device_id,
      device_name: "Teams"
    }
  end

  def map(%{seating_plan_id: id}) do
    %{students: students} = SeatingPlans.by_id!(id)
    map(students)
  end

  def map([%{name: _, id: _} | _] = students) do
    Map.new(students, &{button_id(&1.name), &1.id})
  end

  defp button_id(name) do
    UUID.uuid5(@device_id, name)
  end
end
