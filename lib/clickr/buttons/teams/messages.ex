defmodule Clickr.Buttons.Teams.Messages do
  def button_card(%{action: action, title: title, button_label: button_label} = opts) do
    bubble =
      case opts[:bubble_url] do
        nil ->
          %{}

        url ->
          %{
            # why doesn't this show an meeting dialog box (bubble)?
            channelData: %{
              notification: %{
                alertInMeeting: true,
                externalResourceUrl:
                  "https://teams.microsoft.com/l/bubble/#{Clickr.TeamsApp.app_id()}?#{URI.encode_query(url: url, height: 200, width: 200, title: Title)}"
              }
            }
          }
      end

    %{
      type: "message",
      attachments: [
        %{
          contentType: "application/vnd.microsoft.card.adaptive",
          content: %{
            type: "AdaptiveCard",
            "$schema": "http://adaptivecards.io/schemas/adaptive-card.json",
            version: "1.0",
            body: [
              %{
                type: "TextBlock",
                text: title,
                wrap: true
              }
            ],
            actions: [
              %{
                type: "Action.Execute",
                title: button_label,
                data: %{
                  action: action
                }
              }
            ]
          }
        }
      ]
    }
    |> Map.merge(bubble)
  end
end
