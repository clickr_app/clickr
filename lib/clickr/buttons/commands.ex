defmodule Clickr.Buttons.Commands do
  use Clickr.Mixins.Commands

  field(:button_name, presence: true)
  field(:device_id, presence: true, uuid: true)
  field(:device_name, presence: true)

  command(:Register, [:name, :device_id, :device_name])
end
