defmodule Clickr.Buttons.Aggregates.Button do
  defstruct [:id, :name, :device_id, :device_name]

  use Clickr.Mixins.Aggregate, prefix: "button"
  alias Clickr.Buttons.{Commands, Events}
  alias __MODULE__, as: A

  # Allow duplicate registration
  def execute(%A{}, %Commands.Register{} = c) do
    Events.Registered.from_command(c)
  end

  def apply(%A{} = a, %Events.Registered{} = e) do
    %A{a | id: e.id, name: e.name, device_id: e.device_id, device_name: e.device_name}
  end
end
