# This is **not** a CQRS event. Clicks are ephemeral events, not domain events.
defmodule Clickr.Buttons.Events.Clicked do
  use ExConstructor

  @enforce_keys [:id, :user_id, :name, :device_id, :device_name]
  defstruct [:id, :user_id, :name, :device_id, :device_name, :only_for_user_id]
end
