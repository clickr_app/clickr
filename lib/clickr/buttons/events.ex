defmodule Clickr.Buttons.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "button"

  event(:Registered, [:name, :device_id, :device_name])
end
