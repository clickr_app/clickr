defmodule Clickr.Timer do
  use Agent
  require Logger

  def start_link(_value) do
    Agent.start_link(fn -> :erlang.timestamp() end, name: __MODULE__)
  end

  def time() do
    now = :erlang.timestamp()
    then = Agent.get_and_update(__MODULE__, fn then -> {then, now} end)
    Logger.info("Elapsed: #{:timer.now_diff(now, then)}")
  end
end
