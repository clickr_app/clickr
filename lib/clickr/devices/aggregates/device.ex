defmodule Clickr.Devices.Aggregates.Device do
  defstruct [:id, :name, :battery, deleted?: false]

  alias Clickr.Devices.{Commands, Events}
  alias __MODULE__, as: A

  use Clickr.Mixins.Aggregate,
    prefix: "device",
    with_deleted: [command: Commands.Delete, event: Events.Deleted]

  # Command handlers

  def execute(%A{id: nil}, %Commands.Upsert{} = c) do
    %Events.Created{id: c.id, gateway_id: c.gateway_id, name: c.name, battery: c.battery}
  end

  def execute(%A{} = a, %Commands.Upsert{} = c) do
    [
      if(a.name != c.name, do: %Events.Renamed{id: c.id, name: c.name}),
      if(a.battery != c.battery, do: %Events.SetBattery{id: c.id, battery: c.battery})
    ]
    |> Enum.filter(fn e -> e end)
  end

  # Event handlers

  def apply(%A{} = a, %Events.Created{} = e) do
    %A{a | id: e.id, name: e.name, battery: e.battery}
  end

  def apply(%A{} = a, %Events.Renamed{} = e) do
    %A{a | name: e.name}
  end

  def apply(%A{} = a, %Events.SetBattery{} = e) do
    %A{a | battery: e.battery}
  end

  # Deprecated
  def apply(%A{} = a, %Events.Upserted{} = e) do
    %A{a | id: e.id, name: e.name, battery: e.battery}
  end
end
