defmodule Clickr.Devices.Commands do
  use Clickr.Mixins.Commands

  field(:gateway_id, presence: true, uuid: true)
  field(:name)
  field(:battery)

  command(:Delete, [])
  command(:Upsert, [:gateway_id, :name, :battery])
end
