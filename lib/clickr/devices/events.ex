defmodule Clickr.Devices.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "device"

  event(:Created, [:name, :gateway_id, :battery])
  event(:Deleted, [])
  event(:Renamed, [:name])
  event(:SetBattery, [:battery])

  # Deprecated
  event(:Upserted, [:gateway_id, :name, :battery])
end
