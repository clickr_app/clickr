defmodule Clickr.Devices.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.Devices.Aggregates.Device,
    commands: Clickr.Devices.Commands
end
