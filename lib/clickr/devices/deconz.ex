defmodule Clickr.Devices.Deconz do
  @gateway_type_id "baab6d78-5e7b-11ec-848c-df9ec9854c05"

  def id(%{"uniqueid" => deconz_id}) do
    UUID.uuid5(@gateway_type_id, deconz_id)
  end
end
