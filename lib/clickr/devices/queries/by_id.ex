defmodule Clickr.Devices.Queries.ById do
  use Clickr.Mixins.Queries.ById, schema: Clickr.Devices.Projections.Device
end
