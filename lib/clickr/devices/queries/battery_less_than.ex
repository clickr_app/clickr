defmodule Clickr.Devices.Queries.BatteryLessThan do
  import Ecto.Query
  alias Clickr.Devices.Projections.Device
  alias Clickr.Repo

  # TODO #tenant
  def run(less_than, opts \\ []) do
    Device
    |> where([d], d.battery < ^less_than)
    |> limit(^opts[:limit])
    |> offset(^opts[:offset])
    |> order_by(^opts[:order_by])
    |> Repo.all()
  end
end
