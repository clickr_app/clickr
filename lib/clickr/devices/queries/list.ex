defmodule Clickr.Devices.Queries.List do
  use Clickr.Mixins.Queries.List, schema: Clickr.Devices.Projections.Device, query_field: :name
end
