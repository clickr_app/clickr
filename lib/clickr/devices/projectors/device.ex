defmodule Clickr.Devices.Projectors.Device do
  use Clickr.Mixins.Projector
  alias Clickr.Devices.{Events, Projections}

  # Deprecated
  project(%Events.Upserted{} = e, fn multi ->
    Ecto.Multi.insert(
      multi,
      :upsert,
      %Projections.Device{
        id: e.id,
        gateway_id: e.gateway_id,
        name: e.name,
        battery: e.battery
      },
      on_conflict: {:replace, [:name, :battery]},
      conflict_target: :id
    )
  end)

  project(%Events.Created{} = e, fn multi ->
    Ecto.Multi.insert(multi, :insert, %Projections.Device{
      id: e.id,
      gateway_id: e.gateway_id,
      name: e.name,
      battery: e.battery
    })
  end)

  project(%Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [name: e.name])
  end)

  project(%Events.SetBattery{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [battery: e.battery, updated_battery_at: NaiveDateTime.utc_now()]
    )
  end)

  project(%Events.Deleted{} = e, fn multi ->
    Ecto.Multi.delete_all(multi, :delete, by_id(e.id))
  end)

  defp by_id(id), do: from(c in Projections.Device, where: c.id == ^id)
end
