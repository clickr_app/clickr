defmodule Clickr.Devices.Projections.Device do
  use Clickr.Mixins.Projection, projection_table: "devices_device"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :gateway_id, :binary_id
    field :name, :string
    field :battery, :float
    field :updated_battery_at, :naive_datetime
    timestamps()
  end
end
