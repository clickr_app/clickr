defmodule Clickr.Buttons do
  alias Clickr.Buttons.{Aggregates, Commands, Events}

  use Clickr.Mixins.Context, aggregate: Aggregates.Button, commands: Commands, events: Events

  # Not a domain event
  def clicked(%Clickr.Accounts.User{} = user, %Events.Clicked{} = e) do
    e = Clickr.Map.put_if_nil(e, :user_id, user.id)

    if e.only_for_user_id do
      Clickr.PubSub.broadcast(only_for_user_topic(e.only_for_user_id, e.id), e)
      Clickr.PubSub.broadcast(only_for_user_topic_all(e.only_for_user_id), e)
    else
      Clickr.PubSub.broadcast(topic(e.id), e)
      Clickr.PubSub.broadcast(topic_all(), e)
    end
  end

  def clicked(%Clickr.Accounts.User{} = user, args) do
    clicked(user, Events.Clicked.new(args))
  end

  def subscribe_only_for_user(user_id, id),
    do: Clickr.PubSub.subscribe(only_for_user_topic(user_id, id))

  def subscribe_all_only_for_user(user_id),
    do: Clickr.PubSub.subscribe(only_for_user_topic_all(user_id))

  def unsubscribe_only_for_user(user_id, id),
    do: Clickr.PubSub.unsubscribe(only_for_user_topic(user_id, id))

  def unsubscribe_all_only_for_user(user_id),
    do: Clickr.PubSub.unsubscribe(only_for_user_topic_all(user_id))

  defp only_for_user_topic(user_id, button_id) do
    "#{topic(button_id)}/only_for_user:#{user_id}"
  end

  defp only_for_user_topic_all(user_id) do
    "#{topic_all()}/only_for_user:#{user_id}"
  end
end
