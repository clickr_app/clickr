defmodule Clickr.SeatingPlans do
  alias Clickr.SeatingPlans.{Aggregates, Commands, Events, Queries}

  use Clickr.Mixins.Context, aggregate: Aggregates.SeatingPlan, commands: Commands, events: Events

  def id(%{class_id: _, room_id: _} = opts) do
    Aggregates.SeatingPlan.id(opts)
  end

  def list(opts \\ []) do
    Queries.List.run(opts)
  end

  def by_id!(id) do
    Queries.ById.run!(id)
  end

  def by_id(id) do
    Queries.ById.run(id)
  end

  def exists?(id) do
    Queries.Exists.run(id)
  end
end
