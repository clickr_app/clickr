defmodule Clickr.Devices do
  alias Clickr.Devices.{Aggregates, Commands, Deconz, Events, Queries}

  use Clickr.Mixins.Context, aggregate: Aggregates.Device, commands: Commands, events: Events

  def list(opts \\ []) do
    Queries.List.run(opts)
  end

  def by_id!(id) do
    Queries.ById.run!(id)
  end

  def by_id(id) do
    Queries.ById.run(id)
  end

  def battery_less_than(less_than, opts \\ []) when is_float(less_than) do
    Queries.BatteryLessThan.run(less_than, opts)
  end

  def deconz_id(%{"uniqueid" => _id} = data) do
    Deconz.id(data)
  end
end
