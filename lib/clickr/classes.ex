defmodule Clickr.Classes do
  alias Clickr.Classes.{Aggregates, Commands, Events, Queries}

  use Clickr.Mixins.Context, aggregate: Aggregates.Class, commands: Commands, events: Events

  def list(opts \\ []) do
    Queries.List.run(opts)
  end

  def by_id!(id) do
    Queries.ById.run!(id)
  end
end
