defmodule Clickr.TeamsApp do
  def config() do
    Application.get_env(:clickr, __MODULE__)
  end

  def app_id() do
    config()[:app_id]
  end
end
