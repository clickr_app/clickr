defmodule Clickr.Vex.Validators.SeatingPlanExists do
  use Vex.Validator
  alias Clickr.SeatingPlans

  def validate(_value, context, _options) do
    id = SeatingPlans.id(context)

    case SeatingPlans.exists?(id) do
      true -> :ok
      false -> {:error, "seating plan does not exist"}
    end
  end

  # not actually used, just required by behaviour
  def validate(_value, _options), do: :ok
end
