defmodule Clickr.Vex.Validators.SeatingPlanDoesNotExist do
  use Vex.Validator
  alias Clickr.SeatingPlans

  def validate(_value, context, _options) do
    id = SeatingPlans.id(context)

    case SeatingPlans.exists?(id) do
      true -> {:error, "seating plan exists"}
      false -> :ok
    end
  end

  # not actually used, just required by behaviour
  def validate(_value, _options), do: :ok
end
