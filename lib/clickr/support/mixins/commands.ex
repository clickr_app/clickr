defmodule Clickr.Mixins.Commands do
  def commands(compiled_module) do
    compiled_module.__info__(:attributes)
    |> Enum.filter(fn {name, _} -> name == :commands end)
    |> Enum.map(fn {_, [{name, _, _}]} -> name end)
  end

  def generate_id(%{} = c) do
    Clickr.Map.put_if_nil_lazy(c, :id, &UUID.uuid4/0)
  end

  defmacro __using__(_params) do
    Module.register_attribute(__CALLER__.module, :fields, persist: true, accumulate: true)
    Module.register_attribute(__CALLER__.module, :commands, persist: true, accumulate: true)

    quote do
      import Clickr.Mixins.Commands
      @before_compile unquote(__MODULE__)
      field(:id, presence: true, uuid: true)
      field(:user_id, uuid: true, presence: true)
    end
  end

  defmacro __before_compile__(_env) do
    field_defs = Module.get_attribute(__CALLER__.module, :fields)
    commands = Module.get_attribute(__CALLER__.module, :commands)

    for {module, fields, params} <- commands do
      fields = [:id, :user_id] ++ fields
      validations = Keyword.take(field_defs, fields)
      generate_id? = params[:generate_id] || false

      compile_command_module(module, fields, validations, generate_id?)
    end
  end

  defp compile_command_module(module, fields, validations, generate_id?) do
    validations_ast = compile_validations(validations)
    generate_id_ast = compile_generate_id(generate_id?)

    quote do
      defmodule unquote(module) do
        use ExConstructor
        use Vex.Struct
        defstruct unquote(fields)
        unquote(validations_ast)
        unquote(generate_id_ast)
      end
    end
  end

  defp compile_validations(validations) do
    quote do
      for {field_name, field_validations} <- unquote(validations) do
        validates(field_name, field_validations)
      end
    end
  end

  defp compile_generate_id(generate_id?)

  defp compile_generate_id(true) do
    quote do
      defimpl Clickr.Protocols.CommandEnrichment do
        def enrich(c), do: Clickr.Mixins.Commands.generate_id(c)
      end
    end
  end

  defp compile_generate_id(false), do: nil

  defmacro field(name, validations \\ []) when is_atom(name) do
    quote bind_quoted: [name: name, validations: validations] do
      @fields {name, validations}
    end
  end

  defmacro command(name, fields \\ [], params \\ []) when is_atom(name) and is_list(fields) do
    module_name = :"#{__CALLER__.module}.#{name}"

    quote bind_quoted: [module_name: module_name, fields: fields, params: params] do
      @commands {module_name, fields, params}
    end
  end
end
