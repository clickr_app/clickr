defmodule Clickr.Mixins.Events do
  def event_pubsub_prefix(event_module) do
    [_last | rest] = Module.split(event_module) |> Enum.reverse()
    parent_module = rest |> Enum.reverse() |> Module.concat()
    pubsub_prefix(parent_module)
  end

  def pubsub_prefix(compiled_module) do
    compiled_module.__info__(:attributes)
    |> Enum.filter(fn {name, _} -> name == :pubsub_prefix end)
    |> Enum.map(fn {_, [prefix]} -> prefix end)
    |> List.first()
  end

  defmacro __using__(params) do
    Module.register_attribute(__CALLER__.module, :events, persist: true, accumulate: true)
    Module.register_attribute(__CALLER__.module, :pubsub_prefix, persist: true)

    prefix = params[:pubsub_prefix] || raise "Missing param :pubsub_prefix"
    Module.put_attribute(__CALLER__.module, :pubsub_prefix, prefix)

    quote do
      import Clickr.Mixins.Events
      @before_compile unquote(__MODULE__)
    end
  end

  defmacro __before_compile__(_env) do
    events = Module.get_attribute(__CALLER__.module, :events)

    for {module, fields, opts} <- events do
      fields = [:id, :user_id] ++ fields
      compile_event_module(module, fields, opts)
    end
  end

  defp compile_event_module(module, fields, opts) do
    quote do
      defmodule unquote(module) do
        defstruct unquote(fields)

        def from_command(command, attrs \\ []) do
          opts = unquote(opts[:from_command])
          unquote(__MODULE__).from_command(command, __MODULE__, attrs, opts)
        end
      end
    end
  end

  defmacro event(name, fields \\ [], opts \\ []) when is_atom(name) and is_list(fields) do
    module_name = :"#{__CALLER__.module}.#{name}"

    quote bind_quoted: [module_name: module_name, fields: fields, opts: opts] do
      @events {module_name, fields, opts}
    end
  end

  def from_command(command, event_type, attrs \\ [], opts \\ []) do
    attrs =
      command
      |> Map.from_struct()
      |> Map.drop(opts[:drop] || [])
      |> Map.take(opts[:take] || Map.keys(command))
      |> Map.merge(Map.new(attrs))

    struct!(event_type, attrs)
  end
end
