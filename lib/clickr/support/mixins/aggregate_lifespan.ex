defmodule Clickr.Mixins.AggregateLifespan do
  defmacro __using__(params) do
    deleted_event = params[:deleted_event] |> Macro.expand(__CALLER__)

    quote do
      @behaviour Commanded.Aggregates.AggregateLifespan

      @impl true
      def after_event(e) do
        unquote(__MODULE__).after_event(e, unquote(deleted_event))
      end

      @impl true
      def after_command(_), do: :infinity

      @impl true
      def after_error(_), do: :infinity
    end
  end

  def after_event(current_event, deleted_event) when current_event == deleted_event, do: :stop
  def after_event(_, _), do: :infinity
end
