defmodule Clickr.Mixins.Projection do
  defmacro __using__(params) do
    table = params[:projection_table] || raise "Missing param :projection_table"
    Module.register_attribute(__CALLER__.module, :projection_table, persist: true)
    Module.put_attribute(__CALLER__.module, :projection_table, table)

    quote do
      use Ecto.Schema
    end
  end

  def tables() do
    {:ok, modules} = :application.get_key(:clickr, :modules)

    modules
    |> Enum.map(fn m -> m.module_info(:attributes)[:projection_table] end)
    |> Enum.filter(& &1)
    |> List.flatten()
  end
end
