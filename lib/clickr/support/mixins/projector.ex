defmodule Clickr.Mixins.Projector do
  defmacro __using__(_params) do
    quote do
      use Commanded.Projections.Ecto,
        application: Clickr.CommandedApplication,
        name: __MODULE__,
        consistency: :strong
    end
  end
end
