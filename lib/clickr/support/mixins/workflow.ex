defmodule Clickr.Mixins.Workflow do
  defmacro __using__(params) do
    module = params[:module] || Commanded.Event.Handler

    opts =
      Keyword.merge(
        [
          application: Clickr.CommandedApplication,
          name: __CALLER__.module,
          consistency: :eventual
        ],
        Keyword.drop(params, [:module])
      )

    quote do
      use unquote(module), unquote(opts)
      use Clickr.Mixins.LogError
      import Clickr.CommandedApplication, only: [with_cause: 1, with_cause: 2]
    end
  end
end
