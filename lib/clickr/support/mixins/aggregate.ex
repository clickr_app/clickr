defmodule Clickr.Mixins.Aggregate do
  def prefix(compiled_module) do
    compiled_module.__info__(:attributes)
    |> Enum.filter(fn {name, _} -> name == :aggregate_prefix end)
    |> Enum.map(fn {_, [prefix]} -> prefix end)
    |> List.first()
  end

  defmacro __using__(params) do
    prefix = params[:prefix] || raise "Missing param :prefix"
    Module.register_attribute(__CALLER__.module, :aggregate_prefix, persist: true)
    Module.put_attribute(__CALLER__.module, :aggregate_prefix, prefix)

    compile_deleted(params[:with_deleted], __CALLER__)
  end

  defp compile_deleted(opts, caller)
  defp compile_deleted(nil, _), do: nil

  defp compile_deleted([{k, _v} | _] = opts, caller) when is_atom(k) do
    command =
      (opts[:command] || raise("Missing param :with_deleted[:command]"))
      |> Macro.expand(caller)

    event =
      (opts[:event] || raise("Missing param :with_deleted[:event]"))
      |> Macro.expand(caller)

    lifespan? = Keyword.get(opts, :lifespan?, true)

    [
      if(lifespan?, do: compile_deleted_lifespan(event)),
      compile_deleted_handlers(command, event)
    ]
  end

  defp compile_deleted_handlers(delete, deleted) do
    quote do
      def execute(%{deleted?: true}, _c), do: {:error, :deleted}
      def execute(_a, %unquote(delete){} = c), do: unquote(deleted).from_command(c)

      def apply(a, %unquote(deleted){}), do: %{a | deleted?: true}
    end
  end

  defp compile_deleted_lifespan(deleted) do
    quote do
      use Clickr.Mixins.AggregateLifespan, deleted_event: unquote(deleted)
    end
  end
end
