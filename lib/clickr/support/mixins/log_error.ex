defmodule Clickr.Mixins.LogError do
  require Logger

  defmacro __using__(_params) do
    quote do
      @impl true
      def error(error, event, context) do
        Clickr.Mixins.LogError.log(error, event, context)
      end
    end
  end

  def log(error, event, _context) do
    Logger.error("#{__MODULE__} encountered an error " <> inspect(%{error: error, event: event}))

    :skip
  end
end
