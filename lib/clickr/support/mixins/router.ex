defmodule Clickr.Mixins.Router do
  alias Clickr.Middleware

  defmacro __using__(params) do
    aggregate_module =
      (params[:aggregate] || raise("Missing param :aggregate"))
      |> Macro.expand(__CALLER__)

    commands_module =
      (params[:commands] || raise("Missing param :comands"))
      |> Macro.expand(__CALLER__)

    prefix = Clickr.Mixins.Aggregate.prefix(aggregate_module)
    middleware_ast = compile_middleware()
    dispatches_ast = compile_dispatches(commands_module, aggregate_module)

    quote do
      import Clickr.Mixins.Router
      use Commanded.Commands.Router

      identify(unquote(aggregate_module), by: :id, prefix: unquote(prefix) <> ":")
      unquote(middleware_ast)
      unquote(dispatches_ast)
    end
  end

  defp compile_middleware() do
    for m <- [Middleware.EnrichCommand, Middleware.ValidateCommand] do
      quote do: middleware(unquote(m))
    end
  end

  defp compile_dispatches(commands_module, aggregate_module) do
    commands = Clickr.Mixins.Commands.commands(commands_module)
    lifespan_module = Module.concat(aggregate_module, Lifespan)
    lifespan? = Code.ensure_loaded?(lifespan_module)

    opts =
      [to: aggregate_module, lifespan: if(lifespan?, do: lifespan_module)]
      |> Keyword.filter(fn {_k, v} -> v end)

    quote do: dispatch(unquote(commands), unquote(opts))
  end
end
