defmodule Clickr.Mixins.Context do
  defmacro __using__(params) do
    aggregate_module = module(params, :aggregate, __CALLER__)
    commands_module = module(params, :commands, __CALLER__)
    events_module = module(params, :events, __CALLER__)

    agg_prefix = Clickr.Mixins.Aggregate.prefix(aggregate_module)
    commands = Clickr.Mixins.Commands.commands(commands_module)
    pubsub_prefix = Clickr.Mixins.Events.pubsub_prefix(events_module)

    command_fns_ast = compile_command_fns(commands)
    pubsub_fns_ast = compile_pubsub_fns(pubsub_prefix)

    quote do
      def aggregate_state(id) do
        Clickr.Mixins.Context.aggregate_state(unquote(aggregate_module), unquote(agg_prefix), id)
      end

      unquote(command_fns_ast)
      unquote(pubsub_fns_ast)
    end
  end

  def aggregate_state(aggregate_module, prefix, id) do
    Commanded.aggregate_state(Clickr.CommandedApplication, aggregate_module, prefix <> ":" <> id)
  end

  defp compile_command_fns(commands) do
    for command <- commands do
      function =
        command |> Module.split() |> List.last() |> Macro.underscore() |> String.to_atom()

      quote do
        def unquote(function)(user, command, opts \\ [])

        def unquote(function)(%Clickr.Accounts.User{} = user, %unquote(command){} = c, opts) do
          c_with_user = Clickr.Map.put_if_nil(c, :user_id, user.id)
          Clickr.CommandedApplication.dispatch(c_with_user, opts)
        end

        def unquote(function)(%Clickr.Accounts.User{} = user, attrs, opts)
            when is_map(attrs) or is_list(attrs) do
          c = unquote(command).new(attrs)
          unquote(function)(user, c, opts)
        end
      end
    end
  end

  defp compile_pubsub_fns(prefix) do
    quote do
      def subscribe(id), do: Clickr.PubSub.subscribe(topic(id))
      def subscribe_all(), do: Clickr.PubSub.subscribe(topic_all())
      def unsubscribe(id), do: Clickr.PubSub.unsubscribe(topic(id))
      def unsubscribe_all(), do: Clickr.PubSub.unsubscribe(topic_all())
      defp topic(id), do: unquote(prefix) <> ":" <> id
      defp topic_all(), do: unquote(prefix)
    end
  end

  defp module(params, name, caller) do
    (params[name] || raise("Missing param :#{name}"))
    |> Macro.expand(caller)
  end
end
