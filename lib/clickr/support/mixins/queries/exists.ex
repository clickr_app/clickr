defmodule Clickr.Mixins.Queries.Exists do
  import Ecto.Query

  defmacro __using__(params) do
    schema = params[:schema] || raise "Missing param :schema"

    quote do
      def run(id) do
        Clickr.Mixins.Queries.Exists.run(unquote(schema), id)
      end
    end
  end

  def run(schema, id) do
    schema
    |> where(id: ^id)
    |> Clickr.Repo.exists?()
  end
end
