defmodule Clickr.Mixins.Queries.ById do
  defmacro __using__(params) do
    schema = params[:schema] || raise "Missing param :schema"

    quote do
      def run!(id) do
        Clickr.Mixins.Queries.ById.run!(unquote(schema), id)
      end

      def run(id) do
        Clickr.Mixins.Queries.ById.run(unquote(schema), id)
      end
    end
  end

  def run!(schema, id) do
    Clickr.Repo.get!(schema, id)
  end

  def run(schema, id) do
    Clickr.Repo.get(schema, id)
  end
end
