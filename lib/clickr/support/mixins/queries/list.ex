defmodule Clickr.Mixins.Queries.List do
  import Ecto.Query
  alias Clickr.Repo

  defmacro __using__(params) do
    schema = params[:schema] || raise "Missing param :schema"
    query_field = params[:query_field]

    quote do
      def run(opts \\ []) do
        unquote(__MODULE__).run(unquote(schema), unquote(query_field), opts)
      end
    end
  end

  def run(schema, query_field, opts \\ []) do
    query = query(schema, query_field, opts)
    {entries(query, opts), total_count(query, opts)}
  end

  defp query(schema, query_field, opts) do
    schema
    |> where(^Keyword.get(opts, :where, []))
    |> where_query(query_field, opts)
  end

  defp where_query(q, query_field, opts)
  defp where_query(q, nil, _opts), do: q

  defp where_query(q, query_field, opts),
    do: where(q, [x], ilike(field(x, ^query_field), ^"#{Keyword.get(opts, :query, "")}%"))

  defp entries(query, opts) do
    query
    |> limit(^opts[:limit])
    |> offset(^opts[:offset])
    |> order_by(^opts[:order_by])
    |> Repo.all()
  end

  defp total_count(query, _opts) do
    query
    |> Repo.aggregate(:count)
  end
end
