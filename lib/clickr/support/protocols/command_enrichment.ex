defprotocol Clickr.Protocols.CommandEnrichment do
  @fallback_to_any true
  @spec enrich(struct()) :: struct() | {:ok, struct()} | {:error, String.t() | atom()}
  def enrich(command)
end

defimpl Clickr.Protocols.CommandEnrichment, for: Any do
  def enrich(command), do: {:ok, command}
end
