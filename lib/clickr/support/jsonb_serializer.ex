defmodule Clickr.JsonbSerializer do
  @moduledoc """
  Serialize to/from PostgreSQL's native `jsonb` format.
  """

  @behaviour EventStore.Serializer

  def serialize(%_{} = term) do
    term
    |> Map.from_struct()
    # |> Enum.map(fn {k, v} -> {Atom.to_string(k), v} end)
    |> Enum.into(%{})
  end

  def serialize(term), do: term

  def deserialize(term, config) do
    case config[:type] do
      nil ->
        term

      type ->
        type
        |> String.to_existing_atom()
        |> to_struct(term)
        |> Commanded.Serialization.JsonDecoder.decode()
    end
  end

  def to_struct(type, term) do
    struct(type, keys_to_atoms(term))
  end

  def keys_to_atoms(map) when is_map(map) do
    for {key, value} <- map, into: %{} do
      # {String.to_existing_atom(key), keys_to_atoms(value)}
      # only convert to atoms on first level
      {String.to_existing_atom(key), value}
    end
  end

  def keys_to_atoms(value), do: value
end
