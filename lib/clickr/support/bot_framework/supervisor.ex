defmodule BotFramework.Supervisor do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        BotFramework.Authentication.Server
      ],
      strategy: :one_for_one
    )
  end
end
