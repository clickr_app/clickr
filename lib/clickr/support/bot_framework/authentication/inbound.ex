defmodule BotFramework.Authentication.Inbound do
  @open_id_config_endpoint "https://login.botframework.com/v1/.well-known/openidconfiguration"
  @trusted_issuers ["https://graph.microsoft.com", "https://api.botframework.com"]

  use Joken.Config

  def verify_token("Bearer " <> token, valid_keys), do: verify_token(token, valid_keys)

  def verify_token(token, valid_keys) do
    verify(token, signer(token, valid_keys))
  end

  # Internal

  def get_trusted_keys do
    %{"jwks_uri" => keys_uri} = get_open_id_metadata()

    case HTTPoison.get(keys_uri) do
      {:ok, %{body: body, status_code: 200}} ->
        Jason.decode!(body)["keys"]
    end
  end

  @impl true
  def token_config do
    # https://github.com/IdentityServer/IdentityServer3/issues/1251
    allowed_clock_skew_5_minutes = 5 * 60

    default_claims(default_exp: allowed_clock_skew_5_minutes)
    |> add_claim("iss", nil, &(&1 in @trusted_issuers))
    |> add_claim("aud", nil, &(&1 == BotFramework.client_id()))
  end

  defp signer(token, valid_keys) do
    {:ok, %{"kid" => kid}} = Joken.peek_header(token)
    params = Enum.find(valid_keys, &(&1["kid"] == kid))
    Joken.Signer.create("RS256", params)
  end

  defp get_open_id_metadata do
    case HTTPoison.get(@open_id_config_endpoint) do
      {:ok, %{body: body, status_code: 200}} ->
        Jason.decode!(body)
    end
  end
end
