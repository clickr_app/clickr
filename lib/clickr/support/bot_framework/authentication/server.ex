defmodule BotFramework.Authentication.Server do
  @refresh_once_every_day 24 * 60 * 60 * 1_000
  @retry_one_minute_after_failure 60 * 1_000

  use GenServer
  require Logger
  alias BotFramework.Authentication.{Inbound, Outbound}

  defstruct outbound_token: "not fetched", inbound_trusted_keys: []

  # Public API
  def get_outbound_token(), do: GenServer.call(__MODULE__, :get_outbound_token)
  def get_inbound_trusted_keys(), do: GenServer.call(__MODULE__, :get_inbound_trusted_keys)

  # Internal
  def start_link(_opts) do
    GenServer.start_link(__MODULE__, [], name: __MODULE__)
  end

  @impl true
  def init([]) do
    if BotFramework.authentication_enabled?() do
      send(self(), :refresh_inbound_trusted_keys)
      send(self(), :refresh_outbound_token)
    end

    {:ok, %__MODULE__{}}
  end

  @impl true
  def handle_call(:get_outbound_token, _from, state),
    do: {:reply, state.outbound_token, state}

  def handle_call(:get_inbound_trusted_keys, _from, state),
    do: {:reply, state.inbound_trusted_keys, state}

  @impl true
  def handle_info(:refresh_outbound_token, state) do
    Logger.info("Refresh outbound token")

    try do
      token = Outbound.get_token()
      one_second_before_it_expires = (token["expires_in"] - 1) * 1_000
      refresh_after = Enum.min([one_second_before_it_expires, @refresh_once_every_day])
      Process.send_after(self(), :refresh_outbound_token, refresh_after)
      {:noreply, %{state | outbound_token: token}}
    rescue
      e ->
        Logger.error("Failed to refresh outbound token. Retrying in one minute. #{inspect(e)}")
        Process.send_after(self(), :refresh_outbound_token, @retry_one_minute_after_failure)
        {:noreply, state}
    end
  end

  def handle_info(:refresh_inbound_trusted_keys, state) do
    Logger.info("Refresh inbound trusted keys")

    try do
      trusted_keys = Inbound.get_trusted_keys()
      Process.send_after(self(), :refresh_inbound_trusted_keys, @refresh_once_every_day)
      {:noreply, %{state | inbound_trusted_keys: trusted_keys}}
    rescue
      e ->
        Logger.error(
          "Failed to refresh inbound trusted keys. Retrying in one minute. #{inspect(e)}"
        )

        Process.send_after(self(), :refresh_inbound_trusted_keys, @retry_one_minute_after_failure)
        {:noreply, state}
    end
  end
end
