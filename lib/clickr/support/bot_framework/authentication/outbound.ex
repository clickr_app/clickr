defmodule BotFramework.Authentication.Outbound do
  @jwt_issuer_endpoint "https://login.microsoftonline.com/botframework.com/oauth2/v2.0/token"
  @scope "https://api.botframework.com/.default"

  def get_token do
    case HTTPoison.post(@jwt_issuer_endpoint, {:form, request_body()}) do
      {:ok, %{status_code: sc, body: body}} when sc in 200..202 -> Jason.decode!(body)
    end
  end

  defp request_body() do
    [
      grant_type: "client_credentials",
      client_id: BotFramework.client_id(),
      client_secret: BotFramework.client_secret(),
      scope: @scope
    ]
  end
end
