defmodule BotFramework.Phoenix.Controller do
  alias BotFramework.Client

  @callback handle_activity(Plug.Conn.t(), map()) :: map()

  defmacro __using__(_) do
    quote do
      @behaviour unquote(__MODULE__)

      def handle_post(conn, params) do
        unquote(__MODULE__).handle_post(__MODULE__, conn, params)
      end
    end
  end

  def handle_post(module, conn, %{} = activity) do
    case module.handle_activity(conn, activity) do
      nil -> nil
      res -> Client.send_message(res, activity)
    end

    Phoenix.Controller.json(conn, %{message: :ok})
  end
end
