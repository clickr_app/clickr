defmodule BotFramework.Phoenix.Router do
  defmacro __using__(_) do
    quote do
      import unquote(__MODULE__), only: [bot_framework_handler: 2]

      def authenticate(conn, params) do
        unquote(__MODULE__).authenticate(conn, params)
      end

      pipeline :bot_framework do
        plug :accepts, ["json"]
        plug :authenticate
      end
    end
  end

  defmacro bot_framework_handler(path, controller) do
    quote do
      scope "/" do
        pipe_through :bot_framework
        post unquote(path), unquote(controller), :handle_post
      end
    end
  end

  def authenticate(conn, _params) do
    token = conn |> Plug.Conn.get_req_header("authorization") |> List.first()

    case BotFramework.verify_inbound_token(token) do
      {:ok, _} -> conn
      {:error, _} -> conn |> Plug.Conn.resp(403, "") |> Plug.Conn.halt()
    end
  end
end
