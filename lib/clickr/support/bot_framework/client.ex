defmodule BotFramework.Client do
  alias BotFramework

  def send_message(res, %{} = activity) do
    complete_res =
      %{
        "conversation" => activity.conversation,
        "from" => activity.recipient,
        "recipient" => activity.from,
        "replyToId" => activity.id
      }
      |> Map.merge(res)

    base_url = String.trim_trailing(activity.serviceUrl, "/")
    url = "#{base_url}/v3/conversations/#{activity.conversation.id}/activities"

    case HTTPoison.post(url, Jason.encode!(complete_res), headers()) do
      {:ok, %{status_code: sc}} when sc in 200..202 -> nil
    end
  end

  def headers do
    [
      {"Content-Type", "application/json"},
      {"Accept", "application/json"},
      {"Authorization", "Bearer " <> BotFramework.get_outbound_token()["access_token"]}
    ]
  end
end
