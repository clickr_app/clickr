defmodule BotFramework.Authentication do
  alias __MODULE__.{Inbound, Server}

  def verify_inbound_token(token) do
    Inbound.verify_token(token, Server.get_inbound_trusted_keys())
  end

  def get_outbound_token() do
    Server.get_outbound_token()
  end
end
