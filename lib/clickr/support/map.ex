defmodule Clickr.Map do
  @spec put_if_nil_lazy(map, Map.key(), (() -> Map.value())) :: map
  def put_if_nil_lazy(map, key, fun) when is_map(map) and is_function(fun, 0) do
    case map do
      %{^key => nil} ->
        Map.put(map, key, fun.())

      _ ->
        map
    end
  end

  @spec put_if_nil(map, Map.key(), Map.value()) :: map
  def put_if_nil(map, key, value) when is_map(map) do
    put_if_nil_lazy(map, key, fn -> value end)
  end
end
