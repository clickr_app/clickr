defmodule Clickr.CommandedPubSubBridge do
  use Clickr.Mixins.Workflow, consistency: :eventual

  @impl true
  def handle(%{__struct__: module, id: id} = e, _metadata) do
    prefix = Clickr.Mixins.Events.event_pubsub_prefix(module)

    Clickr.PubSub.broadcast(prefix, e)
    Clickr.PubSub.broadcast(prefix <> ":" <> id, e)

    :ok
  end
end
