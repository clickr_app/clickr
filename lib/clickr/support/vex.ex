defmodule Clickr.Vex do
  def percent(), do: [number: [greater_than_or_equal_to: 0, less_than_or_equal_to: 100]]
  def x(), do: [number: [greater_than: 0, less_than: 100]]
  def y(), do: [number: [greater_than: 0, less_than: 100]]
end
