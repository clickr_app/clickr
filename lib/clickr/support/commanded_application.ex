defmodule Clickr.CommandedApplication do
  use Commanded.Application, otp_app: :clickr

  router(Clickr.Buttons.Router)
  router(Clickr.Devices.Router)
  router(Clickr.Classes.Router)
  router(Clickr.Lessons.Router)
  router(Clickr.Rooms.Router)
  router(Clickr.SeatingPlans.Router)
  router(Clickr.Grades.Router)
  router(Clickr.Students.Router)

  def with_cause(metadata, opts \\ []) do
    Keyword.merge(
      [
        causation_id: metadata[:event_id],
        correlation_id: metadata[:correlation_id] || UUID.uuid4()
      ],
      opts
    )
  end
end
