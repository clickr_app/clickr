defmodule BotFramework do
  defdelegate verify_inbound_token(token), to: __MODULE__.Authentication
  defdelegate get_outbound_token, to: __MODULE__.Authentication

  def config() do
    Application.get_env(:clickr, __MODULE__)
  end

  def authentication_enabled?() do
    config()[:authentication_enabled?]
  end

  def client_id() do
    config()[:client_id]
  end

  def client_secret() do
    config()[:client_secret]
  end
end
