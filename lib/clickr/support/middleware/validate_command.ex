defmodule Clickr.Middleware.ValidateCommand do
  @behaviour Commanded.Middleware

  alias Commanded.Middleware.Pipeline
  import Pipeline

  def before_dispatch(%Pipeline{command: c} = pipeline) do
    case Vex.valid?(c) do
      true -> pipeline
      false -> failed_validation(pipeline)
    end
  end

  def after_dispatch(pipeline), do: pipeline
  def after_failure(pipeline), do: pipeline

  defp failed_validation(%Pipeline{command: c} = pipeline) do
    errors =
      c
      |> Vex.errors()
      |> Enum.map(fn {:error, field, _type, msg} -> {field, msg} end)

    pipeline
    |> respond({:error, :validation_failure, errors})
    |> halt()
  end
end
