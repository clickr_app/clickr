defmodule Clickr.Middleware.EnrichCommand do
  @behaviour Commanded.Middleware

  alias Commanded.Middleware.Pipeline

  def before_dispatch(%Pipeline{command: c} = pipeline) do
    case Clickr.Protocols.CommandEnrichment.enrich(c) do
      {:ok, enriched_c} ->
        %Pipeline{pipeline | command: enriched_c}

      enriched_c when is_struct(enriched_c) ->
        %Pipeline{pipeline | command: enriched_c}

      {:error, _error} = reply ->
        pipeline
        |> Pipeline.respond(reply)
        |> Pipeline.halt()
    end
  end

  def after_dispatch(pipeline), do: pipeline

  def after_failure(pipeline), do: pipeline
end
