defmodule Clickr.Grades do
  alias Clickr.Grades.{Format}
  alias Clickr.Grades.{Aggregates, Commands, Events, Projections, Queries}

  use Clickr.Mixins.Context, aggregate: Aggregates.Grade, commands: Commands, events: Events

  def format(system, percent) do
    Format.format(system, percent)
  end

  def id(%{student_id: _, grading_interval_id: _} = opts) do
    Aggregates.Grade.id(opts)
  end

  def list(opts \\ []) do
    Queries.List.run(opts)
  end

  def by_id!(id) do
    Queries.ById.run!(id)
  end

  def with_details_by_id!(id) do
    Clickr.Repo.get!(Projections.GradeWithDetails, id)
  end

  def with_details_by_ids(ids) do
    Queries.WithDetailsByIds.run(ids)
  end
end
