defmodule Clickr.Rooms.Events do
  use Clickr.Mixins.Events, pubsub_prefix: "room"

  event(:Created, [:name, :width, :height])
  event(:ChangedSize, [:width, :height])
  event(:Deleted, [])
  event(:PositionedButton, [:button_id, :x, :y])
  event(:RemovedButton, [:button_id])
  event(:Renamed, [:name])
end
