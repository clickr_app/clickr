defmodule Clickr.Rooms.Router do
  use Clickr.Mixins.Router,
    aggregate: Clickr.Rooms.Aggregates.Room,
    commands: Clickr.Rooms.Commands
end
