defmodule Clickr.Rooms.Aggregates.Room do
  defstruct [:id, buttons: %{}, deleted?: false]

  alias Clickr.Rooms.{Commands, Events}
  alias __MODULE__, as: A

  use Clickr.Mixins.Aggregate,
    prefix: "room",
    with_deleted: [command: Commands.Delete, event: Events.Deleted]

  # Command handlers

  def execute(%A{id: nil}, %Commands.Create{} = c) do
    %Events.Created{id: c.id, name: c.name, width: c.width, height: c.height}
  end

  def execute(%A{id: nil}, _c) do
    {:error, :not_created}
  end

  def execute(%A{}, %Commands.ChangeSize{} = c) do
    # Leave buttons untouched, even if now out of bounds
    %Events.ChangedSize{id: c.id, width: c.width, height: c.height}
  end

  def execute(%A{} = a, %Commands.PositionButton{} = c) do
    with :ok <- position_empty?(a, c) do
      %Events.PositionedButton{id: c.id, button_id: c.button_id, x: c.x, y: c.y}
    end
  end

  def execute(%A{} = a, %Commands.RemoveButton{} = c) do
    with :ok <- button_added?(a, c) do
      %Events.RemovedButton{id: c.id, button_id: c.button_id}
    end
  end

  def execute(%A{}, %Commands.Rename{} = c) do
    %Events.Renamed{id: c.id, name: c.name}
  end

  # Event handlers

  def apply(%A{} = a, %Events.Created{} = e) do
    %A{a | id: e.id}
  end

  def apply(%A{} = a, %Events.ChangedSize{}), do: a
  require Logger

  def apply(%A{} = a, %Events.PositionedButton{} = e) do
    %A{a | buttons: Map.put(a.buttons, e.button_id, {e.x, e.y})}
  end

  def apply(%A{} = a, %Events.RemovedButton{} = e) do
    %A{a | buttons: Map.delete(a.buttons, e.button_id)}
  end

  def apply(%A{} = a, %Events.Renamed{}) do
    a
  end

  # Private

  defp position_empty?(%A{} = a, %{x: x, y: y}) do
    case Enum.any?(a.buttons, fn {_, p} -> p == {x, y} end) do
      true -> {:error, :position_occupied}
      false -> :ok
    end
  end

  defp button_added?(%A{} = a, %{button_id: id}) do
    case Map.has_key?(a.buttons, id) do
      true -> :ok
      false -> {:error, :button_not_added}
    end
  end
end
