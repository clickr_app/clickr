defmodule Clickr.Rooms.Projectors.Room do
  use Clickr.Mixins.Projector
  alias Clickr.Buttons
  alias Clickr.Rooms.{Events, Projections}

  project(%Events.Created{} = e, fn multi ->
    Ecto.Multi.insert(multi, :insert, %Projections.Room{
      id: e.id,
      name: e.name,
      width: e.width,
      height: e.height,
      buttons: []
    })
  end)

  project(%Events.ChangedSize{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [
        width: e.width,
        height: e.height
      ]
    )
  end)

  project(%Events.PositionedButton{} = e, fn multi ->
    button = Buttons.aggregate_state(e.button_id)
    %{buttons: buttons} = by_id(e.id) |> Clickr.Repo.one!()
    buttons = Enum.reject(buttons, &(&1.id == e.button_id))

    button = %Projections.Room.Button{
      id: e.button_id,
      x: e.x,
      y: e.y,
      name: button.name,
      device_id: button.device_id,
      device_name: button.device_name
    }

    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [buttons: [button | buttons]])
  end)

  project(%Events.RemovedButton{} = e, fn multi ->
    %{buttons: buttons} = by_id(e.id) |> Clickr.Repo.one!()

    Ecto.Multi.update_all(multi, :update, by_id(e.id),
      set: [buttons: Enum.reject(buttons, &(&1.id == e.button_id))]
    )
  end)

  project(%Events.Renamed{} = e, fn multi ->
    Ecto.Multi.update_all(multi, :update, by_id(e.id), set: [name: e.name])
  end)

  project(%Events.Deleted{} = e, fn multi ->
    Ecto.Multi.delete_all(multi, :delete, by_id(e.id))
  end)

  defp by_id(id), do: from(r in Projections.Room, where: r.id == ^id)
end
