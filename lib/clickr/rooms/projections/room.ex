defmodule Clickr.Rooms.Projections.Room do
  use Clickr.Mixins.Projection, projection_table: "rooms_room"

  @primary_key {:id, :binary_id, autogenerate: false}
  schema @projection_table do
    field :name, :string
    field :width, :integer
    field :height, :integer

    embeds_many :buttons, Button, primary_key: {:id, :binary_id, autogenerate: false} do
      field :x, :integer
      field :y, :integer
      field :name, :string
      field :device_id, :binary_id
      field :device_name, :string
    end

    timestamps()
  end
end
