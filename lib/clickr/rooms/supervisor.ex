defmodule Clickr.Rooms.Supervisor do
  use Supervisor

  def start_link(arg) do
    Supervisor.start_link(__MODULE__, arg, name: __MODULE__)
  end

  def init(_arg) do
    Supervisor.init(
      [
        Clickr.Rooms.Projectors.Room,
        Clickr.Rooms.Workflows.DeleteSeatingPlans
      ],
      strategy: :one_for_one
    )
  end
end
