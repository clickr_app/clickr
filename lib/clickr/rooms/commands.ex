defmodule Clickr.Rooms.Commands do
  use Clickr.Mixins.Commands

  field(:name, presence: true, length: [min: 2])
  field(:width, [presence: true] ++ Clickr.Vex.x())
  field(:height, [presence: true] ++ Clickr.Vex.y())
  field(:button_id, presence: true, uuid: true)
  field(:x, [presence: true] ++ Clickr.Vex.x())
  field(:y, [presence: true] ++ Clickr.Vex.y())

  command(:Create, [:name, :width, :height], generate_id: true)
  command(:ChangeSize, [:width, :height])
  command(:Delete, [])
  command(:PositionButton, [:button_id, :x, :y])
  command(:RemoveButton, [:button_id])
  command(:Rename, [:name])
end
