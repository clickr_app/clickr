defmodule Clickr.Rooms.Queries.List do
  use Clickr.Mixins.Queries.List, schema: Clickr.Rooms.Projections.Room, query_field: :name
end
