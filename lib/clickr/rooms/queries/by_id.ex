defmodule Clickr.Rooms.Queries.ById do
  use Clickr.Mixins.Queries.ById, schema: Clickr.Rooms.Projections.Room
end
