defmodule Clickr.Students do
  alias Clickr.Students.{Aggregates, Commands, Events, Queries}

  use Clickr.Mixins.Context, aggregate: Aggregates.Student, commands: Commands, events: Events

  def list(opts \\ []) do
    Queries.List.run(opts)
  end

  def by_id!(id) do
    Queries.ById.run!(id)
  end

  def by_ids(ids) do
    Queries.ByIds.run(ids)
  end
end
