defmodule ClickrWeb.LayoutView do
  use ClickrWeb, :view

  # Phoenix LiveDashboard is available only in development by default,
  # so we instruct Elixir to not warn if the dashboard route is missing.
  @compile {:no_warn_undefined, {Routes, :live_dashboard_path, 2}}

  def render("root.html", assigns) do
    ~F"""
    <html lang="en">
      <head>
        <meta charset="utf-8"/>
        <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
        <meta name="viewport" content="width=device-width, initial-scale=1.0"/>
        {csrf_meta_tag()}
        {live_title_tag assigns[:page_title] || "", suffix: " · KlassenKnopf"}
        <link phx-track-static rel="stylesheet" href={Routes.static_path(ClickrWeb.Endpoint, "/assets/app.css")}/>
        <script defer phx-track-static type="text/javascript" src={Routes.static_path(ClickrWeb.Endpoint, "/assets/app.js")}></script>

        <link rel="apple-touch-icon" sizes="120x120" href={Routes.static_path(ClickrWeb.Endpoint, "/favicon/apple-touch-icon.png")}>
        <link rel="icon" type="image/png" sizes="32x32" href={Routes.static_path(ClickrWeb.Endpoint, "/favicon/favicon-32x32.png")}>
        <link rel="icon" type="image/png" sizes="16x16" href={Routes.static_path(ClickrWeb.Endpoint, "/favicon/favicon-16x16.png")}>
        <link rel="mask-icon" href={Routes.static_path(ClickrWeb.Endpoint, "/favicon/safari-pinned-tab.svg")} color="#f59e0c">
        <meta name="theme-color" content="#f59e0c">
      </head>
      <body>
        {@inner_content}
      </body>
    </html>
    """
  end
end
