defmodule ClickrWeb.UserSettingsView do
  use ClickrWeb, :view
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  import Clickr.Gettext

  def render("edit.html", assigns) do
    ~F"""
    <Components.Layout {=@conn} {=@current_user} live_view?={false}>
      <Components.ActionCard title={gettext("Settings")}>
        <h3 class="font-3xl font-bold">{gettext("Change email")}</h3>
        <Surface.Components.Form for={@email_changeset} action={Routes.user_settings_path(@conn, :update)} class="space-y-6">
          <Components.Alerts.Error :if={@email_changeset.action} text={gettext("Oops, something went wrong! Please check the errors below.")} />

          <Surface.Components.Form.HiddenInput name={:action} opts={value: "update_email"} />

          <Form.Field name={:email} label={gettext("Email address")} :let={class: class, error?: error?}>
            <Form.EmailInput opts={required: true} {=class} {=error?} />
          </Form.Field>

          <Form.Field name={:current_password} label={gettext("Password")} label_opts={for: :curent_password_for_email} :let={class: class, error?: error?}>
            <Form.PasswordInput id="current_password_for_email" opts={required: true} {=class} {=error?} />
          </Form.Field>

          <Components.Button label={gettext("Change email")} opts={type: "submit"} class="mx-auto" />
        </Surface.Components.Form>

        <hr class="my-5" />

        <h3 class="font-3xl font-bold">{gettext("Change password")}</h3>
        <Surface.Components.Form for={@password_changeset} action={Routes.user_settings_path(@conn, :update)} class="space-y-6">
          <Components.Alerts.Error :if={@password_changeset.action} text={gettext("Oops, something went wrong! Please check the errors below.")} />

          <Surface.Components.Form.HiddenInput name={:action} opts={value: "update_password"} />

          <Form.Field name={:password} label={gettext("New password")} :let={class: class, error?: error?}>
            <Form.PasswordInput opts={required: true} {=class} {=error?} />
          </Form.Field>

          <Form.Field name={:password_confirmation} label={gettext("Confirm new password")} :let={class: class, error?: error?}>
            <Form.PasswordInput opts={required: true} {=class} {=error?} />
          </Form.Field>

          <Form.Field name={:current_password} label={gettext("Current password")} label_opts={for: :curent_password_for_password} :let={class: class, error?: error?}>
            <Form.PasswordInput id="current_password_for_password" opts={required: true} {=class} {=error?} />
          </Form.Field>

          <Components.Button label={gettext("Change password")} opts={type: "submit"} class="mx-auto" />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.Layout>
    """
  end
end
