defmodule ClickrWeb.UserRegistrationView do
  use ClickrWeb, :view
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  import Clickr.Gettext

  def render("new.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Register")} or_link={text: gettext("sign in"), href: "/users/log_in"}>
        <Surface.Components.Form for={@changeset} action={Routes.user_registration_path(@conn, :create)} as={:user} class="space-y-6">
          <Components.Alerts.Error :if={@changeset.action} text={gettext("Oops, something went wrong! Please check the errors below.")} />

          <Form.Field name={:email} label={gettext("Email address")} :let={class: class, error?: error?}>
            <Form.EmailInput opts={required: true, autocomplete: :email, disabled: true} {=class} {=error?} />
          </Form.Field>

          <Form.Field name={:password} label={gettext("Password")} :let={class: class, error?: error?}>
            <Form.PasswordInput opts={required: true, disabled: true} {=class} {=error?} />
          </Form.Field>

          <Components.Button label={gettext("Register")} opts={type: "submit", disabled: "true"} class="mx-auto" />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end
end
