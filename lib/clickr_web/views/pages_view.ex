defmodule ClickrWeb.PagesView do
  use ClickrWeb, :view
  alias ClickrWeb.Components
  import Clickr.Gettext

  def render("privacy.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Privacy statement")}>
        {gettext("Privacy statement")}
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end

  def render("tos.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Terms of service")}>
        {gettext("Terms of service")}
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end
end
