defmodule ClickrWeb.UserResetPasswordView do
  use ClickrWeb, :view
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  import Clickr.Gettext

  def render("new.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Reset password")} or_link={text: gettext("sign in"), href: "/users/log_in"}>
        <Surface.Components.Form for={:user} action={Routes.user_reset_password_path(@conn, :create)} class="space-y-6">
          <Form.Field name={:email} label={gettext("Email address")} :let={class: class, error?: error?}>
            <Form.EmailInput opts={required: true, autocomplete: :email} {=class} {=error?} />
          </Form.Field>

          <Components.Button label={gettext("Send reset instructions")} opts={type: "submit"} class="mx-auto" />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end

  def render("edit.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Reset password")} or_link={text: gettext("sign in"), href: "/users/log_in"}>
        <Surface.Components.Form for={@changeset} action={Routes.user_reset_password_path(@conn, :update, @token)} class="space-y-6">
          <Components.Alerts.Error :if={@changeset.action} text={gettext("Oops, something went wrong! Please check the errors below.")} />

          <Form.Field name={:password} label={gettext("New password")} :let={class: class, error?: error?}>
            <Form.PasswordInput opts={required: true} {=class} {=error?} />
          </Form.Field>

          <Form.Field name={:password_confirmation} label={gettext("Confirm new password")} :let={class: class, error?: error?}>
            <Form.PasswordInput opts={required: true} {=class} {=error?} />
          </Form.Field>

          <Components.Button label={gettext("Reset password")} opts={type: "submit"} class="mx-auto" />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end
end
