defmodule ClickrWeb.UserSessionView do
  use ClickrWeb, :view
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  import Clickr.Gettext

  def render("new.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Sign in")} or_link={text: gettext("register"), href: "/users/register"}>
        <Surface.Components.Form for={@conn} action={Routes.user_session_path(@conn, :create)} as={:user} class="space-y-6">
          <Components.Alerts.Error :if={@error_message} text={@error_message} />

          <Form.Field name={:email} label={gettext("Email address")}>
            <Form.EmailInput opts={required: true, autocomplete: :email} />
          </Form.Field>

          <Form.Field name={:password} label={gettext("Password")}>
            <Form.PasswordInput opts={required: true, autocomplete: :'current-password'} />
          </Form.Field>

          <div class="flex items-center justify-between">
            <Form.CheckboxField name={:remember_me} label={gettext("Remember me")}>
              <Form.Checkbox />
            </Form.CheckboxField>

            <div class="text-sm">
              <a href="/users/reset_password" class="font-medium text-indigo-600 hover:text-indigo-500">
                {gettext("Forgot your password?")}
              </a>
            </div>
          </div>

          <Components.Button label={gettext("Sign in")} opts={type: "submit"} class="mx-auto" />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end

  def page_title("new.html"), do: "Log in"
end
