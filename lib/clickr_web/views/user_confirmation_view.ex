defmodule ClickrWeb.UserConfirmationView do
  use ClickrWeb, :view
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  import Clickr.Gettext

  def render("new.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Resend confirmation instructions")} or_link={text: gettext("sign in"), href: "/users/log_in"}>
        <Surface.Components.Form for={:user} action={Routes.user_confirmation_path(@conn, :create)} class="space-y-6">
          <Form.Field name={:email} label={gettext("Email address")} :let={class: class, error?: error?}>
            <Form.EmailInput opts={required: true, autocomplete: :email} {=class} {=error?} />
          </Form.Field>

          <Components.Button label={gettext("Resend confirmation instructions")} opts={type: "submit"} class="mx-auto" />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end

  def render("edit.html", assigns) do
    ~F"""
    <Components.LayoutEmpty {=@conn}>
      <Components.ActionCard title={gettext("Confirm my account")} or_link={text: gettext("sign in"), href: "/users/log_in"}>
        <Surface.Components.Form for={:user} action={Routes.user_confirmation_path(@conn, :update, @token)} class="space-y-6">
          <Components.Button label={gettext("Confirm my account")} opts={type: "submit"} class="mx-auto" />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.LayoutEmpty>
    """
  end
end
