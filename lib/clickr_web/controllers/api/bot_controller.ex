defmodule ClickrWeb.Api.BotController do
  use ClickrWeb, :controller
  use BotFramework.Phoenix.Controller
  alias Clickr.{Accounts, Buttons, Lessons}
  require Logger
  import Clickr.Gettext

  def handle_activity(
        _conn,
        %{"text" => <<"lesson:", lesson_id::binary-size(36), _::binary>>} = activity
      ) do
    case Lessons.by_id!(lesson_id).status do
      s when s not in ["ended", "graded"] -> nil
    end

    :ok = Lessons.Workflows.QuestionToTeams.MappingAgent.put(lesson_id, activity)

    %{
      "type" => "message",
      "text" => gettext("Hi! I'm waiting for the first question 😊.")
    }
  end

  def handle_activity(
        _conn,
        %{"value" => %{"action" => %{"data" => %{"action" => "clicked"}}}} = activity
      ) do
    # TODO Map via teams user ID to avoid name clashes (parallel lessons, students with same name)
    event = Buttons.Teams.ByName.clicked(activity["from"])
    Buttons.clicked(Accounts.system_user(), event)

    nil
  end

  def handle_activity(_conn, _activity) do
    nil
  end
end
