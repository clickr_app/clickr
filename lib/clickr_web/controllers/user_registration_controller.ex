defmodule ClickrWeb.UserRegistrationController do
  use ClickrWeb, :controller
  alias Clickr.Accounts
  alias Clickr.Accounts.User
  # alias ClickrWeb.UserAuth
  import Clickr.Gettext

  plug :assign_page_title

  def new(conn, _params) do
    changeset = Accounts.change_user_registration(%User{})

    conn
    |> put_flash(
      :error,
      gettext("Registration is disabled. You must be invited by an existing user.")
    )
    |> render("new.html", changeset: changeset)
  end

  def create(conn, %{"user" => _user_params}) do
    # TODO #tenants
    conn
    |> put_flash(:error, gettext("Registration is disabled."))
    |> render("new.html", changeset: Accounts.change_user_registration(%User{}))

    # case Accounts.register_user(user_params) do
    #   {:ok, user} ->
    #     {:ok, _} =
    #       Accounts.deliver_user_confirmation_instructions(
    #         user,
    #         &Routes.user_confirmation_url(conn, :edit, &1)
    #       )

    #     conn
    #     |> put_flash(:info, gettext("User created successfully."))
    #     |> UserAuth.log_in_user(user)

    #   {:error, %Ecto.Changeset{} = changeset} ->
    #     render(conn, "new.html", changeset: changeset)
    # end
  end

  defp assign_page_title(conn, _opts) do
    assign(conn, :page_title, gettext("Register"))
  end
end
