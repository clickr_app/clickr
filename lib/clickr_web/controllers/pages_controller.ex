defmodule ClickrWeb.PagesController do
  use ClickrWeb, :controller

  def tos(conn, _params) do
    render(conn, "tos.html")
  end

  def privacy(conn, _params) do
    render(conn, "privacy.html")
  end
end
