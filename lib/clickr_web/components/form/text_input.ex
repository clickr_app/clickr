defmodule ClickrWeb.Components.Form.TextInput do
  use ClickrWeb.Components.Form.Input

  def render(assigns) do
    ~F"""
    <Surface.Components.Form.TextInput {...attrs(assigns)} />
    """
  end
end
