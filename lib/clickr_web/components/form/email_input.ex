defmodule ClickrWeb.Components.Form.EmailInput do
  use ClickrWeb.Components.Form.Input

  def render(assigns) do
    ~F"""
    <Surface.Components.Form.EmailInput {...attrs(assigns)}/>
    """
  end
end
