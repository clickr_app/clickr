defmodule ClickrWeb.Components.Form.SearchInput do
  use ClickrWeb.Components.Form.Input

  def render(assigns) do
    ~F"""
    <Surface.Components.Form.SearchInput {...attrs(assigns)} />
    """
  end
end
