defmodule ClickrWeb.Components.Form.Select do
  use Surface.Component
  import Clickr.Gettext

  prop options, :list, required: true
  prop opts, :keyword, default: []
  prop error?, :boolean, default: false
  prop class, :css_class, default: []

  def render(assigns) do
    ~F"""
    <Surface.Components.Form.Input.InputContext :let={field: field}>
      <div
        id={"#{field}_select"}
        class={@class}
        {...@opts}
        data={options: Jason.encode!(@options)}
        x-init="$root.setAttribute('x-ignore', 'true'); selectedValue = options[0]?.value"
        x-data="{
          isOpen: false,
          max: 10,
          selectedValue: null,
          activeIndex: null,
          query: null,
          open() {
            this.isOpen = true
            this.$nextTick(() => { this.$refs.input.focus() })
          },
          close() {
            this.isOpen = false
            this.activeIndex = null
            this.$refs.button.focus()
            this.query = ''
          },
          get options() {
            return JSON.parse(this.$root.dataset.options)
          },
          get count() {
            return this.options.length
          },
          get activeValue() {
            return this.displayResults[this.activeIndex].value
          },
          choose(value) {
            this.selectedValue = value
            this.close()
          },
          next() {
            if (this.activeIndex == null) {
              this.activeIndex = 0
            } else {
              this.activeIndex = (this.activeIndex + 1) % this.count
            }
          },
          prev() {
            if (this.activeIndex == null) {
              this.activeIndex = this.count - 1
            } else {
              this.activeIndex = (this.activeIndex - 1 + this.count) % this.count
            }
          },
          get selected() {
            return this.options.find(o => o.value === this.selectedValue)
          },
          get results() {
            const q = (this.query || '').toLowerCase()
            const matches = i => i.label.toLowerCase().startsWith(q)

            return this.options
              .filter(matches)
          },
          get displayResults() {
            return this.results.slice(0, this.max)
          }
        }"
      >
        <Surface.Components.Form.HiddenInput opts={'x-ref': "hiddenInput", ':value': "selected?.value"} />
        <div class="relative">
          <button
            type="button"
            class="bg-white relative w-full border border-gray-300 rounded-md shadow-sm pl-3 pr-10 py-2 text-left cursor-default focus-within:outline-none focus-within:ring-1 focus-within:ring-primary-500 focus-within:border-primary-500 sm:text-sm"
            x-ref="button"
            @keydown.arrow-up.stop.prevent="open(); prev()"
            @keydown.arrow-down.stop.prevent="open(); next()"
            @keydown.enter.stop.prevent="choose(activeValue)"
            @keydown.space.stop.prevent="choose(activeValue)"
            @keydown.escape="close()"
            @click="open(); next()"
            aria-haspopup="listbox"
            :aria-expanded="isOpen"
          >
            <span x-show="!isOpen" x-text="selected?.label" class="border-0 p-0 block truncate text-sm h-5"></span>
            <input x-cloak x-show="isOpen" x-ref="input" type="text" x-model="query" class="border-0 p-0 block truncate focus:ring-0 text-sm h-5"/>
            <span class="absolute inset-y-0 right-0 flex items-center pr-2 pointer-events-none">
              {Heroicons.Solid.selector(class: "h-5 w-5 text-gray-400")}
            </span>
          </button>
          <ul
            x-cloak
            x-show="isOpen"
            class="absolute z-10 mt-1 w-full bg-white shadow-lg max-h-60 rounded-md py-1 text-base ring-1 ring-black ring-opacity-5 overflow-auto focus-within:outline-none sm:text-sm"
            @click.away="close()"
            x-ref="listbox"
            tabindex="-1"
            role="listbox"
          >
            <template x-for="(item, i) in displayResults">
              <li
                class="text-gray-900 cursor-default select-none relative py-2 pl-3 pr-9"
                @click="choose(item.value)"
                @mouseenter="activeIndex = i"
                @mouseleave="activeIndex = null"
                :class="{ 'text-white bg-primary-500': activeIndex === i, 'text-gray-900': !(activeIndex === i) }"
              >
                <span
                  class="font-normal block truncate"
                  :class="{ 'font-semibold': selectedValue === item.value, 'font-normal': !(selectedValue === item.value) }"
                  x-text="item.label"
                >
                </span>
                <span
                  class="text-primary-600 absolute inset-y-0 right-0 flex items-center pr-4"
                  :class="{ 'text-white': activeIndex === i, 'text-primary-600': !(activeIndex === i) }"
                  x-show="selectedValue === item.value"
                  style="display: none;"
                >
                  {Heroicons.Solid.check(class: "h-5 w-5")}
                </span>
              </li>
            </template>

            <li
              x-show="results.length == 0"
              class="text-gray-600 cursor-default select-none relative py-2 pl-3 pr-9"
            >
              <span class="font-normal block truncate">{gettext("No results")}</span>
            </li>

            <li
              x-show="results.length > displayResults.length"
              class="text-gray-600 cursor-default select-none relative py-2 pl-3 pr-9"
            >
              <span class="font-normal block truncate">...</span>
            </li>
          </ul>
        </div>
      </div>
    </Surface.Components.Form.Input.InputContext>
    """
  end
end
