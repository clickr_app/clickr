defmodule ClickrWeb.Components.Form.CheckboxField do
  use Surface.Component
  alias Surface.Components.{Context, Form}
  import ClickrWeb.ErrorHelpers

  prop label, :string, required: true
  prop name, :any, required: true
  prop class, :css_class
  slot default, required: true, args: [:error?]

  def render(assigns) do
    ~F"""
    <Context get={Form, form: form}>
      <Form.Field name={@name}>
        <div class="flex items-center">
          <#slot :args={error?: error?(form, @name)} />
          <Form.Label class="ml-2 block text-sm text-gray-700">
            {@label}
          </Form.Label>
        </div>
        {error_tag form, @name}
      </Form.Field>
    </Context>
    """
  end
end
