defmodule ClickrWeb.Components.Form.Field do
  use Surface.Component
  alias Surface.Components.{Context, Form}
  import ClickrWeb.ErrorHelpers

  prop label, :string
  prop name, :any, required: true
  prop class, :css_class
  prop label_opts, :keyword, default: []
  slot default, required: true, args: [:class, :error?]

  def render(assigns) do
    ~F"""
    <Context get={Form, form: form}>
      <Form.Field name={@name} class={@class}>
        <Form.Label :if={@label} opts={@label_opts} class="block text-sm font-medium text-gray-700">
          {@label}
        </Form.Label>
        <div class="mt-1 relative rounded-md shadow-sm">
          <#slot :args={class: (if error?(form, @name), do: "pr-10"), error?: error?(form, @name)} />
          {#if error?(form, @name)}
            <div class="absolute inset-y-0 right-0 pr-3 flex items-center pointer-events-none">
              {Heroicons.Solid.exclamation_circle(class: "h-5 w-5 text-red-500")}
            </div>
          {/if}
        </div>
        {error_tag form, @name}
      </Form.Field>
    </Context>
    """
  end
end
