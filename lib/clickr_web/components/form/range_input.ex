defmodule ClickrWeb.Components.Form.RangeInput do
  use ClickrWeb.Components.Form.Input

  def render(assigns) do
    ~F"""
    <Surface.Components.Form.RangeInput {...attrs(assigns)}/>
    """
  end
end
