defmodule ClickrWeb.Components.Form.PasswordInput do
  use ClickrWeb.Components.Form.Input

  def render(assigns) do
    ~F"""
    <Surface.Components.Form.PasswordInput {...attrs(assigns)} />
    """
  end
end
