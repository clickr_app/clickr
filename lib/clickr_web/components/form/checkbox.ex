defmodule ClickrWeb.Components.Form.Checkbox do
  use Surface.Component

  prop opts, :keyword, default: []
  prop class, :css_class, default: ""

  def render(assigns) do
    ~F"""
    <Surface.Components.Form.Checkbox
      opts={@opts}
      class={@class, "h-4 w-4 text-indigo-600 focus:ring-indigo-500 border-gray-300 rounded"}
    />
    """
  end
end
