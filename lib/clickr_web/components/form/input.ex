defmodule ClickrWeb.Components.Form.Input do
  defmacro __using__(_params) do
    quote do
      use Surface.Component

      prop error?, :boolean, default: false
      prop opts, :keyword, default: []
      prop class, :css_class, default: []
      prop id, :string
      prop name, :string
      prop blur, :event
      prop value, :any

      def attrs(assigns) do
        error? = assigns.error?

        class = [
          Surface.css_class(
            assigns.class ++
              [
                "block w-full sm:text-sm rounded-md focus:outline-none",
                "border-red-300 text-red-900 placeholder-red-300 focus:ring-red-500 focus:border-red-500":
                  error?,
                "border-gray-300 placeholder-gray-400 focus:ring-indigo-500 focus:border-indigo-500":
                  !error?
              ]
          )
        ]

        assigns
        |> Map.put(:class, class)
        |> Map.take([:opts, :class, :id, :name, :blur, :value])
      end
    end
  end
end
