defmodule ClickrWeb.Components.Button do
  use Surface.Component

  prop label, :string, required: true
  prop opts, :keyword, default: []
  prop variant, :string, default: "primary"
  prop class, :css_class, default: []

  def render(assigns) do
    class =
      case assigns.variant do
        "primary" -> ["btn-primary"]
        "danger" -> ["btn-danger"]
      end

    ~F"""
    <button {...@opts} class={class ++ @class}>
      {@label}
    </button>
    """
  end
end
