defmodule ClickrWeb.Components.ActionCard do
  use Surface.Component
  import Clickr.Gettext

  prop title, :string, required: true
  prop or_link, :keyword
  slot default, required: true

  def render(assigns) do
    ~F"""
    <div class="min-h-full flex flex-col justify-center py-12 sm:px-6 lg:px-8">
      <div class="sm:mx-auto sm:w-full sm:max-w-md">
        {!--<img class="mx-auto h-12 w-auto" src="https://tailwindui.com/img/logos/workflow-mark-indigo-600.svg" alt="Workflow">--}
        <h2 class="mt-6 text-center">
          {@title}
        </h2>
        {#if @or_link}
          <p class="mt-2 text-center text-sm text-gray-600">
            {gettext("or")}
            <a href={@or_link[:href]} class="font-medium text-indigo-600 hover:text-indigo-500">
              {@or_link[:text]}
            </a>
          </p>
        {/if}
      </div>

      <div class="mt-8 sm:mx-auto sm:w-full sm:max-w-md">
        <div class="bg-white py-8 px-4 shadow sm:rounded-lg sm:px-10">
          <#slot />
        </div>
      </div>
    </div>
    """
  end
end
