defmodule ClickrWeb.Components.DevicesWithLowBattery do
  use Surface.LiveView
  alias Clickr.Devices
  alias Clickr.Devices.Events
  import Clickr.Gettext

  data threshold, :number, default: 40.0
  data devices, :list

  @impl true
  def render(assigns) do
    ~F"""
    <div :if={length(@devices) > 0} x-data="{open: false}" @click.away="open = false" class="relative">
      <div>
        <button x-on:click="open = !open" type="button" class="max-w-xs bg-white flex items-center text-sm rounded-full focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500 text-red-600" id="user-menu-button" aria-expanded="false" aria-haspopup="true">
          <span class="sr-only">{gettext("Open list of devices with low battery")}</span>
          <svg class="w-8" aria-hidden="true" focusable="false" data-prefix="fas" data-icon="battery-quarter" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 640 512"><path fill="currentColor" d="M544 160v64h32v64h-32v64H64V160h480m16-64H48c-26.51 0-48 21.49-48 48v224c0 26.51 21.49 48 48 48h512c26.51 0 48-21.49 48-48v-16h8c13.255 0 24-10.745 24-24V184c0-13.255-10.745-24-24-24h-8v-16c0-26.51-21.49-48-48-48zm-336 96H96v128h128V192z"></path></svg>
        </button>
      </div>

      <div x-show="open" x-cloak class="origin-top-right absolute right-0 mt-2 w-72 rounded-md shadow-lg py-1 bg-white ring-1 ring-black ring-opacity-5 focus:outline-none"
        role="menu" aria-orientation="vertical" aria-labelledby="user-menu-button" tabindex="-1"
        x-transition:enter="transition ease-out duration-100"
        x-transition:enter-start="transform opacity-0 scale-95"
        x-transition:enter-end="transform opacity-100 scale-100"
        x-transition:leave="transition ease-in duration-75"
        x-transition:leave-start="transform opacity-100 scale-100"
        x-transition:leave-end="transform opacity-0 scale-95"
      >
        {#for device <- @devices}
          <div class="block px-4 py-2 text-sm">
            {device.name}: {Float.round(device.battery, 0)}%
            <span :if={device.updated_battery_at}>({Timex.from_now(device.updated_battery_at)})</span>
          </div>
        {/for}
      </div>
    </div>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    if connected?(socket), do: Devices.subscribe_all()

    {:ok, load_devices(socket)}
  end

  @impl true
  def handle_info(%Events.Upserted{}, socket) do
    {:noreply, load_devices(socket)}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  defp load_devices(socket) do
    assign(socket, devices: Devices.battery_less_than(socket.assigns.threshold, order_by: :name))
  end
end
