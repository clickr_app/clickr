defmodule ClickrWeb.Components.GridWithDragAndDrop do
  use Surface.Component

  prop id, :string, required: true
  prop data, :list, required: true
  prop class, :css_class, default: []
  prop opts, :keyword, default: []
  prop width, :integer, required: true
  prop height, :integer, required: true
  prop dragged, :event, required: true
  slot default, args: [:x, :y, item: ^data], required: true
  slot empty, args: [:x, :y]

  @impl true
  def render(assigns) do
    ~F"""
    <div
      x-data="{dragging: false}"
      x-init="$root.setAttribute('x-ignore', true)"
      role="list"
      class={"grid gap-1" | @class}
      style={
        'grid-template-columns': "repeat(#{@width}, minmax(0, 1fr));",
        'grid-template-rows': "repeat(#{@height}, minmax(0, 1fr));"
      }
      {...Map.new(@opts)}
      id={@id}
      :hook="GridWithDragAndDrop"
      @start-dragging="dragging = true"
      @stop-dragging="dragging = false"
    >
      {#for %{x: x, y: y, id: id} = item <- @data}
        <div
          id={"#{@id}_source_#{x}_#{y}"}
          style={'grid-column': x, 'grid-row': y}
          draggable="true"
          class="flex flex-grow cursor-move"
          data={x: x, y: y, id: id}
          @dragstart.self="
            dragging = true
            event.dataTransfer.effectAllowed = 'move'
            event.dataTransfer.setData('text/plain', event.target.id)
          "
          @dragend.self="dragging = false"
        >
          <#slot :args={item: item, x: x, y: y} />
        </div>
      {/for}

      {#for {x, y} <- drop_targets(@data, @width, @height)}
        <div
          class="flex"
          style={'grid-column': x, 'grid-row': y}
          id={"#{@id}_target_#{x}_#{y}"}
          data={x: x, y: y}
          @drop.self={"
            dragging = false

            const from = document.getElementById(event.dataTransfer.getData('text/plain')).dataset
            const to = event.target.dataset
            const hook = window.phxHooks.GridWithDragAndDrop[$root.id]
            #{if @dragged.target == :live_view do
              "hook.pushEvent('#{@dragged.name}', {from, to})"
            else
              "hook.pushEventTo('#{@dragged.target}', '#{@dragged.name}', {from, to})"
            end}
          "}
          @dragover.self="dragging = $el"
          @dragleave.self="dragging = true"
          :class="{
            'border border-transparent': !dragging,
            'border border-gray-200': dragging,
            'border-gray-500': dragging == $el,
          }"
        >
          <div class="flex flex-grow" :class="{invisible: dragging}">
            <#slot name="empty" :args={x: x, y: y} />
          </div>
        </div>
      {/for}
    </div>
    """
  end

  defp drop_targets(data, width, height) do
    used = MapSet.new(data, &{&1.x, &1.y})
    all = MapSet.new(for x <- 1..width, y <- 1..height, do: {x, y})
    MapSet.difference(all, used)
  end
end
