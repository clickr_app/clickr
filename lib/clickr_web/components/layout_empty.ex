defmodule ClickrWeb.Components.LayoutEmpty do
  use Surface.Component
  alias ClickrWeb.Components.Layout

  prop conn, :map, required: true
  prop v_centered?, :boolean, default: true
  prop container, :atom, default: :md, values: [:xl, :md, :none]
  slot default, required: true

  def render(assigns) do
    ~F"""
    <main class="flex flex-col min-h-screen mt-5">
      <Layout.Flash live_view?={false} type={:info} text={Phoenix.Controller.get_flash(@conn, :info)} />
      <Layout.Flash live_view?={false} type={:error} text={Phoenix.Controller.get_flash(@conn, :error)} />

      <div class={"flex flex-grow flex-col", 'justify-center': @v_centered?, 'container-md': @container == :md, 'container-xl': @container == :xl}>
        <#slot />
      </div>
    </main>
    """
  end
end
