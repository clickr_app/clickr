defmodule ClickrWeb.Components.Modal do
  use Surface.LiveComponent

  prop title, :string, required: true

  data show, :boolean, default: false

  slot default

  def render(assigns) do
    ~F"""
    <div>
      <div :if={@show} class="fixed z-10 inset-0 overflow-y-auto" aria-labelledby="modal-title" role="dialog" aria-modal="true">
        <div class="flex items-end justify-center min-h-screen pt-4 px-4 pb-20 text-center sm:block sm:p-0">
          <div :on-click="hide" class="fixed inset-0 bg-gray-500 bg-opacity-75" aria-hidden="true"></div>

          <!-- This element is to trick the browser into centering the modal contents. -->
          <span class="hidden sm:inline-block sm:align-middle sm:h-screen" aria-hidden="true">&#8203;</span>

          <div
            class="relative inline-block align-bottom bg-white rounded-lg px-4 pt-5 pb-4 text-left overflow-hidden shadow-xl transform transition-all sm:my-8 sm:align-middle sm:max-w-sm sm:w-full sm:p-6"
            :on-window-keydown="hide"
            phx-key="Escape"
          >
            <div>
              {!--<div class="mx-auto flex items-center justify-center h-12 w-12 rounded-full bg-green-100">
                {Heroicons.Outline.check(class: 'h-6 w-6 text-green-600')}
              </div>--}
              <div class="text-center">
                <h3 class="text-lg leading-6 font-medium text-gray-900" id="modal-title">{@title}</h3>
                <div class="mt-2">
                  <#slot />
                </div>
              </div>
            </div>
            {!--<div class="mt-5 sm:mt-6">
              <button :on-click="hide" type="button" class="inline-flex justify-center w-full rounded-md border border-transparent shadow-sm px-4 py-2 bg-indigo-600 text-base font-medium text-white hover:bg-indigo-700 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-indigo-500 sm:text-sm">
                Close
              </button>
            </div>--}
          </div>
        </div>
      </div>
    </div>
    """
  end

  # Public API

  def show(modal_id) do
    send_update(__MODULE__, id: modal_id, show: true)
  end

  def hide(modal_id) do
    send_update(__MODULE__, id: modal_id, show: false)
  end

  # Event handlers

  def handle_event("show", _, socket) do
    {:noreply, assign(socket, show: true)}
  end

  def handle_event("hide", _, socket) do
    {:noreply, assign(socket, show: false)}
  end
end
