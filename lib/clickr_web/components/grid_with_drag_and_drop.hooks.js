export const GridWithDragAndDrop = {
    mounted() {
        const hook = "GridWithDragAndDrop"
        window.phxHooks ||= {}
        window.phxHooks[hook] ||= {}
        window.phxHooks[hook][this.el.id] = this
    }
}
