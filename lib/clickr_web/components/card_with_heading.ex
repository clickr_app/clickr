defmodule ClickrWeb.Components.CardWithHeading do
  use Surface.Component

  alias ClickrWeb.Components

  slot default, required: true
  slot header, required: false

  prop heading, :string, required: false
  prop class, :css_class, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <div class={@class}>
      <#slot name="header">
        <h1 class="text-center">{@heading}</h1>
      </#slot>

      <Components.Card class={"mt-8"}>
        <#slot />
      </Components.Card>
    </div>
    """
  end
end
