defmodule ClickrWeb.Components.Card do
  use Surface.Component

  slot default, required: true
  prop class, :css_class, default: []
  prop border?, :boolean, default: false
  prop opts, :keyword, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <div {...@opts} class={["bg-white py-8 px-4 sm:rounded-lg sm:px-10", shadow: !@border?, 'border border-black-300': @border?] ++ @class}>
      <#slot />
    </div>
    """
  end
end
