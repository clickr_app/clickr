defmodule ClickrWeb.Components.Rename do
  use Surface.LiveComponent
  alias ClickrWeb.Components.Form
  require Logger
  import Clickr.Gettext

  prop rename_fn, :fun, required: true
  prop display_value, :string, default: nil
  prop value, :string, required: true
  data rename?, :boolean, default: false

  @impl true
  def render(assigns) do
    ~F"""
    <div class="text-center">
      <button :if={!@rename?} class="group" :on-click="show_rename">
        <h1>
          <span class="border-2 border-transparent group-hover:border-gray-400 p-2 flex">
            {@display_value || @value}
            {Heroicons.Solid.pencil(class: "hidden group-hover:inline ml-2 h-8 w-8 text-gray-600")}
          </span>
        </h1>
      </button>
      <Form.TextInput :if={@rename?} class="max-w-md mx-auto" blur="rename" value={@value} opts={required: true, 'x-trap': "true", 'phx-keyup': "rename_keyup"} />
    </div>
    """
  end

  @impl true
  def handle_event("show_rename", _params, socket) do
    {:noreply, assign(socket, rename?: true)}
  end

  def handle_event("rename_keyup", %{"key" => "Escape"}, socket) do
    {:noreply, assign(socket, rename?: false)}
  end

  def handle_event("rename_keyup", %{"key" => "Enter", "value" => name}, socket) do
    {:noreply, rename(socket, name)}
  end

  def handle_event("rename_keyup", _, socket) do
    {:noreply, socket}
  end

  def handle_event("rename", %{"value" => name}, %{assigns: %{rename?: true}} = socket) do
    {:noreply, rename(socket, name)}
  end

  def handle_event("rename", _, socket) do
    {:noreply, socket}
  end

  defp rename(%{assigns: %{value: old}} = socket, new) when old == new do
    assign(socket, rename?: false)
  end

  defp rename(socket, name) do
    case socket.assigns.rename_fn.(name) do
      {:ok, _} ->
        socket
        |> assign(rename?: false)
        |> put_flash(:info, gettext("Renamed"))

      error ->
        Logger.error("Failed to rename: #{inspect(error)}")
        put_flash(socket, :error, gettext("Failed to rename"))
    end
  end
end
