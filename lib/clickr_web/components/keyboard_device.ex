defmodule ClickrWeb.Components.KeyboardDevice do
  use Surface.LiveComponent
  alias Clickr.Buttons
  require Logger
  import Clickr.Gettext

  prop user, :map, required: true
  prop class, :css_class, default: ""
  data last_key, :string, default: nil

  @impl true
  def render(assigns) do
    ~F"""
      <div :on-window-keyup="keyup" class={@class} id="keyboard-device" tabindex="0">
        {#if @last_key}
          <span class="font-bold">'{@last_key}'</span> {gettext("pressed")}
        {/if}
      </div>
    """
  end

  @impl true
  def handle_event("keyup", %{"key" => key}, socket) do
    key = String.downcase(key)

    if String.length(key) == 1 do
      event = Buttons.Keyboard.KeyUp.clicked(socket.assigns.user, key)

      case Buttons.clicked(socket.assigns.user, event) do
        :ok ->
          {:noreply, assign(socket, last_key: key)}

        e ->
          Logger.error("Failed to handle key up: #{inspect(e)}")
          {:noreply, put_flash(socket, :error, gettext("Failed to handle key up"))}
      end
    else
      {:noreply, socket}
    end
  end
end
