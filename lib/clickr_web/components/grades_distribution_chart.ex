defmodule ClickrWeb.Components.GradesDistributionChart do
  use Surface.Component
  alias Clickr.Grades

  prop grades, :list, required: true
  prop width, :integer, default: 600
  prop height, :integer, default: 400
  prop class, :css_class, default: []

  def render(%{grades: grades, width: width, height: height} = assigns) when length(grades) > 1 do
    buckets = 10

    chart =
      grades
      |> Grades.Distribution.buckets(buckets)
      |> Enum.map(fn {{from, to}, count} -> ["#{trunc(from)} - #{trunc(to)} %", count] end)
      |> Contex.Dataset.new()
      |> Contex.Plot.new(Contex.BarChart, width, height, colour_palette: ["F3940D"])
      |> Contex.Plot.to_svg()

    style =
      Phoenix.HTML.raw("""
        <style type="text/css" scoped>
          svg { max-width: #{width}px; max-height: #{height}px; }
        </style>
      """)

    ~F"""
    <div class={"flex justify-center" | @class}>
      {style}
      {chart}
    </div>
    """
  end

  def render(assigns) do
    ~F"""
    """
  end
end
