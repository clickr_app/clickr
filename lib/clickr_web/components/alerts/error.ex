defmodule ClickrWeb.Components.Alerts.Error do
  use Surface.Component

  prop text, :string, required: true
  prop class, :css_class, default: []

  def render(assigns) do
    ~F"""
    <div class={"rounded-md bg-red-50 p-4" | @class}>
      <div class="flex">
        <div class="flex-shrink-0">
          {Heroicons.Solid.x_circle(class: "h-5 w-5 text-red-400")}
        </div>
        <div class="ml-3">
          <h3 class="text-sm font-medium text-red-800">
            {@text}
          </h3>
        </div>
      </div>
    </div>
    """
  end
end
