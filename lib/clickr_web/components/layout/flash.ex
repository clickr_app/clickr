defmodule ClickrWeb.Components.Layout.Flash do
  use Surface.Component
  import Clickr.Gettext

  prop type, :atom, values!: [:info, :error], default: :info
  prop text, :string
  prop live_view?, :boolean, default: true

  def render(assigns) do
    type = assigns[:type]
    info? = type == :info
    error? = type == :error

    ~F"""
    {#if @text}
      <div class="sm:mx-auto sm:w-full sm:max-w-md mb-4" x-data={"{ live_view: #{@live_view?} }"}>
        <div class={"rounded-md p-4", 'bg-green-50': info?, 'bg-red-50': error?}>
          <div class="flex items-center">
            <div class="flex-shrink-0">
              {#if info?}
                {Heroicons.Solid.check_circle(class: "h-5 w-5 text-green-400")}
              {/if}
              {#if error?}
                {Heroicons.Solid.x_circle(class: "h-5 w-5 text-red-400")}
              {/if}
            </div>
            <div class="ml-3">
              <p class={"text-sm font-medium", 'text-green-700': info?, 'text-red-700': error?}>
                {@text}
              </p>
            </div>
            <div class="ml-auto pl-3">
              <div class="-mx-1.5 -my-1.5">
                <button :on-click="lv:clear-flash" @click="if (!live_view) $root.remove()" :values={key: @type} type="button" class={
                  "inline-flex rounded-md p-1.5 focus:outline-none focus:ring-2 focus:ring-offset-2",
                  'text-green-500 hover:bg-green-100 focus:ring-offset-green-50 focus:ring-green-600': info?,
                  'text-red-500 hover:bg-red-100 focus:ring-offset-red-50 focus:ring-red-600': error?
                }>
                  <span class="sr-only">{gettext("Dismiss")}</span>
                  {Heroicons.Solid.x(class: "h-5 w-5")}
                </button>
              </div>
            </div>
          </div>
        </div>
      </div>
    {/if}
    """
  end
end
