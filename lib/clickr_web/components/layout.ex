defmodule ClickrWeb.Components.Layout do
  use Surface.Component
  alias Heroicons.{Outline, Solid}
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Layout
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  slot default, required: true
  prop socket, :map, required: false
  prop current_user, :map
  prop live_view?, :boolean, default: true
  prop flash, :map
  prop conn, :map
  prop v_centered?, :boolean, default: true
  prop container, :atom, default: :md, values: [:xl, :md, :none]
  prop opts, :keyword, default: []
  prop extra_wide?, :boolean, default: false

  prop entries, :list,
    default:
      Enum.filter(
        [
          {gettext("Lessons"), &Solid.play/1, "/lessons"},
          {gettext("Classes"), &Solid.user_group/1, "/classes"},
          {gettext("Rooms"), &Solid.home/1, "/rooms"},
          {gettext("Seating Plans"), &Solid.view_grid/1, "/seating-plans"}
        ],
        fn e -> e end
      )

  prop logged_in_entries, :list,
    default: [
      {:live_redirect, gettext("Settings"), "/users/settings"},
      {:live_redirect, gettext("API Token"), "/users/api_token"},
      {:live_redirect, gettext("Invite"), "/users/invite"},
      {:live_redirect, gettext("Dashboard"), "/dashboard"},
      {:link, gettext("Sign out"), "/users/log_out", [method: :delete]}
    ]

  prop logged_out_entries, :list,
    default: [
      {gettext("Register"), "/users/register"},
      {gettext("Log in"), "/users/log_in"}
    ]

  prop branding, :string, default: "KlassenKnopf"

  def render(assigns) do
    ~F"""
    <Context put={__MODULE__, current_user: @current_user}>
      <div x-data="{open: false}" class="h-screen flex overflow-hidden bg-gray-100" {...@opts}>
        <div x-show="open" x-cloak class={"fixed inset-0 flex z-40", 'lg:hidden': !@extra_wide?} role="dialog" aria-modal="true">
          <div class="fixed inset-0 bg-gray-600 bg-opacity-75" aria-hidden="true"
            x-transition:enter="transition-opacity ease-linear duration-300"
            x-transition:enter-start"opacity-0"
            x-transition:enter-end"opacity-100"
            x-transition:leave="transition-opacity ease-linear duration-300"
            x-transition:leave-start"opacity-100"
            x-transition:leave-end"opacity-0"
          ></div>

          <div class="relative flex-1 flex flex-col max-w-xs w-full pb-4 bg-gray-800"
            x-transition:enter="transition ease-in-out duration-300 transform"
            x-transition:enter-start="-translate-x-full"
            x-transition:enter-end="translate-x-0"
            x-transition:leave="transition ease-in-out duration-300 transform"
            x-transition:leave-start="translate-x-0"
            x-transition:leave-end="-translate-x-full"
          >
            <!-- Close button, show/hide based on off-canvas menu state. -->
            <div class="absolute top-0 right-0 -mr-12 pt-2"
              x-transition:enter="ease-in-out duration-300"
              x-transition:enter-start="opacity-0"
              x-transition:enter-end="opacity-100"
              x-transition:leave="ease-in-out duration-300"
              x-transition:leave-start="opacity-100"
              x-transition:leave-end="opacity-0"
            >
              <button type="button" x-on:click="open = false" class="ml-1 flex items-center justify-center h-10 w-10 rounded-full focus:outline-none focus:ring-2 focus:ring-inset focus:ring-white">
                <span class="sr-only">{gettext("Close sidebar")}</span>
                {Outline.x(class: "h-6 w-6 text-white")}
              </button>
            </div>

            <div class="flex-shrink-0 flex items-center h-16 px-4 bg-gray-900">
              <img class="h-8 w-auto mr-4" src={Routes.static_path(ClickrWeb.Endpoint, "/images/logo_transparent.png")} alt={@branding} />
              <span class="text-white font-bold text-2xl">{@branding}</span>
            </div>
            <div class="mt-5 flex-1 h-0 overflow-y-auto">
              <nav class="px-2 space-y-1">
                {#for {text, icon, to} <- @entries}
                  <.menu_entry {=text} {=icon} {=to} current={false} />
                {/for}
              </nav>
            </div>
          </div>

          <div class="flex-shrink-0 w-14" aria-hidden="true">
            <!-- Dummy element to force sidebar to shrink to fit close icon -->
          </div>
        </div>

        <!-- Static sidebar for desktop -->
        <div class={"hidden", 'lg:flex lg:flex-shrink-0': !@extra_wide?}>
          <div class="flex flex-col w-64">
            <!-- Sidebar component, swap this element with another sidebar if you like -->
            <div class="flex-1 flex flex-col min-h-0">
              <div class="flex items-center h-16 flex-shrink-0 px-4 bg-gray-900">
                <img class="h-8 w-auto mr-4" src={Routes.static_path(ClickrWeb.Endpoint, "/images/logo_transparent.png")} alt={@branding} />
                <span class="text-white font-bold text-2xl">{@branding}</span>
              </div>
              <div class="flex-1 flex flex-col overflow-y-auto">
                <nav class="flex-1 px-2 py-4 bg-gray-800 space-y-1">
                  {#for {text, icon, to} <- @entries}
                    <.menu_entry {=text} {=icon} {=to} current={false} />
                  {/for}
                </nav>
              </div>
            </div>
          </div>
        </div>
        <div class="flex flex-col w-0 flex-1 overflow-hidden">
          <div class="relative z-10 flex-shrink-0 flex h-16 bg-white shadow">
            <button x-on:click="open = true" type="button" class={"px-4 border-r border-gray-200 text-gray-500 focus:outline-none focus:ring-2 focus:ring-inset focus:ring-primary-500", 'lg:hidden': !@extra_wide?}>
              <span class="sr-only">{gettext("Open sidebar")}</span>
              {Outline.menu_alt_2(class: "h-6 w-6")}
            </button>
            <div class="flex-1 px-4 flex justify-between">
              <div class="flex-1 flex">
                {!--<form class="w-full flex lg:ml-0 mb-0" action="#" method="GET">
                  <label for="search-field" class="sr-only">Search</label>
                  <div class="relative w-full text-gray-400 focus-within:text-gray-600">
                    <div class="absolute inset-y-0 left-0 flex items-center pointer-events-none">
                      {Solid.search(class: "h-5 w-5")}
                    </div>
                    <input id="search-field" class="block w-full h-full pl-8 pr-3 py-2 border-transparent text-gray-900 placeholder-gray-500 focus:outline-none focus:placeholder-gray-400 focus:ring-0 focus:border-transparent sm:text-sm" placeholder="Search" type="search" name="search">
                  </div>
                </form>--}
              </div>
              <div class="ml-4 flex items-center lg:ml-6 gap-3">
                {#if @live_view? && @current_user}
                  <Components.KeyboardDevice id="keyboard_device" user={@current_user} />
                  {live_render @socket, Components.DevicesWithLowBattery, id: "devices_with_low_battery"}
                {/if}

                {#if @current_user}
                  {!--<button type="button" class="bg-white p-1 rounded-full text-gray-400 hover:text-gray-500 focus:outline-none focus:ring-2 focus:ring-offset-2 focus:ring-primary-500">
                    <span class="sr-only">View notifications</span>
                    {Outline.bell(class: "h-6 w-6")}
                  </button>--}

                  <Layout.ProfileDropdown user={@current_user} entries={@logged_in_entries} />
                {#else}
                  {#for {text, to} <- @logged_out_entries}
                    <.logged_out_entry {=text} {=to} />
                  {/for}
                {/if}
              </div>
            </div>
          </div>

          <main class="flex-grow flex relative overflow-y-auto focus:outline-none py-6 sm:px-6 lg:px-8">
            <div class="flex flex-col flex-grow">
              {#if @flash}
                <Layout.Flash {=@live_view?} type={:info} text={live_flash(@flash, :info)} />
                <Layout.Flash {=@live_view?} type={:error} text={live_flash(@flash, :error)} />
              {/if}
              {#if @conn}
                <Layout.Flash {=@live_view?} type={:info} text={Phoenix.Controller.get_flash(@conn, :info)} />
                <Layout.Flash {=@live_view?} type={:error} text={Phoenix.Controller.get_flash(@conn, :error)} />
              {/if}
              <div class={"flex flex-grow flex-col", 'justify-center': @v_centered?, 'container-md': @container == :md, 'container-xl': @container == :xl}>
                <#slot />
              </div>
            </div>
          </main>
        </div>
      </div>
    </Context>
    """
  end

  defp logged_out_entry(assigns) do
    ~F"""
    <Surface.Components.LiveRedirect label={@text} {=@to} class="text-gray-700 text-sm ml-3" />
    """
  end

  defp menu_entry(assigns) do
    ~F"""
    <Surface.Components.LiveRedirect {=@to} class={
      "group flex items-center px-2 py-2 text-sm font-medium rounded-md",
      'bg-gray-900 text-white': @current,
      'text-gray-300 hover:bg-gray-700 hover:text-white': !@current
    }>
      {@icon.(class: Surface.css_class([
        "mr-3 flex-shrink-0 h-6 w-6",
        "text-gray-300": @current,
        "text-gray-400 group-hover:text-gray-300": !@current
      ]))}
      {@text}
    </Surface.Components.LiveRedirect>
    """
  end
end
