defmodule ClickrWeb.Components.Delete do
  use Surface.LiveComponent
  require Logger
  import Clickr.Gettext
  import Clickr.Gettext

  prop delete_fn, :fun, required: true
  prop class, :css_class, default: []
  prop icon_class, :css_class, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <button :on-click="delete" class={["btn-danger !w-auto flex items-center" | @class]}>
      {Heroicons.Solid.trash(class: Surface.css_class(["h-5 w-5 mr-2" | @icon_class]))}
      {gettext("Delete")}
    </button>
    """
  end

  @impl true
  def handle_event("delete", _params, socket) do
    case socket.assigns.delete_fn.() do
      {:ok, _} ->
        {:noreply, put_flash(socket, :info, gettext("Deleted"))}

      error ->
        Logger.error("Failed to delete: #{inspect(error)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to delete"))}
    end
  end
end
