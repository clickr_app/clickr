defmodule ClickrWeb.Channels.Deconz do
  use Phoenix.Channel
  alias Clickr.{Buttons, Devices}
  alias Clickr.Buttons.Events.Clicked
  require Logger

  @get_devices_every_minute 60 * 1_000

  @impl true
  def join("deconz", %{"gateway_id" => id}, socket) do
    send(self(), :get_devices)
    {:ok, assign(socket, gateway_id: id)}
  end

  @impl true
  def handle_in(
        "event",
        %{"e" => "changed", "r" => "sensors"} = msg,
        %{assigns: %{devices: devices}} = socket
      ) do
    button = devices[msg["uniqueid"]]

    case Buttons.Deconz.clicked(button, msg) do
      %Clicked{} = event -> Buttons.clicked(socket.assigns.current_user, event)
      nil -> Logger.error("Failed to handle deconz click event: #{inspect(msg)}")
    end

    {:noreply, socket}
  end

  def handle_in("sensors", msg, socket) do
    devices =
      msg
      |> Map.values()
      |> Enum.reject(&(&1["type"] == "Daylight"))

    for device <- devices do
      Task.start(fn -> upsert_device(socket, device) end)
    end

    {:noreply, assign(socket, devices: Map.new(devices, &{&1["uniqueid"], &1}))}
  end

  def handle_in("event", msg, socket) do
    Logger.info("Ignore deconz event #{inspect(msg)}")
    {:noreply, socket}
  end

  def handle_in(type, msg, socket) do
    Logger.info("Ignore #{type} event #{inspect(msg)}")
    {:noreply, socket}
  end

  @impl true
  def handle_info(:get_devices, socket) do
    Process.send_after(self(), :get_devices, @get_devices_every_minute)
    push(socket, "get_sensors", %{})
    {:noreply, socket}
  end

  defp upsert_device(socket, device) do
    id = Devices.deconz_id(device)
    user = socket.assigns.current_user
    opts = [consistency: :eventual]

    name = device["name"]
    battery = battery(device)
    attrs = [id: id, gateway_id: socket.assigns.gateway_id, name: name, battery: battery]

    Devices.upsert(user, attrs, opts)
  end

  defp battery(%{"config" => %{"battery" => battery}}) when is_number(battery), do: battery * 1.0
  defp battery(_), do: nil
end
