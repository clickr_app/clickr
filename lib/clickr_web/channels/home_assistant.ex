defmodule ClickrWeb.Channels.HomeAssistant do
  use Phoenix.Channel
  alias Clickr.Buttons
  alias Clickr.Buttons.Events.Clicked
  alias Clickr.Buttons.Zigbee.IkeaTradfriRemote

  @impl true
  def join("home_assistant", _payload, socket) do
    {:ok, socket}
  end

  @impl true
  def handle_in("event", %{"event" => %{"data" => data}}, socket) do
    case IkeaTradfriRemote.home_assistant_clicked(data) do
      %Clicked{} = event -> Buttons.clicked(socket.assigns.current_user, event)
      nil -> nil
    end

    {:noreply, socket}
  end
end
