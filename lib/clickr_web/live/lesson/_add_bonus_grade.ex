defmodule ClickrWeb.Lesson.AddBonusGrade do
  use Surface.Component

  prop student_id, :string, required: true

  def render(assigns) do
    ~F"""
    <button
      :on-click="add_bonus_grade"
      :values={'student-id': @student_id}
      class="h-full flex-grow flex justify-center items-center hover:bg-primary-100 hover:bg-opacity-50 border-r"
    >
      {Heroicons.Solid.sparkles(class: "h-6 w-6")}
    </button>
    """
  end
end
