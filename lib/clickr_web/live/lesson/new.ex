defmodule ClickrWeb.Lesson.New do
  use Surface.LiveView
  require Logger
  alias Clickr.{Classes, Rooms, Lessons}
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Lesson.Redirect
  import Clickr.Gettext

  data page_title, :string, default: gettext("New lesson")
  data errors, :list, default: []
  data class_options, :list, default: []
  data room_options, :list, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <Components.ActionCard title={gettext("New lesson")}>
        <Surface.Components.Form for={:lesson} errors={@errors} submit="start" class="space-y-6">
          <Components.Alerts.Error :if={not Enum.empty?(@errors)} text={gettext("An error occured. See details below.")} class="mb-3" />

          <Form.Field label={gettext("Class")} name={:class_id} :let={class: class, error?: error?}>
            <Form.Select options={@class_options} {=class} {=error?} />
          </Form.Field>

          <Form.Field label={gettext("Room")} name={:room_id} :let={class: class, error?: error?}>
            <Form.Select options={@room_options} {=class} {=error?} />
          </Form.Field>

          <Components.Button class="mt-5 mx-auto" label={pgettext("imperative_verbs", "Start")} opts={'phx-disable-with': gettext("Starting...")} />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> load_class_options()
     |> load_room_options()}
  end

  @impl true
  def handle_event("start", %{"lesson" => params}, socket) do
    case Lessons.start(socket.assigns.current_user, params) do
      {:ok, lesson} ->
        {:noreply,
         socket
         |> put_flash(:info, gettext("Lesson started"))
         |> push_redirect(to: Redirect.path(socket, lesson))}

      {:error, :validation_failure, errors} ->
        {:noreply, assign(socket, errors: errors)}

      e ->
        Logger.error("Could not start lesson: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Could not start lesson"))}
    end
  end

  def load_class_options(socket) do
    {classes, _count} = Classes.list()
    options = Enum.map(classes, &%{label: &1.name, value: &1.id})
    assign(socket, class_options: options)
  end

  def load_room_options(socket) do
    {rooms, _count} = Rooms.list()
    options = Enum.map(rooms, &%{label: &1.name, value: &1.id})
    assign(socket, room_options: options)
  end
end
