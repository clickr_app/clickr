defmodule ClickrWeb.Lesson.Graded do
  use Surface.LiveView
  require Logger
  alias Clickr.{Grades, Lessons, Students}
  alias Clickr.Lessons.Events
  alias ClickrWeb.Components
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Lesson")
  data lesson_rm, :map
  data students, :map
  data grades, :map

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:none} extra_wide?={true}>
      <ClickrWeb.Lesson.Layout {=@lesson_rm} students={@lesson_rm.students} id={@lesson_rm.id} {=@current_user}>
        <Components.GradesDistributionChart grades={Map.values(@lesson_rm.grades)} width={900} height={400} class="mt-5" />

        <:student :let={student: student}>
          <ClickrWeb.Lesson.StudentCard
            {=student}
            attending?={student.id in @lesson_rm.attending}
          >
            <div class="flex flex-row">
              <div title={gettext("Lesson")} :if={student.id in @lesson_rm.attending} class="flex-grow text-center text-xl">{percent(@lesson_rm.grades[student.id])}</div>
              <div :if={student.id in @students} title={grades_title(@grades[student.id])} class="flex-grow text-center text-xl">{percent(@students[student.id].current_grade)}</div>
            </div>
          </ClickrWeb.Lesson.StudentCard>
        </:student>
      </ClickrWeb.Lesson.Layout>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    if connected?(socket), do: Lessons.subscribe(id)

    {:ok,
     socket
     |> assign(lesson_id: id)
     |> load()
     |> ClickrWeb.Lesson.Redirect.assert_status_or_redirect()}
  end

  @impl true
  def handle_info(%Events.Deleted{}, socket) do
    {:noreply, push_redirect(socket, to: Routes.lessons_path(socket, :list))}
  end

  # TODO Show udpated grade after finishing grading (need to hard reload)
  # def handle_info(%Grades.Events.Changed{}, socket) do
  #   {:noreply, load(socket)}
  # end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp load(socket) do
    rm = Lessons.by_id!(socket.assigns.lesson_id)

    student_ids = Enum.map(rm.students, fn s -> s.id end)

    students =
      Students.by_ids(student_ids)
      |> Map.new(fn s -> {s.id, s} end)

    grade_ids = Enum.map(students, fn {_, s} -> s.current_grade_id end)

    grades_by_id =
      Grades.with_details_by_ids(grade_ids)
      |> Map.new(fn g -> {g.id, g} end)

    grades = Map.new(students, fn {_, s} -> {s.id, grades_by_id[s.current_grade_id]} end)

    assign(socket, lesson_rm: rm, students: students, grades: grades)
  end

  defp percent(float), do: "#{Float.round(float || 0.0, 1)} %"

  defp grades_title(nil), do: gettext("Overall")

  defp grades_title(grade) do
    lessons = (grade.lessons || []) |> Enum.map(fn l -> percent(l.percent) end) |> Enum.join(", ")
    bonus = (grade.bonus || []) |> Enum.map(&percent/1) |> Enum.join(", ")

    "#{gettext("Overall")}: #{gettext("Lessons")}(#{lessons}), #{gettext("Bonus")}(#{bonus})"
  end
end
