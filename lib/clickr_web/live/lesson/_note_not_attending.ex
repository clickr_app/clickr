defmodule ClickrWeb.Lesson.NoteNotAttending do
  use Surface.Component
  alias Clickr.Lessons
  alias Clickr.Lessons.Commands

  prop student_id, :string, required: true

  def render(assigns) do
    ~F"""
    <button
      :on-click="note_not_attending"
      :values={'student-id': @student_id}
      class="h-full flex-grow flex justify-center items-center hover:bg-primary-100 hover:bg-opacity-50 border-r"
    >
      {Heroicons.Solid.user_remove(class: "h-6 w-6")}
    </button>
    """
  end

  def handle_event(%{"student-id" => student_id}, socket) do
    cmd = %Commands.NoteLateArrival{id: socket.assigns.lesson_id, student_id: student_id}
    Lessons.note_not_attending(socket.assigns.current_user, cmd)
  end
end
