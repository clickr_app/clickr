defmodule ClickrWeb.Lesson.Redirect do
  require Logger
  import Phoenix.LiveView
  alias Clickr.Lessons
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  defmodule View do
    use Surface.LiveView
    require Logger

    alias Clickr.Lessons
    alias ClickrWeb.Router.Helpers, as: Routes

    @impl true
    def render(assigns),
      do: ~F"""
      """

    @impl true
    def mount(%{"id" => id}, _session, socket) do
      lesson = Lessons.by_id!(id)
      {:ok, ClickrWeb.Lesson.Redirect.push_redirect(socket, lesson, replace: true)}
    end
  end

  def handle_dispatch_result(socket, text, result) do
    case result do
      {:ok, lesson} ->
        {:noreply, push_patch_or_redirect(socket, lesson, replace: true)}

      e ->
        Logger.error("Failed to #{text}: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to %{text}", text: text))}
    end
  end

  def push_patch_or_redirect(socket, %{status: status} = lesson, opts \\ []) do
    if status in socket.assigns.aliases do
      push_patch(socket, lesson, opts)
    else
      push_redirect(socket, lesson, opts)
    end
  end

  def assert_status_or_redirect(socket, aliases \\ []) do
    action = Atom.to_string(socket.assigns.live_action)
    lesson = socket.assigns.lesson_rm

    if lesson.status in [action | aliases] do
      Phoenix.LiveView.assign(socket, aliases: aliases)
    else
      push_redirect(socket, lesson, replace: true)
    end
  end

  def path(socket, lesson) do
    Routes.lesson_path(socket, String.to_atom(lesson.status), lesson.id)
  end

  def push_patch(socket, lesson, opts \\ []) do
    opts = Keyword.merge([to: path(socket, lesson)], opts)
    Phoenix.LiveView.push_patch(socket, opts)
  end

  def push_redirect(socket, lesson, opts \\ []) do
    opts = Keyword.merge([to: path(socket, lesson)], opts)
    Phoenix.LiveView.push_redirect(socket, opts)
  end
end
