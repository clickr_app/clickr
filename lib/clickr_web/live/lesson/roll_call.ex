defmodule ClickrWeb.Lesson.RollCall do
  use Surface.LiveView
  require Logger
  alias Clickr.{Buttons, Lessons}
  alias Clickr.Lessons.Events
  alias ClickrWeb.Components
  alias ClickrWeb.Lesson.Redirect
  import ClickrWeb.Lesson.WithRaisedHands
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Lesson")
  data lesson_ag, :map
  data lesson_rm, :map
  data raised_hands, :mapset, default: MapSet.new()
  data status, :string

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:none} extra_wide?={true}>
      <ClickrWeb.Lesson.Layout {=@lesson_rm} {=@students} phx_update_student="append" id={@lesson_rm.id} {=@current_user}>
        <:buttons>
          <button x-data={"{text: 'lesson:#{@lesson_rm.id}'}"} @click="navigator.clipboard.writeText(text)" class="btn bg-purple-700 text-white !w-auto text-xl flex items-center">
            {Heroicons.Solid.user_group(class: "h-6 w-6 mr-2")}
            {gettext("Copy Teams invitation")}
          </button>
          {#case @lesson_ag.status}
          {#match "started"}
            <button :on-click="call_the_roll" class="btn-primary !w-auto text-xl flex items-center">
              {Heroicons.Solid.user_add(class: "h-6 w-6 mr-2")}
              {gettext("Call the roll")}
            </button>
          {#match "roll_call"}
            <button :on-click="note_attendance" class="btn-primary !w-auto text-xl flex items-center">
              {Heroicons.Solid.stop(class: "h-6 w-6 mr-2")}
              {gettext("Note attendance")}
            </button>
          {/case}
        </:buttons>

        <:student :let={student: student}>
          <ClickrWeb.Lesson.StudentCard {=student} raised_hand?={MapSet.member?(@raised_hands, student.id)} />
        </:student>
      </ClickrWeb.Lesson.Layout>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    if connected?(socket), do: Lessons.subscribe(id)

    {:ok,
     socket
     |> assign(lesson_id: id)
     |> load_lesson_read_model()
     # handle page reload during question
     |> set_raised_hands_and_subscribe_if_status("roll_call")
     |> Redirect.assert_status_or_redirect(["started", "roll_call"]),
     temporary_assigns: [students: []]}
  end

  @impl true
  def handle_params(_params, _uri, socket) do
    {:noreply,
     socket
     |> assign_students()
     |> load_lesson_aggregate()}
  end

  @impl true
  def handle_event("call_the_roll", _params, socket) do
    result = Lessons.call_the_roll(socket.assigns.current_user, %{id: socket.assigns.lesson_id})
    subscribe_to_buttons(socket)
    Redirect.handle_dispatch_result(socket, "call the roll", result)
  end

  def handle_event("note_attendance", _params, socket) do
    result = Lessons.note_attendance(socket.assigns.current_user, %{id: socket.assigns.lesson_id})
    Redirect.handle_dispatch_result(socket, "note attendance", result)
  end

  @impl true
  def handle_info(%Buttons.Events.Clicked{} = e, %{assigns: %{status: "roll_call"}} = socket) do
    # Immediate: pubsub event
    student_id = socket.assigns.lesson_rm.button_mapping[e.id]
    raise_hand(socket, student_id)
  end

  def handle_info(%Events.RaisedHand{} = e, socket) do
    # Delayed fallback: domain event
    raise_hand(socket, e.student_id)
  end

  def handle_info(%Events.Deleted{}, socket) do
    {:noreply, push_redirect(socket, to: Routes.lessons_path(socket, :list))}
  end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp load_lesson_read_model(socket) do
    rm = Lessons.by_id!(socket.assigns.lesson_id)
    assign(socket, lesson_rm: rm, students: rm.students)
  end

  defp load_lesson_aggregate(socket) do
    lesson_ag = Lessons.aggregate_state(socket.assigns.lesson_id)
    assign(socket, lesson_ag: lesson_ag, status: lesson_ag.status)
  end
end
