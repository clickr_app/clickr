defmodule ClickrWeb.Lesson.List do
  use Surface.LiveView

  alias Surface.Components.{LiveRedirect, Form}
  alias Clickr.Lessons
  alias ClickrWeb.Components
  alias ClickrWeb.Router.Helpers, as: Routes
  alias ClickrWeb.Lesson.Redirect
  import Clickr.Gettext

  data page_title, :string, default: gettext("Lessons")
  data lessons, :list
  data count, :integer
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <h1 class="text-center">{gettext("Lessons")}</h1>
      <LiveRedirect to={Routes.lesson_path(@socket, :new)} class="mt-3 mx-auto btn-primary">
        {Heroicons.Solid.plus(class: "h-5 w-5 mr-2")}
        {gettext("New lesson")}
      </LiveRedirect>

      <Form for={:search} change="search" submit="search" class="mt-3">
        <Components.Form.SearchInput name="query" opts={placeholder: gettext("Search by class or room name"), 'phx-debounce': 100} />
      </Form>

      <Components.ContactCards class="mt-5">
        {#for lesson <- @lessons}
          <LiveRedirect to={Redirect.path(@socket, lesson)}>
            <Components.Card>
              <Components.ContactCard name={"#{lesson.class_name}/#{lesson.room_name} (#{Timex.format! lesson.inserted_at, "{D}.{M}."})"} tag={Lessons.gettext_status(lesson)} />
            </Components.Card>
          </LiveRedirect>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        {gettext("showing %{count} of %{total} matching", count: Enum.min([@limit, @count]), total: @count)}
      </div>
    </Components.Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> load_lessons()}
  end

  @impl true
  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, load_lessons(socket, query: query)}
  end

  defp load_lessons(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit, order_by: [desc: :updated_at]], opts)
    {lessons, count} = Lessons.list(opts)
    assign(socket, lessons: lessons, count: count)
  end
end
