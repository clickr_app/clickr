defmodule ClickrWeb.Lesson.LinearGrade do
  use Surface.Component
  alias ClickrWeb.Components.Form
  import Clickr.Gettext

  prop lesson_ag, :map, required: true
  prop opts, :map, required: true
  prop class, :css_class, default: []
  prop change, :event, required: true

  def render(assigns) do
    min = 0
    max = Enum.max([assigns.lesson_ag.questions | Map.values(assigns.lesson_ag.points)])

    ~F"""
    <Surface.Components.Form for={:opts} change={@change} {=@class}>
      <div class="flex flex-row gap-10 container-md">
        <Form.Field name={:min} label={"#{gettext("Min")}: #{@opts["min"]}"} class="flex-grow">
          <Form.RangeInput opts={step: 0.1, min: min, max: max, value: @opts["min"]} />
        </Form.Field>

        <Form.Field name={:max} label={"#{gettext("Max")}: #{@opts["max"]}"} class="flex-grow">
          <Form.RangeInput opts={step: 0.1, min: min, max: max, value: @opts["max"]} />
        </Form.Field>
      </div>
    </Surface.Components.Form>
    """
  end
end
