defmodule ClickrWeb.Lesson.Layout do
  use Surface.LiveComponent
  alias ClickrWeb.Components
  alias Clickr.Lessons
  require Logger
  import Clickr.Gettext

  prop current_user, :map, required: true
  prop lesson_rm, :map, required: true
  prop students, :list, required: true
  prop phx_update_student, :string, default: "replace"
  prop deletable?, :boolean, default: true
  slot default
  slot buttons
  slot student, required: true, args: [:student]

  @impl true
  def render(assigns) do
    ~F"""
    <div>
      <h1 class="text-center">
        {gettext("Lesson")} {@lesson_rm.class_name}/{@lesson_rm.room_name} ({Timex.format! @lesson_rm.inserted_at, "{D}.{M}."})

        <span class="mt-3 mx-auto px-2 py-0.5 text-green-800 text-2xl font-medium bg-green-100 rounded-full">
          {Lessons.gettext_status(@lesson_rm)}
        </span>
      </h1>

      <div :if={@deletable? || slot_assigned?(:buttons)} class="mt-5 flex gap-3 justify-center">
        <#slot name="buttons" />

        <Components.Delete :if={@deletable?} id={@lesson_rm.id} delete_fn={fn -> Lessons.delete(@current_user, id: @lesson_rm.id) end} class="text-xl" icon_class="h-6 w-6" />
      </div>

      <#slot />

      <div
        role="list"
        class="grid gap-1 mt-5"
        id="student-grid"
        phx-update={@phx_update_student}
        style={
          'grid-template-columns': "repeat(#{@lesson_rm.width}, minmax(0, 1fr));",
          'grid-template-rows': "repeat(#{@lesson_rm.height}, minmax(0, 1fr));"
        }
      >
        {#for %{x: x, y: y} = student <- @students}
          <div
            style={'grid-column': x, 'grid-row': y}
            class="flex flex-grow"
            id={"student-grid-#{student.id}"}
          >
            <#slot name="student" :args={student: student} />
          </div>
        {/for}
      </div>
    </div>
    """
  end
end
