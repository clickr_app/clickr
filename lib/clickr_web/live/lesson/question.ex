defmodule ClickrWeb.Lesson.Question do
  use Surface.LiveView
  require Logger
  alias Clickr.{Buttons, Grades, GradingIntervals, Lessons}
  alias Clickr.Lessons.Events
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Lesson.Redirect
  import ClickrWeb.Lesson.WithRaisedHands
  import Clickr.Gettext

  data page_title, :string, default: gettext("Lesson")
  data lesson_ag, :map
  data lesson_rm, :map
  data students, :list
  data raised_hands, :mapset, default: MapSet.new()
  data add_bonus_grade, :map
  data add_bonus_grade_percent, :number, default: 100.0

  alias ClickrWeb.Router.Helpers, as: Routes

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:none} extra_wide?={true}>
      <Components.Modal title="Add bonus grade" id="add_bonus_grade">
        <b>{gettext("Student")}:</b> {@add_bonus_grade.student_name}
        <Surface.Components.Form for={:opts} change="add_bonus_grade_change" submit="add_bonus_grade_commit">
          <Form.Field name={:percent} label={"#{gettext("Percent")}: #{@add_bonus_grade_percent} %"} class="flex-grow">
            <Form.RangeInput opts={step: 0.1, min: 0, max: 100, value: @add_bonus_grade_percent} />
          </Form.Field>
          <button type="submit" class="btn-primary w-full mt-5">{gettext("Add")}</button>
        </Surface.Components.Form>
      </Components.Modal>

      <ClickrWeb.Lesson.Layout {=@lesson_rm} {=@students} phx_update_student="append" id={@lesson_rm.id} {=@current_user}>
        <:buttons>
          {#case @lesson_ag.status}
          {#match "await_question"}
            <button :on-click="ask_question" class="btn-primary !w-auto text-xl flex items-center">
              {Heroicons.Solid.question_mark_circle(class: "h-6 w-6 mr-2")}
              {gettext("Ask question")}
            </button>
            <button :on-click="add_point_for_all" class="btn-primary !w-auto text-xl flex items-center">
              {Heroicons.Solid.plus_circle(class: "h-6 w-6 mr-2")}
              {gettext("Add point for all")}
            </button>
            <ClickrWeb.Class.SelectStudent :if={MapSet.size(@raised_hands) > 0} />
            <button :on-click="end" class="btn-primary !w-auto text-xl flex items-center">
              {Heroicons.Solid.stop(class: "h-6 w-6 mr-2")}
              {pgettext("imperative_verbs", "End")}
            </button>
          {#match "question"}
            <button :on-click="note_raised_hands" class="btn-primary !w-auto text-xl flex items-center">
              {Heroicons.Solid.stop(class: "h-6 w-6 mr-2")}
              {gettext("Note raised hands")}
            </button>
          {/case}
        </:buttons>

        {!-- TODO Optimize websocket diff: use heex or minify surface diff (unchanged class attributes) --}
        <:student :let={student: student}>
          <ClickrWeb.Lesson.StudentCard
            {=student}
            raised_hand?={MapSet.member?(@raised_hands, student.id)}
            attending?={MapSet.member?(@lesson_ag.attending, student.id)}
            opts={
              'data-raised-hand': MapSet.member?(@raised_hands, student.id) || nil
            }
          >
            <div class="text-center text-xl mt-1 test-points">
              {@lesson_ag.points[student.id]}
            </div>

            <:hover>
              <ClickrWeb.Lesson.AdjustPoints :if={MapSet.member?(@lesson_ag.attending, student.id)} student_id={student.id} />
              <ClickrWeb.Lesson.NoteLateArrival :if={!MapSet.member?(@lesson_ag.attending, student.id)} student_id={student.id} />
              <ClickrWeb.Lesson.NoteNotAttending :if={MapSet.member?(@lesson_ag.attending, student.id)} student_id={student.id} />
              <ClickrWeb.Lesson.AddBonusGrade :if={MapSet.member?(@lesson_ag.attending, student.id)} student_id={student.id} />
            </:hover>
          </ClickrWeb.Lesson.StudentCard>
        </:student>
      </ClickrWeb.Lesson.Layout>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    if connected?(socket), do: Lessons.subscribe(id)

    {:ok,
     socket
     |> assign(lesson_id: id)
     |> load_lesson_read_model()
     # handle page reload during question
     |> set_raised_hands_and_subscribe_if_status("question")
     |> Redirect.assert_status_or_redirect(["await_question", "question"]),
     temporary_assigns: [students: []]}
  end

  @impl true
  def handle_params(_params, _uri, socket) do
    {:noreply,
     socket
     |> load_lesson_aggregate()
     |> assign_students()}
  end

  @impl true
  def handle_info(%Buttons.Events.Clicked{} = e, %{assigns: %{status: "question"}} = socket) do
    # Immediate: pubsub event
    student_id = socket.assigns.lesson_rm.button_mapping[e.id]
    raise_hand(socket, student_id, check_attending?: true)
  end

  def handle_info(%Events.RaisedHand{} = e, socket) do
    # Delayed fallback: domain event
    raise_hand(socket, e.student_id, check_attending?: true)
  end

  def handle_info(%Events.Deleted{}, socket) do
    {:noreply, push_redirect(socket, to: Routes.lessons_path(socket, :list))}
  end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  @impl true
  def handle_event("adjust_points", %{"student-id" => student_id} = data, socket) do
    result = ClickrWeb.Lesson.AdjustPoints.handle_event(data, socket)
    update_student(socket, "adjust points", student_id, result)
  end

  @impl true
  def handle_event("add_point_for_all", _data, socket) do
    result =
      Lessons.add_point_for_all(socket.assigns.current_user, %{
        id: socket.assigns.lesson_id,
        delta: 1
      })

    Redirect.handle_dispatch_result(socket, "add point for all", result)
  end

  def handle_event("note_late_arrival", %{"student-id" => student_id} = data, socket) do
    result = ClickrWeb.Lesson.NoteLateArrival.handle_event(data, socket)
    update_student(socket, "note late arrival", student_id, result)
  end

  def handle_event("note_not_attending", %{"student-id" => student_id} = data, socket) do
    result = ClickrWeb.Lesson.NoteNotAttending.handle_event(data, socket)
    update_student(socket, "note late arrival", student_id, result)
  end

  def handle_event("ask_question", _params, socket) do
    result = Lessons.ask_question(socket.assigns.current_user, %{id: socket.assigns.lesson_id})
    subscribe_to_buttons(socket)

    socket
    |> assign(raised_hands: MapSet.new())
    |> Redirect.handle_dispatch_result("ask question", result)
  end

  def handle_event("note_raised_hands", _params, socket) do
    unsubscribe_from_buttons(socket)

    result =
      Lessons.note_raised_hands(socket.assigns.current_user, %{id: socket.assigns.lesson_id})

    Redirect.handle_dispatch_result(socket, "note raised hands", result)
  end

  def handle_event("end", _params, socket) do
    result = Lessons.end(socket.assigns.current_user, %{id: socket.assigns.lesson_id})
    Redirect.handle_dispatch_result(socket, "end lesson", result)
  end

  def handle_event("add_bonus_grade", %{"student-id" => student_id}, socket) do
    Components.Modal.show("add_bonus_grade")

    student_name =
      Enum.find(socket.assigns.lesson_rm.students, fn %{id: id} -> id == student_id end).name

    data = %{student_id: student_id, student_name: student_name}

    {:noreply,
     socket
     |> assign(add_bonus_grade: data)}
  end

  def handle_event("add_bonus_grade_change", %{"opts" => %{"percent" => percent_string}}, socket) do
    {percent, _} = Float.parse(percent_string)
    {:noreply, assign(socket, add_bonus_grade_percent: percent)}
  end

  def handle_event("add_bonus_grade_commit", %{"opts" => %{"percent" => percent_string}}, socket) do
    {percent, _} = Float.parse(percent_string)

    grade_id =
      Grades.id(%{
        grading_interval_id: GradingIntervals.default_id(),
        student_id: socket.assigns.add_bonus_grade.student_id
      })

    Grades.add_bonus_grade(socket.assigns.current_user, %{id: grade_id, percent: percent})
    Components.Modal.hide("add_bonus_grade")
    {:noreply, socket}
  end

  defp load_lesson_read_model(socket) do
    assign(socket, lesson_rm: Lessons.by_id!(socket.assigns.lesson_id))
  end

  defp load_lesson_aggregate(socket) do
    lesson_ag = Lessons.aggregate_state(socket.assigns.lesson_id)
    assign(socket, lesson_ag: lesson_ag, status: lesson_ag.status)
  end

  defp update_student(socket, text, student_id, result) do
    case result do
      {:ok, _} ->
        {:noreply,
         socket
         |> assign_student(student_id)
         |> load_lesson_aggregate()}

      e ->
        Logger.error("Failed to #{text}: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to %{text}", text: text))}
    end
  end
end
