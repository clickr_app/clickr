defmodule ClickrWeb.Lesson.AdjustPoints do
  use Surface.Component
  alias Clickr.Lessons
  alias Clickr.Lessons.Commands

  prop student_id, :string, required: true

  def render(assigns) do
    ~F"""
    <button
      :on-click="adjust_points"
      :values={'student-id': @student_id, delta: "-1"}
      class="h-full flex-grow flex justify-center items-center hover:bg-primary-100 hover:bg-opacity-50"
    >
      {Heroicons.Solid.minus_circle(class: "h-6 w-6")}
    </button>
    <button
      :on-click="adjust_points"
      :values={'student-id': @student_id, delta: "1"}
      class="h-full flex-grow flex justify-center items-center hover:bg-primary-100 hover:bg-opacity-50"
    >
      {Heroicons.Solid.plus_circle(class: "h-6 w-6")}
    </button>
    """
  end

  def handle_event(%{"student-id" => student_id, "delta" => delta}, socket) do
    cmd = %Commands.AdjustPoints{
      id: socket.assigns.lesson_id,
      student_id: student_id,
      delta: String.to_integer(delta)
    }

    Lessons.adjust_points(socket.assigns.current_user, cmd)
  end
end
