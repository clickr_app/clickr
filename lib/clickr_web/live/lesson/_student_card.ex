defmodule ClickrWeb.Lesson.StudentCard do
  use Surface.Component

  prop student, :map, required: true
  prop attending?, :boolean, default: true
  prop raised_hand?, :boolean, default: false
  prop class, :css_class, default: []
  prop opts, :keyword, default: []

  slot default
  slot hover

  def render(assigns) do
    ~F"""
    <div
      class={[
        "bg-white py-8 px-0 sm:rounded-lg relative overflow-hidden overflow-ellipsis whitespace-nowrap h-20 w-full flex flex-col justify-center border border-black-300 relative",
        'bg-gray-100 text-gray-500': !@attending?,
        'bg-green-200': @raised_hand?,
      ] ++ @class}

      x-data="{ hover: false }"
      @mouseenter="hover = true"
      @mouseleave="hover = false"

      {...@opts}
    >
      <div class="text-2xl font-bold text-center">{@student.name}</div>

      <#slot name="default" />

      <div :if={slot_assigned?(:hover)} x-cloak x-show="hover" class="absolute top-0 left-0 w-full h-full bg-white bg-opacity-70 flex divide-x divide-solid text-gray-700">
        <#slot name="hover" />
      </div>
    </div>
    """
  end
end
