defmodule ClickrWeb.Lesson.WithRaisedHands do
  @moduledoc """
  Performance optimization: "RaisedHand" events take ~0.1s to arrive, in sequential order -> 10 events = 1 second
  "Clicked" events arrive straight away, as they are not processed by the aggregate.
  Bottom line: Mimic/duplicate the aggregate behaviour to achieve faster visual feedback.
  """
  require Logger
  alias Clickr.{Buttons, Lessons}
  import Phoenix.LiveView

  def subscribe_to_buttons(socket) do
    for button_id <- Map.keys(socket.assigns.lesson_rm.button_mapping) do
      Buttons.subscribe(button_id)
      Buttons.subscribe_only_for_user(socket.assigns.current_user.id, button_id)
    end
  end

  def unsubscribe_from_buttons(socket) do
    socket.assigns.lesson_rm.button_mapping
    |> Map.keys()
    |> Enum.each(&Buttons.unsubscribe/1)
  end

  def set_raised_hands_and_subscribe_if_status(socket, status) do
    if socket.assigns.lesson_rm.status == status do
      subscribe_to_buttons(socket)
      assign(socket, raised_hands: Lessons.aggregate_state(socket.assigns.lesson_id).raised_hands)
    else
      socket
    end
  end

  def raise_hand(socket, student_id, opts \\ []) do
    check_attending? = opts[:check_attending?] || false

    if !check_attending? || MapSet.member?(socket.assigns.lesson_ag.attending, student_id) do
      {:noreply,
       socket
       |> assign_student(student_id)
       |> update(:raised_hands, &MapSet.put(&1, student_id))}
    else
      {:noreply, socket}
    end
  end

  def assign_student(socket, student_id) do
    student = Enum.find(socket.assigns.lesson_rm.students, &(&1.id == student_id))
    assign(socket, students: [student])
  end

  def assign_students(socket) do
    assign(socket, students: socket.assigns.lesson_rm.students)
  end
end
