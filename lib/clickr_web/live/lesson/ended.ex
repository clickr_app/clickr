defmodule ClickrWeb.Lesson.Ended do
  use Surface.LiveView
  require Logger

  alias Clickr.Lessons
  alias Clickr.Lessons.{Events, GradingMethods}
  alias ClickrWeb.Components
  alias ClickrWeb.Lesson.Redirect
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Lesson")
  data lesson_ag, :map
  data lesson_rm, :map
  data students, :list
  data grades, :map, default: %{}
  data method, :string, default: "linear"
  data opts, :map

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:none} extra_wide?={true}>
      <ClickrWeb.Lesson.Layout {=@lesson_rm} {=@students} id={@lesson_rm.id} {=@current_user}>
        <:buttons>
          <button :on-click="grade" class="btn-primary !w-auto text-xl flex items-center">
            {Heroicons.Solid.calculator(class: "h-8 w-8 mr-2")}
            {pgettext("imperative_verbs", "Grade")}
          </button>
        </:buttons>

        <ClickrWeb.Lesson.LinearGrade {=@lesson_ag} {=@opts} change="change_opts" class="mt-5" />
        <Components.GradesDistributionChart grades={Map.values(@grades)} width={900} height={400} class="mt-5" />

        <:student :let={student: student}>
          <ClickrWeb.Lesson.StudentCard
            {=student}
            attending?={MapSet.member?(@lesson_ag.attending, student.id)}
          >
            <div :if={MapSet.member?(@lesson_ag.attending, student.id)} class="flex justify-center gap-5 text-xl mt-1">
              <span>{@lesson_ag.points[student.id]}</span>
              <span>{Float.round(@grades[student.id] || 0.0, 1)} %</span>
            </div>

            <:hover>
              <ClickrWeb.Lesson.AdjustPoints :if={MapSet.member?(@lesson_ag.attending, student.id)} student_id={student.id} />
              <ClickrWeb.Lesson.NoteLateArrival :if={!MapSet.member?(@lesson_ag.attending, student.id)} student_id={student.id} />
              <ClickrWeb.Lesson.NoteNotAttending :if={MapSet.member?(@lesson_ag.attending, student.id)} student_id={student.id} />
            </:hover>
          </ClickrWeb.Lesson.StudentCard>
        </:student>
      </ClickrWeb.Lesson.Layout>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    if connected?(socket), do: Lessons.subscribe(id)

    {:ok,
     socket
     |> assign(lesson_id: id)
     |> load()
     |> set_initial_opts()
     |> calculate_grades()
     |> ClickrWeb.Lesson.Redirect.assert_status_or_redirect()}
  end

  @impl true
  def handle_event("grade", _params, socket) do
    %{lesson_id: id, method: method, opts: opts} = socket.assigns
    result = Lessons.grade(socket.assigns.current_user, %{id: id, method: method, opts: opts})
    Redirect.handle_dispatch_result(socket, "grade", result)
  end

  @impl true
  def handle_event("change_opts", %{"opts" => opts}, socket) do
    opts = GradingMethods.parse_opts(socket.assigns.method, opts)

    {:noreply,
     socket
     |> assign(opts: opts)
     |> calculate_grades()}
  end

  def handle_event("adjust_points", data, socket) do
    result = ClickrWeb.Lesson.AdjustPoints.handle_event(data, socket)
    reload_on_successful_dispatch(socket, "adjust points", result)
  end

  def handle_event("note_late_arrival", data, socket) do
    result = ClickrWeb.Lesson.NoteLateArrival.handle_event(data, socket)
    reload_on_successful_dispatch(socket, "note late arrival", result)
  end

  def handle_event("note_not_attending", data, socket) do
    result = ClickrWeb.Lesson.NoteNotAttending.handle_event(data, socket)
    reload_on_successful_dispatch(socket, "note not attending", result)
  end

  @impl true
  def handle_info(%Events.Deleted{}, socket) do
    {:noreply, push_redirect(socket, to: Routes.lessons_path(socket, :list))}
  end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp load(socket) do
    socket
    |> load_lesson_read_model()
    |> load_lesson_aggregate()
  end

  defp load_lesson_read_model(socket) do
    rm = Lessons.by_id!(socket.assigns.lesson_id)
    assign(socket, lesson_rm: rm, students: rm.students)
  end

  defp load_lesson_aggregate(socket) do
    assign(socket, lesson_ag: Lessons.aggregate_state(socket.assigns.lesson_id))
  end

  defp set_initial_opts(socket) do
    opts = Lessons.GradingMethods.initial_opts(socket.assigns.method, socket.assigns.lesson_ag)
    assign(socket, opts: opts)
  end

  defp calculate_grades(socket) do
    %{lesson_ag: lesson_ag, method: method, opts: opts} = socket.assigns
    grades = Lessons.Aggregates.Lesson.grade(lesson_ag, method, opts)
    assign(socket, grades: grades)
  end

  def reload_on_successful_dispatch(socket, text, result) do
    case result do
      {:ok, _lesson} ->
        {:noreply, socket |> load() |> calculate_grades()}

      e ->
        Logger.error("Failed to #{text}: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to %{text}", text: text))}
    end
  end
end
