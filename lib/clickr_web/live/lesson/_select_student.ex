defmodule ClickrWeb.Class.SelectStudent do
  use Surface.Component
  import Clickr.Gettext

  def render(assigns) do
    ~F"""
      <button
        class="btn btn-secondary text-xl"
        x-data="{
          select() {
            const students = document.querySelectorAll('[data-raised-hand]')
            const selectedClass = '!bg-primary-500'

            students.forEach(el => { el.classList.remove(selectedClass) })
            const selected = this.random(students)
            selected.classList.add(selectedClass)
          },

          random(items) {
            const index = Math.floor(Math.random() * items.length)
            return items[index]
          },
        }"
        @click="select"
      >
        {Heroicons.Solid.search(class: "h-6 w-6 mr-2")}
        {gettext("Select student")}
      </button>
    """
  end
end
