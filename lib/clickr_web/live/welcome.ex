defmodule ClickrWeb.Welcome do
  use Surface.LiveView
  alias ClickrWeb.Router.Helpers, as: Routes

  @impl true
  def render(assigns) do
    ~F"""
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok, push_redirect(socket, to: Routes.lessons_path(socket, :list))}
  end
end
