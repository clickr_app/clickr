defmodule ClickrWeb.User.Invite do
  use Surface.LiveView
  alias Clickr.Accounts
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Invite user")

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash}>
      <Components.ActionCard title={gettext("Invite")}>
        <Surface.Components.Form for={@changeset} submit="invite" class="space-y-6">
          <ClickrWeb.Components.Alerts.Error :if={@changeset.action} text={gettext("Oops, something went wrong! Please check the errors below.")} />

          <Form.Field name={:email} label={gettext("Email address")} :let={class: class, error?: error?}>
            <Form.EmailInput opts={required: true} {=class} {=error?} />
          </Form.Field>

          <ClickrWeb.Components.Button class="mt-5 mx-auto" label={gettext("Invite")} opts={type: "submit"} />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> assign_initial_changeset()}
  end

  @impl true
  def handle_event("invite", %{"user" => user_params}, socket) do
    case Accounts.invite_user(user_params) do
      {:ok, user} ->
        {:ok, _} =
          Accounts.deliver_user_reset_password_instructions(
            user,
            &Routes.user_reset_password_url(socket, :edit, &1)
          )

        {:noreply,
         socket
         |> put_flash(:info, gettext("User invited successfully."))
         |> assign_initial_changeset()}

      {:error, %Ecto.Changeset{} = changeset} ->
        {:noreply, assign(socket, changeset: changeset)}
    end
  end

  defp assign_initial_changeset(socket) do
    assign(socket, changeset: Accounts.change_user_invitation(%Accounts.User{}))
  end
end
