defmodule ClickrWeb.User.ApiToken do
  use Surface.LiveView
  alias ClickrWeb.Components
  alias Clickr.Accounts
  import Clickr.Gettext

  data page_title, :string, default: gettext("API token")
  data api_token, :string, default: nil

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash}>
      <Components.ActionCard title={gettext("API token")}>
        <button class="btn-primary mx-auto" :on-click="generate">{gettext("Generate")}</button>

        <div :if={@api_token} class="mt-5 rounded-md bg-gray-100 p-5 text-center text-xs text-mono">
          {@api_token}
        </div>
      </Components.ActionCard>
    </Components.Layout>
    """
  end

  @impl true
  def handle_event("generate", _params, socket) do
    api_token = Accounts.create_api_token(socket.assigns.current_user)
    {:noreply, assign(socket, api_token: api_token)}
  end
end
