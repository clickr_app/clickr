defmodule ClickrWeb.Class.Show do
  use Surface.LiveView
  alias Surface.Components.LiveRedirect
  alias Clickr.{Classes, Students}
  alias Clickr.Classes.Events
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Router.Helpers, as: Routes
  require Logger
  import Clickr.Gettext

  data page_title, :string, default: gettext("Class")
  data class, :map
  data students, :list
  data grades, :list
  data query, :string, default: ""
  data rename?, :boolean, default: false

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <Components.Rename id={@class_id} value={@class.name} display_value={"#{gettext("Class")} #{@class.name}"} rename_fn={fn name -> Classes.rename(@current_user, id: @class_id, name: name) end} />

      <div class="mt-3 flex gap-3 justify-center">
        <Components.Delete id={@class_id} delete_fn={fn -> Classes.delete(@current_user, id: @class_id) end} />
      </div>

      <Components.GradesDistributionChart :if={@query == ""} {=@grades} width={900} height={400} class="mt-5" />

      <h2 class="text-center mt-3">{gettext("Students")}</h2>
      <LiveRedirect to={Routes.student_path(@socket, :new, class_id: @class.id)} class="mt-3 mx-auto btn-primary">
        {Heroicons.Solid.plus(class: "h-5 w-5 mr-2")}
        {gettext("New student")}
      </LiveRedirect>

      <Surface.Components.Form for={:search_student} change="search_student" submit="search_student" class="mt-3">
        <Form.SearchInput name="query" opts={placeholder: gettext("Search by student name"), 'phx-debounce': 100} />
      </Surface.Components.Form>

      <Components.ContactCards class="mt-5">
        {#for student <- @students}
          <LiveRedirect to={Routes.student_path(@socket, :show, student)}>
            <Components.Card>
              <Components.ContactCard name={student.name} tag={"#{Float.round(student.current_grade || 0, 1)} %"} />
            </Components.Card>
          </LiveRedirect>
        {/for}
      </Components.ContactCards>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    if connected?(socket), do: Classes.subscribe(id)

    {:ok,
     socket
     |> assign(class_id: id)
     |> load_class()
     |> load_students()}
  end

  @impl true
  def handle_event("search_student", %{"query" => query}, socket) do
    {:noreply,
     socket
     |> assign(query: query)
     |> load_students(query: query)}
  end

  @impl true
  def handle_info(%Events.Renamed{}, socket) do
    {:noreply, load_class(socket)}
  end

  def handle_info(%Events.Deleted{}, socket) do
    {:noreply, push_redirect(socket, to: Routes.classes_path(socket, :list))}
  end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp load_class(socket) do
    assign(socket, class: Classes.by_id!(socket.assigns.class_id))
  end

  defp load_students(socket, opts \\ []) do
    opts = Keyword.merge([where: [class_id: socket.assigns.class_id], order_by: :name], opts)
    {students, _count} = Students.list(opts)
    grades = Enum.map(students, & &1.current_grade)

    assign(socket, students: students, grades: grades)
  end
end
