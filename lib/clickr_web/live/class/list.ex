defmodule ClickrWeb.Class.List do
  use Surface.LiveView
  alias Surface.Components.{LiveRedirect, Form}
  alias Clickr.Classes
  alias ClickrWeb.Components
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Classes")
  data classes, :list
  data count, :integer
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <h1 class="text-center">{gettext("Classes")}</h1>
      <LiveRedirect to={Routes.class_path(@socket, :new)} class="mt-3 mx-auto btn-primary">
        {Heroicons.Solid.plus(class: "h-5 w-5 mr-2")}
        {gettext("New class")}
      </LiveRedirect>

      <Form for={:search} change="search" submit="search" class="mt-3">
        <Components.Form.SearchInput name="query" opts={placeholder: gettext("Search by class name"), 'phx-debounce': 100} />
      </Form>

      <Components.ContactCards class="mt-5">
        {#for class <- @classes}
          <LiveRedirect to={Routes.class_path(@socket, :show, class)}>
            <Components.Card>
              <Components.ContactCard name={class.name} />
            </Components.Card>
          </LiveRedirect>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        {gettext("showing %{count} of %{total} matching", count: Enum.min([@limit, @count]), total: @count)}
      </div>
    </Components.Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> load_classes()}
  end

  @impl true
  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, load_classes(socket, query: query)}
  end

  defp load_classes(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit], opts)
    {classes, count} = Classes.list(opts)
    assign(socket, classes: classes, count: count)
  end
end
