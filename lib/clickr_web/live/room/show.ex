defmodule ClickrWeb.Room.Show do
  use Surface.LiveView
  require Logger

  alias Clickr.{Buttons, Rooms}
  alias Clickr.Rooms.Events
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Room")
  data room_rm, :map
  data room_ag, :map
  data buttons, :list, default: []
  data students, :list
  data state, :atom, default: :default
  data coordinates, :map, default: nil
  data clicked_button, :string, default: nil

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:none}>
      <Components.Rename id={@room_id} value={@room_rm.name} display_value={"#{gettext("Room")} #{@room_rm.name}"} rename_fn={fn name -> Rooms.rename(@current_user, id: @room_id, name: name) end} />

      <div class="mt-3 flex gap-3 justify-center">
        <Components.Delete id={@room_id} delete_fn={fn -> Rooms.delete(@current_user, id: @room_id) end} />
      </div>

      <div class="mt-3 container-md">
        <Surface.Components.Form for={:room} change="change_size" submit="change_size" class="space-y-6">
          <div class="flex flex-row gap-10">
            <Form.Field name={:width} label={"#{gettext("Width")}: #{@room_rm.width}"} class="flex-grow">
              <Form.RangeInput opts={min: 1, max: 10, value: @room_rm.width, 'phx-debounce': 100} />
            </Form.Field>

            <Form.Field name={:height} label={"#{gettext("Height")}: #{@room_rm.height}"} class="flex-grow">
              <Form.RangeInput opts={min: 1, max: 10, value: @room_rm.height, 'phx-debounce': 100} />
            </Form.Field>
          </div>
        </Surface.Components.Form>
      </div>

      <div class="mt-3">
        <h2 class="text-center">{gettext("Buttons")}</h2>
        <Components.GridWithDragAndDrop id="room_grid" dragged="button_dragged" width={@room_rm.width} height={@room_rm.height} class="mt-5" data={@buttons}>
          <:default :let={item: button}>
            <Components.Card
              border?={true}
              class={
                "relative overflow-hidden overflow-ellipsis whitespace-nowrap h-20 w-full flex flex-col justify-center !px-0",
                'bg-primary-200': @clicked_button == button.id
              }
            >
              <div class="text-2-xl font-bold text-center">{button.name}</div>
              <div class="text-center">{button.device_name}</div>
              <button :on-click="remove_button" :values={'button-id': button.id} class="absolute top-0 right-0 p-1" title={gettext("Remove from room")}>
                {Heroicons.Solid.x(class: "h-5 w-5")}
              </button>
            </Components.Card>
          </:default>

          <:empty :let={x: x, y: y}>
            <div class={
              "flex flex-grow justify-center items-center h-20 border",
              'text-gray-200': @state == :position_button,
              'text-gray-500': @state != :position_button,
              'bg-yellow-300': @coordinates == %{x: x, y: y}
            }>
              <button
                @click="document.querySelector('#keyboard-device').focus()"
                :on-click="start_position_button"
                :values={x: x, y: y}
                disabled={@state == :position_button}
              >
                {Heroicons.Solid.plus_circle(class: "h-12 w-12")}
              </button>
            </div>
          </:empty>
        </Components.GridWithDragAndDrop>
      </div>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    if connected?(socket) do
      Buttons.subscribe_all()
      Buttons.subscribe_all_only_for_user(socket.assigns.current_user.id)
      Rooms.subscribe(id)
    end

    {:ok,
     socket
     |> assign(room_id: id)
     |> load_room()}
  end

  @impl true
  def handle_event("change_size", %{"room" => params}, socket) do
    params =
      params
      |> Map.put(:id, socket.assigns.room_id)
      |> Map.update!("width", &String.to_integer/1)
      |> Map.update!("height", &String.to_integer/1)

    case Rooms.change_size(socket.assigns.current_user, params) do
      {:ok, _} ->
        {:noreply, load_room(socket)}

      e ->
        Logger.error("Falied to change size: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to change size"))}
    end
  end

  def handle_event("button_dragged", %{"from" => %{"id" => from_id}, "to" => to}, socket) do
    to = parse_coordinates(to)

    cmd = %Rooms.Commands.PositionButton{
      id: socket.assigns.room_id,
      button_id: from_id,
      x: to.x,
      y: to.y
    }

    case Rooms.position_button(socket.assigns.current_user, cmd) do
      {:ok, _} ->
        {:noreply, load_room(socket)}

      e ->
        Logger.error("Failed to move button: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to move button"))}
    end
  end

  def handle_event("start_position_button", %{} = coordinates, socket) do
    coordinates = parse_coordinates(coordinates)
    {:noreply, assign(socket, state: :position_button, coordinates: coordinates)}
  end

  def handle_event("remove_button", %{"button-id" => button_id}, socket) do
    cmd = %Rooms.Commands.RemoveButton{
      id: socket.assigns.room_id,
      button_id: button_id
    }

    case Rooms.remove_button(socket.assigns.current_user, cmd) do
      {:ok, _} -> {:noreply, load_room(socket)}
      _ -> {:noreply, put_flash(socket, :error, gettext("Failed to remove button"))}
    end
  end

  @impl true
  def handle_info(
        %Buttons.Events.Clicked{} = event,
        %{assigns: %{state: :position_button}} = socket
      ) do
    %{room_id: room_id, coordinates: coordinates} = socket.assigns

    cmd = %Rooms.Commands.PositionButton{
      id: room_id,
      button_id: event.id,
      x: coordinates.x,
      y: coordinates.y
    }

    with {:ok, _} <- Buttons.register(socket.assigns.current_user, event),
         {:ok, _} <- Rooms.position_button(socket.assigns.current_user, cmd) do
      {:noreply, socket |> load_room() |> assign(state: :default, coordinates: nil)}
    else
      e ->
        Logger.error("Failed to add button: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to add button"))}
    end
  end

  def handle_info(%Buttons.Events.Clicked{} = click, socket) do
    Process.send_after(self(), "unset_clicked_button", 100)
    {:noreply, assign(socket, clicked_button: click.id)}
  end

  def handle_info(%Buttons.Events.Registered{}, socket) do
    {:noreply, socket}
  end

  def handle_info("unset_clicked_button", socket) do
    {:noreply, assign(socket, clicked_button: nil)}
  end

  def handle_info(%Events.Renamed{}, socket) do
    {:noreply, load_room(socket)}
  end

  def handle_info(%Events.Deleted{}, socket) do
    {:noreply, push_redirect(socket, to: Routes.rooms_path(socket, :list))}
  end

  def handle_info(_, socket) do
    {:noreply, socket}
  end

  defp load_room(socket) do
    socket
    |> load_room_read_model()
    |> load_room_aggregate()
  end

  defp load_room_read_model(socket) do
    room_rm = Rooms.by_id!(socket.assigns.room_id)

    socket
    |> assign(room_rm: room_rm)
    |> assign(buttons: room_rm.buttons)
  end

  defp load_room_aggregate(socket) do
    socket
    |> assign(room_ag: Rooms.aggregate_state(socket.assigns.room_id))
  end

  defp parse_coordinates(%{"x" => x, "y" => y}),
    do: %{x: String.to_integer(x), y: String.to_integer(y)}
end
