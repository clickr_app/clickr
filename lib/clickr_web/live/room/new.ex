defmodule ClickrWeb.Room.New do
  use Surface.LiveView

  alias Clickr.Rooms
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("New room")
  data errors, :list, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <Components.ActionCard title={gettext("New room")}>
        <Surface.Components.Form for={:room} errors={@errors} submit="save">
          <Components.Alerts.Error :if={not Enum.empty?(@errors)} text={gettext("An error occured. See details below.")} class="mb-3" />

          <Form.Field name={:name}>
            <Form.TextInput opts={required: true, 'x-trap': "true"} />
          </Form.Field>

          <Components.Button class="mt-5 mx-auto" label={gettext("Create")} opts={'phx-disable-with': gettext("Creating...")} />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.Layout>
    """
  end

  @impl true
  def handle_event("save", %{"room" => params}, socket) do
    params =
      params
      |> Map.put(:width, 8)
      |> Map.put(:height, 6)

    case Rooms.create(socket.assigns.current_user, params) do
      {:ok, %{id: id}} ->
        {:noreply,
         socket
         |> put_flash(:info, gettext("Room created"))
         |> push_redirect(to: Routes.room_path(socket, :show, id, replace: true))}

      {:error, :validation_failure, errors} ->
        {:noreply, assign(socket, errors: errors)}

      _ ->
        {:noreply, put_flash(socket, :error, gettext("Could not create room"))}
    end
  end
end
