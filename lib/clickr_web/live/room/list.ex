defmodule ClickrWeb.Room.List do
  use Surface.LiveView
  alias Surface.Components.{LiveRedirect, Form}
  alias Clickr.Rooms
  alias ClickrWeb.Components
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Rooms")
  data rooms, :list
  data count, :integer
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <h1 class="text-center">{gettext("Rooms")}</h1>
      <LiveRedirect to={Routes.room_path(@socket, :new)} class="mt-3 mx-auto btn-primary">
        {Heroicons.Solid.plus(class: "h-5 w-5 mr-2")}
        {gettext("New room")}
      </LiveRedirect>

      <Form for={:search} change="search" submit="search" class="mt-3">
        <Components.Form.SearchInput name="query" opts={placeholder: gettext("Search by room name"), 'phx-debounce': 100, 'x-trap': "true"} />
      </Form>

      <Components.ContactCards class="mt-5">
        {#for room <- @rooms}
          <LiveRedirect to={Routes.room_path(@socket, :show, room)}>
            <Components.Card>
              <Components.ContactCard name={room.name} />
            </Components.Card>
          </LiveRedirect>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        {gettext("showing %{count} of %{total} matching", count: Enum.min([@limit, @count]), total: @count)}
      </div>
    </Components.Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> load_rooms()}
  end

  @impl true
  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, load_rooms(socket, query: query)}
  end

  defp load_rooms(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit], opts)
    {rooms, count} = Rooms.list(opts)
    assign(socket, rooms: rooms, count: count)
  end
end
