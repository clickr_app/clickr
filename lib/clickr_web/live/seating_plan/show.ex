defmodule ClickrWeb.SeatingPlan.Show do
  use Surface.LiveView
  require Logger
  alias Clickr.{Buttons, Rooms, SeatingPlans}
  alias ClickrWeb.Components
  import Clickr.Gettext

  data page_title, :string, default: gettext("Seating plan")
  data seating_plan_rm, :map
  data seating_plan_ag, :map
  data room_rm, :map
  data button_mapping, :map, default: %{}
  data positioned_students, :list, default: []
  data unpositioned_students, :list, default: []
  data clicked_student, :string, default: nil

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:none}>
      <h1 class="text-center">{gettext("Seating plan")} {@seating_plan_rm.class_name}/{@seating_plan_rm.room_name}</h1>

      <Components.GridWithDragAndDrop id="seating_plan_grid" dragged="dragged" width={@room_rm.width} height={@room_rm.height} class="mt-5" data={@positioned_students}>
        <:default :let={item: student}>
          <Components.Card
            border?={true}
            class={
              "relative overflow-hidden overflow-ellipsis whitespace-nowrap h-20 w-full flex flex-col justify-center !px-0",
              'bg-primary-200': @clicked_student == student.id
            }
          >
            <div class="text-2-xl font-bold text-center">{student.name}</div>
            <button :on-click="unposition_student" :values={'student-id': student.id} class="absolute top-0 right-0 p-1" title={gettext("Remove from seating plan")}>
              {Heroicons.Solid.x(class: "h-5 w-5")}
            </button>
          </Components.Card>
        </:default>
        <:empty>
          <div class="w-full h-20 flex items-center justify-center border">

          </div>
        </:empty>
      </Components.GridWithDragAndDrop>

      <div :if={Enum.any? @unpositioned_students} class="mt-5">
        <h2 class="text-center">{gettext("Students without a seat")}</h2>
        <div class="mt-3 flex flex-row flex-wrap justify-center gap-1">
          {#for student <- @unpositioned_students}
            <Components.Card
              border?={true}
              opts={
                id: "unpositioned-student-#{student.id}",
                'x-data': "{}",
                draggable: "true",
                'data-id': student.id,
                '@dragstart.self': "
                  document.getElementById('seating_plan_grid').dispatchEvent(new CustomEvent('start-dragging'))
                  event.dataTransfer.effectAllowed = 'move'
                  event.dataTransfer.setData('text/plain', event.target.id)
                ",
                '@dragend.self': "document.getElementById('seating_plan_grid').dispatchEvent(new CustomEvent('stop-dragging'))"
              }
              class="cursor-move overflow-hidden overflow-ellipsis whitespace-nowrap w-40 h-20 flex flex-col justify-center !px-0"
            >
              <div class="text-2-xl font-bold text-center">{student.name}</div>
            </Components.Card>
          {/for}
        </div>
      </div>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    socket =
      socket
      |> assign(seating_plan_id: id)
      |> load()

    if connected?(socket) do
      for button <- socket.assigns.room_rm.buttons do
        Buttons.subscribe(button.id)
        Buttons.subscribe_only_for_user(socket.assigns.current_user.id, button.id)
      end
    end

    {:ok, socket}
  end

  @impl true
  def handle_event("dragged", %{"from" => %{"id" => from_id}, "to" => to}, socket) do
    to = parse_coordinates(to)

    cmd = %SeatingPlans.Commands.PositionStudent{
      id: socket.assigns.seating_plan_id,
      student_id: from_id,
      x: to.x,
      y: to.y
    }

    case SeatingPlans.position_student(socket.assigns.current_user, cmd) do
      {:ok, _} ->
        {:noreply, load(socket)}

      e ->
        Logger.error("Failed to position student: #{inspect(e)}")
        {:noreply, put_flash(socket, :error, gettext("Failed to position student"))}
    end
  end

  def handle_event("unposition_student", %{"student-id" => student_id}, socket) do
    cmd = %SeatingPlans.Commands.UnpositionStudent{
      id: socket.assigns.seating_plan_id,
      student_id: student_id
    }

    case SeatingPlans.unposition_student(socket.assigns.current_user, cmd) do
      {:ok, _} ->
        {:noreply, load(socket)}

      e ->
        Logger.error("Failed to unposition student: #{inspect(e)}")

        {:noreply,
         put_flash(socket, :error, gettext("Failed to remove student from seating plan"))}
    end
  end

  @impl true
  def handle_info(%Buttons.Events.Clicked{} = click, socket) do
    clicked_student = socket.assigns.button_mapping[click.id]
    Process.send_after(self(), "unset_clicked_student", 100)
    {:noreply, assign(socket, clicked_student: clicked_student)}
  end

  def handle_info("unset_clicked_student", socket) do
    {:noreply, assign(socket, clicked_student: nil)}
  end

  defp load(socket) do
    socket
    |> load_seating_plan_read_model()
    |> load_seating_plan_aggregate()
    |> load_room_read_model()
    |> calculate_button_mapping()
  end

  defp load_seating_plan_read_model(socket) do
    rm = SeatingPlans.by_id!(socket.assigns.seating_plan_id)
    positions = Map.new(rm.positions, fn {k, v} -> {k, %{x: v["x"], y: v["y"]}} end)
    positioned_ids = Map.keys(positions)
    {positioned, unpositioned} = Enum.split_with(rm.students, &(&1.id in positioned_ids))
    positioned = Enum.map(positioned, &Map.merge(&1, positions[&1.id]))
    unpositioned = Enum.sort_by(unpositioned, & &1.name)

    socket
    |> assign(seating_plan_rm: rm)
    |> assign(positioned_students: positioned)
    |> assign(unpositioned_students: unpositioned)
  end

  defp load_seating_plan_aggregate(socket) do
    socket
    |> assign(seating_plan_ag: SeatingPlans.aggregate_state(socket.assigns.seating_plan_id))
  end

  defp load_room_read_model(socket) do
    assign(socket, room_rm: Rooms.by_id!(socket.assigns.seating_plan_rm.room_id))
  end

  defp calculate_button_mapping(socket) do
    assign(socket,
      button_mapping:
        SeatingPlans.Buttons.map(%{
          room_id: socket.assigns.room_rm.id,
          seating_plan_id: socket.assigns.seating_plan_id
        })
    )
  end

  defp parse_coordinates(%{"x" => x, "y" => y}),
    do: %{x: String.to_integer(x), y: String.to_integer(y)}
end
