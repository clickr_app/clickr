defmodule ClickrWeb.SeatingPlan.New do
  use Surface.LiveView
  alias Clickr.{Classes, Rooms, SeatingPlans}
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("New seating plan")
  data errors, :list, default: []
  data class_options, :list, default: []
  data room_options, :list, default: []

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <Components.ActionCard title={gettext("New seating plan")}>
        <Surface.Components.Form for={:seating_plan} errors={@errors} submit="save" class="space-y-6">
          <Components.Alerts.Error :if={not Enum.empty?(@errors)} text={gettext("An error occured. See details below.")} class="mb-3" />

          <Form.Field label={gettext("Class")} name={:class_id}>
            <Form.Select options={@class_options} />
          </Form.Field>

          <Form.Field label={gettext("Room")} name={:room_id}>
            <Form.Select options={@room_options} />
          </Form.Field>

          <Components.Button class="mt-5 mx-auto" label={gettext("Create")} opts={'phx-disable-with': gettext("Creating...")} />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> load_class_options()
     |> load_room_options()}
  end

  @impl true
  def handle_event("save", %{"seating_plan" => params}, socket) do
    params =
      params
      |> Map.put(:width, 8)
      |> Map.put(:height, 6)

    case SeatingPlans.create(socket.assigns.current_user, params) do
      {:ok, %{id: id}} ->
        {:noreply,
         socket
         |> put_flash(:info, gettext("Seating plan created"))
         |> push_redirect(to: Routes.seating_plan_path(socket, :show, id, replace: true))}

      {:error, :validation_failure, errors} ->
        {:noreply, assign(socket, errors: errors)}

      _ ->
        {:noreply, put_flash(socket, :error, gettext("Could not create seating plan"))}
    end
  end

  def load_class_options(socket) do
    {classes, _count} = Classes.list()
    options = Enum.map(classes, &%{label: &1.name, value: &1.id})
    assign(socket, class_options: options)
  end

  def load_room_options(socket) do
    {rooms, _count} = Rooms.list()
    options = Enum.map(rooms, &%{label: &1.name, value: &1.id})
    assign(socket, room_options: options)
  end
end
