defmodule ClickrWeb.SeatingPlan.List do
  use Surface.LiveView
  alias Surface.Components.{LiveRedirect, Form}
  alias Clickr.SeatingPlans
  alias ClickrWeb.Components
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Seating plans")
  data seating_plans, :list
  data count, :integer
  data limit, :integer, default: 12

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <h1 class="text-center">{gettext("Seating plans")}</h1>
      <LiveRedirect to={Routes.seating_plan_path(@socket, :new)} class="mt-3 mx-auto btn-primary">
        {Heroicons.Solid.plus(class: "h-5 w-5 mr-2")}
        {gettext("New seating plan")}
      </LiveRedirect>

      <Form for={:search} change="search" submit="search" class="mt-3">
        <Components.Form.SearchInput name="query" opts={placeholder: gettext("Search by class or room name"), 'phx-debounce': 100} />
      </Form>

      <Components.ContactCards class="mt-5">
        {#for seating_plan <- @seating_plans}
          <LiveRedirect to={Routes.seating_plan_path(@socket, :show, seating_plan)}>
            <Components.Card>
              <Components.ContactCard name={"#{seating_plan.class_name}/#{seating_plan.room_name}"} />
            </Components.Card>
          </LiveRedirect>
        {/for}
      </Components.ContactCards>

      <div class="mt-5 text-center text-gray-700">
        {gettext("showing %{count} of %{total} matching", count: Enum.min([@limit, @count]), total: @count)}
      </div>
    </Components.Layout>
    """
  end

  @impl true
  def mount(_params, _session, socket) do
    {:ok,
     socket
     |> load_seating_plans()}
  end

  @impl true
  def handle_event("search", %{"query" => query}, socket) do
    {:noreply, load_seating_plans(socket, query: query)}
  end

  defp load_seating_plans(socket, opts \\ []) do
    opts = Keyword.merge([limit: socket.assigns.limit], opts)
    {seating_plans, count} = SeatingPlans.list(opts)
    assign(socket, seating_plans: seating_plans, count: count)
  end
end
