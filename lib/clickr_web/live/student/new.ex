defmodule ClickrWeb.Student.New do
  use Surface.LiveView

  alias Clickr.Students
  alias ClickrWeb.Components
  alias ClickrWeb.Components.Form
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data class_id, :string
  data errors, :list, default: []
  data page_title, :string, default: gettext("New student")

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <Components.ActionCard title={gettext("New student")}>
        <Surface.Components.Form for={:student} errors={@errors} submit="save">
          <Components.Alerts.Error :if={not Enum.empty?(@errors)} text={gettext("An error occured. See details below.")} class="mb-3" />
          <Surface.Components.Form.HiddenInput name="student[class_id]" value={@class_id} />

          <Form.Field name={:name}>
            <Form.TextInput opts={required: true, 'x-trap': "true"} />
          </Form.Field>

          <Components.Button class="mt-5 mx-auto" label={gettext("Create")} opts={'phx-disable-with': gettext("Creating...")} />
        </Surface.Components.Form>
      </Components.ActionCard>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"class_id" => class_id}, _session, socket) do
    {:ok, assign(socket, class_id: class_id)}
  end

  @impl true
  def handle_event("save", %{"student" => params}, socket) do
    case Students.create(socket.assigns.current_user, params) do
      {:ok, _} ->
        {:noreply,
         socket
         |> put_flash(:info, gettext("Student created"))
         |> push_redirect(
           to: Routes.class_path(socket, :show, socket.assigns.class_id, replace: true)
         )}

      {:error, :validation_failure, errors} ->
        {:noreply, assign(socket, errors: errors)}

      _ ->
        {:noreply, put_flash(socket, :error, gettext("Could not create student"))}
    end
  end
end
