defmodule ClickrWeb.Student.Show do
  use Surface.LiveView

  alias Clickr.{Grades, Students}
  alias Clickr.Students.Events
  alias ClickrWeb.Components
  alias ClickrWeb.Router.Helpers, as: Routes
  import Clickr.Gettext

  data page_title, :string, default: gettext("Student")
  data student_id, :string
  data student, :map
  data grade, :map

  @impl true
  def render(assigns) do
    ~F"""
    <Components.Layout {=@current_user} {=@socket} {=@flash} container={:xl}>
      <Components.Rename id={@student_id} value={@student.name} display_value={"#{gettext("Student")} #{@student.name}"} rename_fn={fn name -> Students.rename(@current_user, id: @student_id, name: name) end} />

      <div class="mt-3 flex gap-3 justify-center">
        <Components.Delete id={@student_id} delete_fn={fn -> Students.delete(@current_user, id: @student_id) end} />
      </div>

      <div class="mt-8">
        <h1 class="text-xl font-semibold text-gray-900">{gettext("Lessons")}</h1>

        <div class="mt-3 flex flex-col">
          <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
              <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                <table class="min-w-full divide-y divide-gray-300">
                  <thead class="bg-gray-50">
                    <tr>
                      <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">{gettext("Date")}</th>
                      <th scope="col" class="px-3 py-3.5 text-left text-sm font-semibold text-gray-900">{gettext("Percent")}</th>
                      <th scope="col" class="relative py-3.5 pl-3 pr-4 sm:pr-6">
                        <span class="sr-only">{gettext("Show")}</span>
                      </th>
                    </tr>
                  </thead>
                  <tbody class="divide-y divide-gray-200 bg-white">
                    {#for lesson <- @grade.lessons}
                      <tr>
                        <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">01.02.2023</td>
                        <td class="whitespace-nowrap px-3 py-4 text-sm text-gray-500">{percent(lesson.percent)}</td>
                        <td class="relative whitespace-nowrap py-4 pl-3 pr-4 text-right text-sm font-medium sm:pr-6">
                          <a href={Routes.lesson_path(@socket, :show, lesson.id)} class="text-primary">{gettext("Show")}<span class="sr-only">, 01.02.2023</span></a>
                        </td>
                      </tr>
                    {/for}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>

      <div class="mt-8">
        <h1 class="text-xl font-semibold text-gray-900">{gettext("Bonus")}</h1>

        <div class="mt-3 flex flex-col">
          <div class="-my-2 -mx-4 overflow-x-auto sm:-mx-6 lg:-mx-8">
            <div class="inline-block min-w-full py-2 align-middle md:px-6 lg:px-8">
              <div class="overflow-hidden shadow ring-1 ring-black ring-opacity-5 md:rounded-lg">
                <table class="min-w-full divide-y divide-gray-300">
                  <thead class="bg-gray-50">
                    <tr>
                      <th scope="col" class="py-3.5 pl-4 pr-3 text-left text-sm font-semibold text-gray-900 sm:pl-6">{gettext("Percent")}</th>
                    </tr>
                  </thead>
                  <tbody class="divide-y divide-gray-200 bg-white">
                    {#for bonus <- @grade.bonus}
                      <tr>
                        <td class="whitespace-nowrap py-4 pl-4 pr-3 text-sm font-medium text-gray-900 sm:pl-6">{percent(bonus)}</td>
                      </tr>
                    {/for}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </Components.Layout>
    """
  end

  @impl true
  def mount(%{"id" => id}, _session, socket) do
    if connected?(socket), do: Students.subscribe(id)

    {:ok,
     socket
     |> assign(student_id: id)
     |> load_student()}
  end

  @impl true
  def handle_info(%Events.Renamed{}, socket) do
    {:noreply, load_student(socket)}
  end

  def handle_info(%Events.Deleted{}, socket) do
    route =
      case socket.assigns.student.class_id do
        nil -> Routes.classes_path(socket, :list)
        class_id -> Routes.class_path(socket, :show, class_id)
      end

    {:noreply, push_redirect(socket, to: route)}
  end

  def handle_info(_, socket), do: {:noreply, socket}

  defp load_student(socket) do
    student = Students.by_id!(socket.assigns.student_id)
    grade = Grades.with_details_by_id!(student.current_grade_id)
    assign(socket, student: student, grade: grade)
  end

  defp percent(float), do: "#{Float.round(float || 0.0, 1)} %"
end
