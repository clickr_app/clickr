defmodule ClickrWeb.Sockets.Api do
  use Phoenix.Socket
  alias Clickr.Accounts

  channel "home_assistant", ClickrWeb.Channels.HomeAssistant
  channel "deconz", ClickrWeb.Channels.Deconz

  def connect(%{"auth_token" => auth_token}, socket, _connect_info) do
    case Accounts.get_user_by_api_token(auth_token) do
      nil -> {:error, :invalid_auth_token}
      user -> {:ok, assign(socket, auth_token: auth_token, current_user: user)}
    end
  end

  def connect(_params, _socket, _connect_info) do
    {:error, :invalid_auth_token}
  end

  def id(socket), do: "auth_token:#{socket.assigns.auth_token}"
end
