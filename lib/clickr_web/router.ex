defmodule ClickrWeb.Router do
  use ClickrWeb, :router
  use BotFramework.Phoenix.Router

  import ClickrWeb.UserAuth
  import Phoenix.LiveDashboard.Router

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {ClickrWeb.LayoutView, :root}
    plug :put_layout, false
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  # Other scopes may use custom stacks.
  # scope "/api", ClickrWeb do
  #   pipe_through :api
  # end

  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      # Note that preview only shows emails that were sent by the same
      # node running the Phoenix server.
      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  scope "/", ClickrWeb do
    pipe_through [:browser, :redirect_if_user_is_authenticated]

    get "/users/register", UserRegistrationController, :new
    post "/users/register", UserRegistrationController, :create
    get "/users/log_in", UserSessionController, :new
    post "/users/log_in", UserSessionController, :create
    get "/users/reset_password", UserResetPasswordController, :new
    post "/users/reset_password", UserResetPasswordController, :create
    get "/users/reset_password/:token", UserResetPasswordController, :edit
    put "/users/reset_password/:token", UserResetPasswordController, :update
  end

  scope "/", ClickrWeb do
    pipe_through [:browser, :require_authenticated_user]

    live_session :authenticated, on_mount: ClickrWeb.UserLiveAuth do
      live "/", Welcome

      live "/classes", Class.List, :list, as: :classes
      live "/classes/new", Class.New, :new, as: :class
      live "/classes/:id", Class.Show, :show, as: :class

      live "/lessons", Lesson.List, :list, as: :lessons
      live "/lessons/new", Lesson.New, :new, as: :lesson
      live "/lessons/:id", Lesson.Redirect.View, :show, as: :lesson
      live "/lessons/:id/started", Lesson.RollCall, :started, as: :lesson
      live "/lessons/:id/roll-call", Lesson.RollCall, :roll_call, as: :lesson
      live "/lessons/:id/await-question", Lesson.Question, :await_question, as: :lesson
      live "/lessons/:id/question", Lesson.Question, :question, as: :lesson
      live "/lessons/:id/ended", Lesson.Ended, :ended, as: :lesson
      live "/lessons/:id/graded", Lesson.Graded, :graded, as: :lesson

      live "/rooms", Room.List, :list, as: :rooms
      live "/rooms/new", Room.New, :new, as: :room
      live "/rooms/:id", Room.Show, :show, as: :room

      live "/seating-plans", SeatingPlan.List, :list, as: :seating_plans
      live "/seating-plans/new", SeatingPlan.New, :new, as: :seating_plan
      live "/seating-plans/:id", SeatingPlan.Show, :show, as: :seating_plan

      live "/students/new", Student.New, :new, as: :student
      live "/students/:id", Student.Show, :show, as: :student

      live "/users/api_token", User.ApiToken, :api_token, as: :users
      live "/users/invite", User.Invite, :invite, as: :users
    end

    get "/users/settings", UserSettingsController, :edit
    put "/users/settings", UserSettingsController, :update
    get "/users/settings/confirm_email/:token", UserSettingsController, :confirm_email
    live_dashboard "/dashboard", metrics: ClickrWeb.Telemetry
  end

  scope "/", ClickrWeb do
    pipe_through [:browser]

    get "/privacy", PagesController, :privacy
    get "/tos", PagesController, :tos

    delete "/users/log_out", UserSessionController, :delete
    get "/users/confirm", UserConfirmationController, :new
    post "/users/confirm", UserConfirmationController, :create
    get "/users/confirm/:token", UserConfirmationController, :edit
    post "/users/confirm/:token", UserConfirmationController, :update
  end

  scope "/api", ClickrWeb do
    bot_framework_handler("/bot", Api.BotController)
  end
end
